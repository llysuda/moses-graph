#! /usr/bin/perl -w

use strict;
use warnings;

my $depcount = 0;
my @pos;
my $line = "";
my $reformated = "";
my $linecount = 0;
while (<>) {
    chomp;
    
    if (/^$/) {
        next;
    }
    
    if (/^(\S+)\/(\S+)/ and ($#pos == 0 or $depcount > $#pos)) { # pos line
        if ($#pos >= 0) {
            output();
            $depcount = 0;
            $reformated = "";
        }
        @pos = GetPos($_);
        $line = $_;
        $linecount++;
    } elsif (/^\(/) { # tree line
        
    } elsif (/^(\S+)\((\S+)\-(\d+),\s+(\S+)\-(\d+)\)$/) { # dep line
        die "$linecount: $line\n@pos\n$_\n" if $depcount > $#pos;
        my ($word,$p,$fid,$relation) = ($4, $pos[$depcount],$3-1,$1);
	    $word =~ s/\\(\S+)/$1/g;
	    $reformated .= "$word|$p|$fid|$relation ";
	    $depcount++;
    }
}
output();

sub output {
    if ($depcount == 0) {
        if (not $#pos == 0) {
            die "error: $line\n";
        }
        my @words = GetWord($line);
        $reformated = "$words[0]|$pos[0]|-1|root ";
    }
    chop ($reformated);
    print "$reformated\n";
}

sub GetWord {
    my $str = shift;
    my @ret;
    my @tokens = split /\s+/, $str;
    for my $tok (@tokens) {
        if ($tok =~ /(.+)\/(\S+)/) {
            push @ret,$1;
        } else {
            die "format error: $str\n";
        }
    }
    return @ret;
}

sub GetPos {
    my $str= shift;
    my @ret;
    my @tokens = split /\s+/, $str;
    for my $tok (@tokens) {
        if ($tok =~ /(.+)\/(\S+)$/) {
            push @ret,$2;
        } else {
            die "pos format error: $str\n$line\n";
        }
    }
    return @ret;
}
