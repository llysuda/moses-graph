#!/usr/bin/perl -w

use strict;
use warnings;

my $last_line = "";
my $last_src = "";
my $last_tgt = "";
my $last_score = 0.0;

while (<>) {
	chomp;
	my ($src,$tgt,$scores,$rest) = split/\s+\|\|\|\s+/,$_,4;
	my @tokens = split /\s+/,$scores;
	my $score = 0.0;
	for my $tok (@tokens) {
		$score += $tok;
	}
		
	if ($src eq $last_src and $tgt eq $last_tgt) {
		
		if ($last_score < $score) {
			$last_score = $score;
		}
	} else {
		if (not $last_line eq "") {
			print "$last_line\n";
		}
		
		$last_line = $_;
		$last_src = $src;
		$last_tgt = $tgt;
		$last_score = $score;
	}
}
if (not $last_line eq "") {
	print "$last_line\n";
}
