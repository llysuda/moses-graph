#!/usr/bin/perl -w

use strict;
use warnings;

my ($A,$B) = @ARGV;

open A,"<",$A or die "open $A error\n";
open B,"<",$B or die "open $A error\n";
my $COUNT = 0;
while (my $aline = <A>) {
	chomp $aline;
	my $bline=<B>;
	chomp $bline;
	
	my ($asrc,$atgt,$arest) = split /\s+\|\|\|\s+/,$aline;
	my ($bsrc,$btgt,$brest) = split /\s+\|\|\|\s+/,$bline;
	
	if (not($asrc eq $bsrc and $atgt eq $btgt)) {
		die "$COUNT\n$aline\n$bline\n";
	}
	my $COUNT++;
}

close A;
close B;
