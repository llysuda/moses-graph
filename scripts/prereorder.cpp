#include <string>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <boost/iostreams/filtering_stream.hpp>


#define SAFE_GETLINE(_IS, _LINE, _SIZE, _DELIM, _FILE) {            \
    _IS.getline(_LINE, _SIZE, _DELIM);                              \
    if(_IS.fail() && !_IS.bad() && !_IS.eof()) _IS.clear();         \
    if (_IS.gcount() == _SIZE-1) {                                  \
      cerr << "Line too long! Buffer overflow. Delete lines >="     \
       << _SIZE << " chars or raise LINE_MAX_LENGTH in " << _FILE   \
       << endl;                                                     \
      exit(1);                                                      \
    }                                                               \
  }


using namespace std;

class InputFileStream : public std::istream
{
protected:
  std::streambuf *m_streambuf;
public:

  InputFileStream(const std::string &filePath);
  ~InputFileStream();

  void Close();
};

class OutputFileStream : public boost::iostreams::filtering_ostream
{
protected:
  std::ofstream *m_outFile;
public:
  OutputFileStream();

  OutputFileStream(const std::string &filePath);
  virtual ~OutputFileStream();

  bool Open(const std::string &filePath);
  void Close();
};

InputFileStream::InputFileStream(const std::string &filePath)
  : std::istream(NULL)
  , m_streambuf(NULL)
{
    std::filebuf* fb = new std::filebuf();
    fb = fb->open(filePath.c_str(), std::ios::in);
    if (! fb) {
      cerr << "Can't read " << filePath.c_str() << endl;
      exit(1);
    }
    m_streambuf = fb;
  this->init(m_streambuf);
}

InputFileStream::~InputFileStream()
{
  delete m_streambuf;
  m_streambuf = NULL;
}

void InputFileStream::Close()
{
}

OutputFileStream::OutputFileStream()
  :boost::iostreams::filtering_ostream()
  ,m_outFile(NULL)
{
}

OutputFileStream::OutputFileStream(const std::string &filePath)
  : m_outFile(NULL)
{
  Open(filePath);
}

OutputFileStream::~OutputFileStream()
{
  Close();
}

bool OutputFileStream::Open(const std::string &filePath)
{
  m_outFile = new ofstream(filePath.c_str(), ios_base::out | ios_base::binary);
  if (m_outFile->fail()) {
    return false;
  }

  this->push(*m_outFile);

  return true;
}

void OutputFileStream::Close()
{
  if (m_outFile == NULL) {
    return;
  }

  this->flush();
  this->pop(); // file

  m_outFile->close();
  delete m_outFile;
  m_outFile = NULL;
  return;
}

inline std::vector<std::string> Tokenize(const std::string& str,
    const std::string& delimiters = " \t")
{
  std::vector<std::string> tokens;
  // Skip delimiters at beginning.
  std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  // Find first "non-delimiter".
  std::string::size_type pos     = str.find_first_of(delimiters, lastPos);

  while (std::string::npos != pos || std::string::npos != lastPos) {
    // Found a token, add it to the vector.
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    // Skip delimiters.  Note the "not_of"
    lastPos = str.find_first_not_of(delimiters, pos);
    // Find next "non-delimiter"
    pos = str.find_first_of(delimiters, lastPos);
  }

  return tokens;
}

std::string Trim(const std::string& str, const std::string dropChars)
{
  std::string res = str;
  res.erase(str.find_last_not_of(dropChars)+1);
  return res.erase(0, res.find_first_not_of(dropChars));
}

string ParseXmlTagAttribute(const string& tag,const string& attributeName)
{
  /*TODO deal with unescaping \"*/
  string tagOpen = attributeName + "=\"";
  size_t contentsStart = tag.find(tagOpen);
  if (contentsStart == string::npos) return "";
  contentsStart += tagOpen.size();
  size_t contentsEnd = tag.find_first_of('"',contentsStart+1);
  if (contentsEnd == string::npos) {
    cerr << "Malformed XML attribute: "<< tag << endl;
    exit(1);
  }
  size_t possibleEnd;
  while (tag.at(contentsEnd-1) == '\\' && (possibleEnd = tag.find_first_of('"',contentsEnd+1)) != string::npos) {
    contentsEnd = possibleEnd;
  }
  return tag.substr(contentsStart,contentsEnd-contentsStart);
}

string TrimXml(const string& str, const std::string& lbrackStr, const std::string& rbrackStr)
{
  // too short to be xml token -> do nothing
  if (str.size() < lbrackStr.length()+rbrackStr.length() ) return str;

  // strip first and last character
  if (str.substr(0,lbrackStr.length()) == lbrackStr  &&  str.substr(str.size()-rbrackStr.length()) == rbrackStr) {
    return str.substr(lbrackStr.length(), str.size()-lbrackStr.length()-rbrackStr.length());
  }
  // not an xml token -> do nothing
  else {
    return str;
  }
}

bool isXmlTag(const string& tag, const std::string& lbrackStr="<", const std::string& rbrackStr=">")
{
  return (tag.substr(0,lbrackStr.length()) == lbrackStr &&
          (tag[lbrackStr.length()] == '/' ||
           (tag[lbrackStr.length()] >= 'a' && tag[lbrackStr.length()] <= 'z') ||
           (tag[lbrackStr.length()] >= 'A' && tag[lbrackStr.length()] <= 'Z')));
}

vector<string> TokenizeXml(const string& str, const std::string& lbrackStr="<", const std::string& rbrackStr=">")
{
  string lbrack = lbrackStr; // = "<";
  string rbrack = rbrackStr; // = ">";
  vector<string> tokens; // vector of tokens to be returned
  string::size_type cpos = 0; // current position in string
  string::size_type lpos = 0; // left start of xml tag
  string::size_type rpos = 0; // right end of xml tag

  // walk thorugh the string (loop vver cpos)
  while (cpos != str.size()) {
    // find the next opening "<" of an xml tag
    lpos = str.find(lbrack, cpos);			// lpos = str.find_first_of(lbrack, cpos);
    if (lpos != string::npos) {
      // find the end of the xml tag
      rpos = str.find(rbrack, lpos+lbrackStr.length()-1);			// rpos = str.find_first_of(rbrack, lpos);
      // sanity check: there has to be closing ">"
      if (rpos == string::npos) {
        cerr << "ERROR: malformed XML: " << str << endl;
        exit(1);
      }
    } else { // no more tags found
      // add the rest as token
      tokens.push_back(str.substr(cpos));
      break;
    }

    // add stuff before xml tag as token, if there is any
    if (lpos - cpos > 0)
      tokens.push_back(str.substr(cpos, lpos - cpos));

    // add xml tag as token
    tokens.push_back(str.substr(lpos, rpos-lpos+rbrackStr.length()));
    cpos = rpos + rbrackStr.length();
  }
  return tokens;
}

class Node {
public:
  std::string label;
  std::string word;
  Node* father;
  std::vector<Node*> kids;
public:
  Node(): father(NULL), label(""), word(""){}

  Node(const Node* node) {
    label = node->label;
    father = node->father;
    kids = node->kids;
    label = node->label;
    word = node->word;
  }

  Node(std::string label, Node* father,std::string word="") {
      this->label = label;
      this->word=word;
      this->father = father;
  }

  bool IsLeaf() const {
      return kids.size() == 0;
  }
  
  void setLabel(string label) {
    this->label = label;
  }
  
  void setWord(string word) {
    this->word = word;
  }
  
  void setFather(Node* father) {
    this->father = father;
  }
};

class Converter {
private:
  int m_sentID;
  Node* root;
public:
  Converter(int sentID, const std::string & line);
  void process();
  std::string output() const {
    return output(root);
  }
  std::string output(Node* node) const;
  virtual ~Converter() {};
private:
  void step1(Node* node);
  void step2(Node* node);
  void step3(Node* node);
  void step4(Node* node);
  void RemoveVP(Node* node);
  void step5(Node* node);
  void step6(Node* node);
};

Converter::Converter(int sentID, const std::string & line) {
  root = new Node();
  //parse XML markup in translation line

  // no xml tag? we're done.
  if (line.find_first_of('<') == string::npos) {
    return;
  }

  // break up input into a vector of xml tags and text
  // example: (this), (<b>), (is a), (</b>), (test .)
  vector<string> xmlTokens = TokenizeXml(line,"<",">");

  // we need to store opened tags, until they are closed
  // tags are stored as tripled (tagname, startpos, contents)
  typedef pair< string, pair< size_t, string > > OpenedTag;
  vector< OpenedTag > tagStack; // stack that contains active opened tags

  string cleanLine; // return string (text without xml)
  size_t wordPos = 0; // position in sentence (in terms of number of words)
  
  Node* currentNode =  NULL;
  // loop through the tokens
  for (size_t xmlTokenPos = 0 ; xmlTokenPos < xmlTokens.size() ; xmlTokenPos++) {
    // not a xml tag, but regular text (may contain many words)
    if(!isXmlTag(xmlTokens[xmlTokenPos])) {
      // add a space at boundary, if necessary
      if (cleanLine.size()>0 &&
          cleanLine[cleanLine.size() - 1] != ' ' &&
          xmlTokens[xmlTokenPos][0] != ' ') {
        cleanLine += " ";
      }
      cleanLine += xmlTokens[xmlTokenPos]; // add to output
      string word = Trim(xmlTokens[xmlTokenPos]," \t");
      if (word.size() > 0)
        currentNode->word = word;
      wordPos = Tokenize(cleanLine).size(); // count all the words
    }

    // process xml tag
    else {
      // *** get essential information about tag ***

      // strip extra boundary spaces and "<" and ">"
      string tag =  Trim(TrimXml(xmlTokens[xmlTokenPos],"<",">")," \t");
      //VERBOSE(3,"XML TAG IS: " << tag << std::endl);

      if (tag.size() == 0) {
        cerr << "ERROR: empty tag name: " << line << endl;
        exit(1);
      }

      // check if unary (e.g., "<wall/>")
      bool isUnary = ( tag[tag.size() - 1] == '/' );

      // check if opening tag (e.g. "<a>", not "</a>")g
      bool isClosed = ( tag[0] == '/' );
      bool isOpen = !isClosed;

      if (isClosed && isUnary) {
        cerr << "ERROR: can't have both closed and unary tag <" << tag << ">: " << line << endl;
        exit(1);
      }

      if (isClosed)
        tag = tag.substr(1); // remove "/" at the beginning
      if (isUnary)
        tag = tag.substr(0,tag.size()-1); // remove "/" at the end

      // find the tag name and contents
      string::size_type endOfName = tag.find_first_of(' ');
      string tagName = tag;
      string tagContent = "";
      if (endOfName != string::npos) {
        tagName = tag.substr(0,endOfName);
        tagContent = tag.substr(endOfName+1);
      }

      // *** process new tag ***

      if (isOpen || isUnary) {
        // put the tag on the tag stack
        OpenedTag openedTag = make_pair( tagName, make_pair( wordPos, tagContent ) );
        tagStack.push_back( openedTag );
        //VERBOSE(3,"XML TAG " << tagName << " (" << tagContent << ") added to stack, now size " << tagStack.size() << endl);
        
        string label = ParseXmlTagAttribute(tagContent,"label");
        
        if (label.length() > 0) {
          Node* node = new Node(label,currentNode,"");
          if (currentNode != NULL) {
            currentNode->kids.push_back(node);
          }
          currentNode = node;
        } else {
          cerr << "empty label" << endl;
          exit(1);
        }
      }

      // *** process completed tag ***

      if (isClosed || isUnary) {
        // pop last opened tag from stack;
        if (tagStack.size() == 0) {
          cerr << "ERROR: tag " << tagName << " closed, but not opened" << ":" << line << endl;
          exit(1);
        }
        OpenedTag openedTag = tagStack.back();
        tagStack.pop_back();

        // tag names have to match
        if (openedTag.first != tagName) {
          cerr << "ERROR: tag " << openedTag.first << " closed by tag " << tagName << ": " << line << endl;
          exit(1);
        }

        // assemble remaining information about tag
        size_t startPos = openedTag.second.first;
        string tagContent = openedTag.second.second;
        size_t endPos = wordPos;

        if (startPos >= endPos) {
          cerr << "ERROR: tag " << tagName << " must span at least one word: " << line << endl;
          exit(1);
        }

        // specified label
        if (currentNode->father != NULL)
          currentNode = currentNode->father;

        
      }
    }
  }
  // we are done. check if there are tags that are still open
  if (tagStack.size() > 0) {
    cerr << "ERROR: some opened tags were never closed: " << line << endl;
    exit(1);
  }

  // return de-xml'ed sentence in line
  //line = cleanLine;
  root = currentNode;
}

void Converter::process() {
  //cerr << "original: " << output() << endl;
  //cerr << "step 1" << endl;
  step1(root);
  //cerr << "step 1: " << output() << endl;
  //cerr << "step 2" << endl;
  step2(root);
  //cerr << "step 2: " << output() << endl;
  //cerr << "step 3" << endl;
  step3(root);
  //cerr << "step 3: " << output() << endl;
  //cerr << "step 4 " << endl;
  step4(root);
  //cerr << "step 4: " << output() << endl;
  //cerr << "remove vp" << endl;
  RemoveVP(root);
  //cerr << "step $: " << output() << endl;
  //cerr << "step 5" << endl;
  step5(root);
  //cerr << "step 5: " << output() << endl;
  //cerr << "step 6" <<endl;
  step6(root);
  //cerr << "step 6: " << output() << endl;
}

void Converter::step1(Node* node) {
  if (node->label.find("VP-") == 0) {
    //vector<Node*>::iterator iter = node->kids.begin();
	size_t i = 0, hdpos;
	int hdcount = 0;
    for(; i < node->kids.size(); i++) {
      Node* kid = node->kids[i];
      if (kid->label.find("-HD") != std::string::npos) {
        if (hdcount > 0) {
          cerr << "step 1: more than one hd found" << endl;
          break;
        }
        hdpos = i;
        hdcount++;
        //break;
      }
    }
    i = hdpos;
    if (i < node->kids.size()) {
      node->kids.insert(node->kids.begin(),node->kids[i]);
      node->kids.erase(node->kids.begin()+i+1);
    }
  }
  for(size_t i = 0; i < node->kids.size(); i++) {
    step1(node->kids[i]);
  }
}

void Converter::step2(Node* node) {
  if (node->label.find("S-") == 0 && node->label.find("S-TOP") == std::string::npos) {
    bool flag = false;
    
    //vector<Node*>::iterator iter = node->kids.begin();
    size_t i = 0, pos;
    int count = 0;
    for(; i < node->kids.size(); i++) {
      Node* kid = node->kids[i];
      if (kid->label.find("KOUS-") == 0 ||
          kid->label.find("PRELS-") == 0 ||
          kid->label.find("PWS-") == 0 ||
          kid->label.find("PWAV-") == 0 ) {
        if(count > 0) {
          cerr << "step 2: more than one complimentizer found" << endl;
          break;
        }
         count++;
         pos = i;
      }
    }
    i = pos;
    count = 0;
    pos = node->kids.size();
    
    if (i < node->kids.size()) {
      size_t j = 0;
      for(; j < node->kids.size(); j++) {
        Node* kid = node->kids[j];
        if (kid->label.find("-HD") != std::string::npos) {
          if(count > 0) {
            cerr << "step 2: more than one head found" << endl;
            break;
          }
           count++;
           pos = j;
        }
      }
      j = pos;
      
      if (j < node->kids.size()) {
        node->kids.insert(node->kids.begin()+i+1, node->kids[j]);
		if (j > i)
          node->kids.erase(node->kids.begin()+j+1);
		else
		  node->kids.erase(node->kids.begin()+j);
      }
    }
  }
  for(size_t i = 0; i < node->kids.size(); i++) {
    step2(node->kids[i]);
  }
}

void Converter::step3(Node* node) {
  if (node->label.find("S-") == 0) {
    
    size_t i=0;
    
    for(; i < node->kids.size(); i++) {
      Node* kid = node->kids[i];
      if (kid->label.find("-SB") != std::string::npos || kid->label.find("PPER-EP") == 0) {
          break;
      }
    }
    
    if (i < node->kids.size()) {
      size_t j = 0;
      for(; j < node->kids.size(); j++) {
        Node* kid = node->kids[j];
        if (kid->label.find("-HD") != std::string::npos) {
          break;
        }
      }
      
      if (j < node->kids.size()) {
        node->kids.insert(node->kids.begin()+j, node->kids[i]);
		if (i > j)
          node->kids.erase(node->kids.begin()+i+1);
		else
		  node->kids.erase(node->kids.begin()+i);
      }
    }
  }
  for(size_t i = 0; i < node->kids.size(); i++) {
    step3(node->kids[i]);
  }
}

void Converter::step4(Node* node) {
  if (node->label.find("S-") == 0) {
  size_t i=0,pos;
  int count = 0;
    
	for(; i < node->kids.size(); i++) {
	  Node* kid = node->kids[i];
	  if (kid->label.find("VVFIN-") == 0) {
		 if (count > 0) {
		   cerr << "step 4: more than one VVFIN found" << endl;
		   break;
		 }
		 count++;
		 pos = i;
	  }
	}
	i = pos;
	count = 0;
	pos = node->kids.size();

	if (i < node->kids.size()) {
	  size_t j = 0;
	  for(; j < node->kids.size(); j++) {
		Node* kid = node->kids[j];
		if (kid->label.find("PTKVZ-") == 0) {
		  if (count > 0) {
             cerr << "step 4: more than one PTKVZ found" << endl;
             break;
           }
           count++;
           pos = j;
		}
	  }
	  j = pos;
	  
	  if (j < node->kids.size()) {
		node->kids.insert(node->kids.begin()+i, node->kids[j]);
		if (j > i)
		  node->kids.erase(node->kids.begin()+j+1);
		else
		  node->kids.erase(node->kids.begin()+j);
	  }
	}
  }
  for(size_t i = 0; i < node->kids.size(); i++) {
    step4(node->kids[i]);
  }
}

void Converter::step5(Node* node) {
  if (node->label.find("S-") == 0) {
    size_t i, posINF=std::string::npos, posFIN=std::string::npos;
    bool flag = false;
    int infCount = 0, finCount = 0;
    for (i = 0; i < node->kids.size(); i++) {
      Node* kid = node->kids[i];
      if(kid->label.find("VVINF-")==0 ||
         kid->label.find("VAINF-")==0 ||
         kid->label.find("VMINF-")==0 ) {
         if (posINF == std::string::npos) {
           posINF = i;
         }
         infCount++;
           
       } else if(kid->label.find("VVFIN-") == 0 ||
                 kid->label.find("VAFIN-") == 0 ||
                 kid->label.find("VMFIN-") == 0 ) {
         if (posFIN == std::string::npos) {
           posFIN = i;
         }
         finCount++;
       } else {
       
         if ((posINF != std::string::npos && posFIN==std::string::npos) || 
			 (posINF == std::string::npos && posFIN!=std::string::npos)) {
           if (kid->label.find("-OA") != std::string::npos ||
               kid->label.find("-OC") != std::string::npos ||
               kid->label.find("-OG") != std::string::npos ||
               kid->label.find("-OP") != std::string::npos ||
               kid->label.find("-SB") != std::string::npos) {
             flag = true;
           }
         }
       }
    }

    if (flag && posFIN != std::string::npos && posINF != std::string::npos) {

      if (finCount > 1 || infCount > 1) {
        cerr << "step 5: more than one inf verb or finite verb" << endl;
        //exit(1);
      }

      node->kids.insert(node->kids.begin()+posFIN+1, node->kids[posINF]);
	  if (posFIN < posINF)
        node->kids.erase(node->kids.begin()+posINF+1);
	  else
	    node->kids.erase(node->kids.begin()+posINF);
    }
  }
  
  for(size_t i = 0; i < node->kids.size(); i++) {
    step5(node->kids[i]);
  }
}

void Converter::RemoveVP(Node* node) {
  
  if (node->IsLeaf()) return;
  
  vector<Node*>::iterator iter;
  for (iter = node->kids.begin(); iter != node->kids.end(); iter++) {
    Node* kid = *iter;
    RemoveVP(kid);
  }
  
  //vector<vector<Node*>::iterator> iters;
  for (size_t j = 0; j < node->kids.size();) {
    Node* kid = node->kids[j];
    if(kid->label.find("VP-") == 0) {
	  for (size_t i = 0; i < kid->kids.size(); i++) {
		node->kids.insert(node->kids.begin()+j+i+1,kid->kids[i]);
	  }
	  node->kids.erase(node->kids.begin()+j);
	} else {
	  j++;
	}
  }
}
  
  //for(size_t i = 0; i < iters.size(); i++) {
  //  Node* kid = *iters[i];
  //  for (size_t i = 0; i < kid->kids.size(); i++) {
  //    node->kids.push_back(kid->kids[i]);
  //  }
  //  node->kids.erase(iters[i]);
  //}
//}

void Converter::step6(Node* node) {
  if (node->label.find("S-") == 0) {
    size_t iter, iterFIN=std::string::npos, iterINF=std::string::npos, iterP=std::string::npos;
    bool flag = false;
    int fincount = 0,pcount=0;
    for (iter = 0; iter < node->kids.size(); iter++) {
      Node* kid = node->kids[iter];
      if(kid->label.find("VVINF-") == 0 ||
         kid->label.find("VAINF-") == 0 ||
         kid->label.find("VMINF-") == 0 ) {
    
           iterINF = iter;
           
       } else if(kid->label.find("VVFIN-") == 0 ||
                 kid->label.find("VAFIN-") == 0 ||
                 kid->label.find("VMFIN-") == 0) {
         if (iterFIN == std::string::npos) {
           iterFIN = iter;
         }
         fincount++;
       } else if(kid->label.find("PTKNEG-") == 0) {
         if (iterP == std::string::npos) {
           iterP = iter;
         }
         pcount++;
       }
    }
    
    if (iterFIN != std::string::npos && iterINF != std::string::npos && iterP != std::string::npos) {
      if (fincount > 1 || pcount > 1) {
        cerr << "step 6: more than one time finite verbs or neg particle" << endl;
        //exit(1);
      }
      node->kids.insert(node->kids.begin()+iterFIN+1, node->kids[iterP]);
	  if (iterP > iterFIN)
        node->kids.erase(node->kids.begin()+iterP+1);
	  else
	    node->kids.erase(node->kids.begin()+iterP);
    }
  }

  for(size_t i = 0; i < node->kids.size(); i++) {
    step6(node->kids[i]);
  }
}

std::string Converter::output(Node* node) const {
  
  if (node->IsLeaf()) {
    return node->word;
  }
  
  string ret;
  for (size_t i = 0; i < node->kids.size(); i++) {
    ret += output(node->kids[i]);
    ret += " ";
  }
  
  return ret.erase(ret.size()-1);
}

int main(int argc, char* argv[])
{
  cerr  << "prereorder v1.0, written by Liangyou Li" << endl;

  if (argc < 3) {
    cerr << "syntax: prereorder input.xml output.plaintext" << endl;
    exit(1);
  }


 const long int LINE_MAX_LENGTH = 500000 ;

 const char* const &fileNameIN = argv[1];
 const char* const &fileNameOUT = argv[2];

 OutputFileStream outFile(fileNameOUT);
 InputFileStream inFile(fileNameIN);

 istream *inFileP = &inFile;

 int i = 0;
 while(true)
 {
   i++;
   if (i%10000 == 0) cerr << "." << flush;
   //cerr << i << " " << flush;
   char inString[LINE_MAX_LENGTH];
   SAFE_GETLINE((*inFileP), inString, LINE_MAX_LENGTH, '\n', __FILE__);
   if (inFileP->eof()) break;
   //cerr << "init converter" << endl;
   Converter converter(i,string(inString));
   //cerr << "output" << endl;
   //string out1 = converter.output();
   //outFile << out1 << endl;
   //cerr << "process" << endl;
   converter.process();
   //cerr << "output" << endl;
   string out = converter.output();

   outFile << out << endl;
   //exit(1);
 }
 cerr << "end" << endl;
 inFile.Close();
 outFile.Close();

}
