#pragma once

namespace Moses
{

class InputType;
class WordsBitmap;
class TargetPhrase;

namespace Graph
{
class G2sHypothesis;
class G2sTranslationOption;
}

/**
  * Contains all that a feature function can access without affecting recombination.
  * For stateless features, this is all that it can access. Currently this is not
  * used for stateful features, as it would need to be retro-fitted to the LM feature.
  * TODO: Expose source segmentation,lattice path.
  * XXX Don't add anything to the context that would break recombination XXX
 **/
class PhraseBasedFeatureContextG2s
{
  // The context either has a hypothesis (during search), or a TranslationOption and
  // source sentence (during pre-calculation).
  const Moses::Graph::G2sHypothesis* m_hypothesis;
  const Moses::Graph::G2sTranslationOption& m_translationOption;
//  const Moses::Graph::G2sTranslationOption& m_translationOptionPg2s;
  const InputType& m_source;

public:
  PhraseBasedFeatureContextG2s(const Moses::Graph::G2sHypothesis* hypothesis);
  PhraseBasedFeatureContextG2s(const Moses::Graph::G2sTranslationOption& translationOption,
                            const InputType& source);

  const Moses::Graph::G2sTranslationOption& GetTranslationOption() const {
    return m_translationOption;
  }
  const InputType& GetSource() const {
    return m_source;
  }

  const Moses::Graph::G2sHypothesis* GetHypothesis() const {
    return m_hypothesis;
  }
  const TargetPhrase& GetTargetPhrase() const; //convenience method
  const WordsBitmap& GetWordsBitmap() const;

};

} // namespace


