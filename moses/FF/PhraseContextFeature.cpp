#include "PhraseContextFeature.h"

#include "moses/Hypothesis.h"
#include "moses/TranslationOption.h"

using namespace std;

namespace Moses
{

int PhraseContextState::Compare(const FFState& other) const
{
  const PhraseContextState& rhs = dynamic_cast<const PhraseContextState&>(other);
  int tgt = Word::Compare(*m_targetWord,*(rhs.m_targetWord));
  if (tgt) return tgt;
  return Word::Compare(*m_sourceWord,*(rhs.m_sourceWord));
}

PhraseContextFeature::PhraseContextFeature(const std::string &line)
  : StatefulFeatureFunction("PhraseContextFeature", 0, line)
{
  std::cerr << "Initializing source word deletion feature.." << std::endl;
  ReadParameters();
}

void PhraseContextFeature::SetParameter(const std::string& key, const std::string& value)
{
  if (key == "source") {
    m_sourceFactors = Tokenize<FactorType>(value, ",");
  } else if (key == "target") {
    m_targetFactors = Tokenize<FactorType>(value, ",");
  } else {
    StatefulFeatureFunction::SetParameter(key, value);
  }
}

const FFState* PhraseContextFeature::EmptyHypothesisState(const InputType &) const
{
  return new PhraseContextState(NULL,NULL);
}


void PhraseContextFeature::AddFeatures(
  const Word* leftWord, const Word* rightWord, const FactorList& factors, const string& side,
  ScoreComponentCollection* scores) const
{
  for (size_t i = 0; i < factors.size(); ++i) {
    ostringstream name;
    name << side << ":";
    name << factors[i];
    name << ":";
    if (leftWord) {
      name << leftWord->GetFactor(factors[i])->GetString();
    } else {
      name << BOS_;
    }
    name << ":";
    if (rightWord) {
      name << rightWord->GetFactor(factors[i])->GetString();
    } else {
      name << EOS_;
    }
    scores->PlusEquals(this,name.str(),1);
  }

}

FFState* PhraseContextFeature::Evaluate
(const Hypothesis& cur_hypo, const FFState* prev_state,
 ScoreComponentCollection* scores) const
{
  const PhraseContextState* pbState = dynamic_cast<const PhraseContextState*>(prev_state);
  const Phrase& targetPhrase = cur_hypo.GetCurrTargetPhrase();
  if (targetPhrase.GetSize() == 0) {
    return new PhraseContextState(*pbState);
  }
  const Word* leftTargetWord = pbState->GetTargetWord();
  const Word* rightTargetWord = &(targetPhrase.GetWord(0));
  AddFeatures(leftTargetWord,NULL,m_targetFactors,"tgtleft",scores);
  AddFeatures(rightTargetWord,NULL,m_targetFactors,"tgtright",scores);

  const Phrase& sourcePhrase = cur_hypo.GetTranslationOption().GetInputPath().GetPhrase();
  const Word* leftSourceWord = pbState->GetSourceWord();
  const Word* rightSourceWord = &(sourcePhrase.GetWord(0));
  AddFeatures(leftSourceWord,NULL,m_sourceFactors,"srcleft",scores);
  AddFeatures(rightSourceWord,NULL,m_sourceFactors,"srcright",scores);

  const WordsRange& srcRange = cur_hypo.GetCurrSourceWordsRange();
  const InputType& source = cur_hypo.GetInput();
  if (srcRange.GetStartPos() == 0) {
    AddFeatures(NULL,NULL,m_sourceFactors,"srcpre",scores);
  } else {
    const Word* preWord = &(source.GetWord(srcRange.GetStartPos()));
    AddFeatures(preWord,NULL,m_sourceFactors,"srcpre",scores);
  }
  if (srcRange.GetEndPos() == source.GetSize() -1) {
    AddFeatures(NULL,NULL,m_sourceFactors,"srcnext",scores);
  } else {
    const Word* nextWord = &(source.GetWord(srcRange.GetEndPos()));
    AddFeatures(nextWord,NULL,m_sourceFactors,"srcnext",scores);
  }

  const Word* endSourceWord = &(sourcePhrase.GetWord(sourcePhrase.GetSize()-1));
  const Word* endTargetWord = &(targetPhrase.GetWord(targetPhrase.GetSize()-1));

  //if end of sentence add EOS
  if (cur_hypo.IsSourceCompleted()) {
    AddFeatures(endSourceWord,NULL,m_sourceFactors,"srcend",scores);
    AddFeatures(endTargetWord,NULL,m_targetFactors,"tgtend",scores);
  }

  return new PhraseContextState(endSourceWord,endTargetWord);
}

bool PhraseContextFeature::IsUseable(const FactorMask &mask) const
{
  for (size_t i = 0; i < m_targetFactors.size(); ++i) {
    const FactorType &factor = m_targetFactors[i];
    if (!mask[factor]) {
      return false;
    }
  }
  return true;
}

}
