#include "PhraseBasedFeatureContextPg2s.h"
#include "moses/graph/TransOptPg2s.h"
#include "moses/graph/Pg2sHypothesis.h"
#include "moses/graph/Pg2sManager.h"


using namespace Moses::Graph;

namespace Moses
{

PhraseBasedFeatureContextPg2s::PhraseBasedFeatureContextPg2s(const Pg2sHypothesis* hypothesis) :
  m_hypothesis(hypothesis),
  m_translationOption(m_hypothesis->GetTranslationOption()),
  m_source(m_hypothesis->GetManager().GetSource()) {}

PhraseBasedFeatureContextPg2s::PhraseBasedFeatureContextPg2s
(const TransOptPg2s& translationOption, const InputType& source) :
  m_hypothesis(NULL),
  m_translationOption(translationOption),
  m_source(source)
{}

const TargetPhrase& PhraseBasedFeatureContextPg2s::GetTargetPhrase() const
{
  return m_translationOption.GetTargetPhrase();
}

const WordsBitmap& PhraseBasedFeatureContextPg2s::GetWordsBitmap() const
{
  if (!m_hypothesis) {
    throw std::logic_error("Coverage vector not available during pre-calculation");
  }
  return m_hypothesis->GetWordsBitmap();
}

}
