#include <boost/algorithm/string.hpp>

#include "GraphSegFeature.h"
#include "moses/AlignmentInfo.h"
#include "moses/TargetPhrase.h"
#include "moses/Hypothesis.h"
#include "moses/TranslationOption.h"
#include "util/string_piece_hash.hh"
#include "util/exception.hh"

#include "moses/graph/InputSubgraph.h"
#include "moses/graph/WordsSet.h"
#include "moses/graph/MultiDiGraphInput.h"
#include "moses/InputFileStream.h"
#include "moses/Util.h"
#include "moses/graph/TransOptPg2s.h"
#include "moses/graph/Pg2sHypothesis.h"
#include "moses/graph/G2sTranslationOption.h"
#include "moses/graph/G2sHypothesis.h"

using namespace std;
using namespace Moses::Graph;

namespace Moses
{

GraphSegFeature::GraphSegFeature(const std::string &line)
  :StatelessFeatureFunction("GraphSegFeature", 0, line),m_enableClass(false),m_enableVocab(false)
{
  std::cerr << "Initializing GraphSegFeature.." << std::endl;
  ReadParameters();
}

void GraphSegFeature::SetParameter(const std::string& key, const std::string& value)
{
	if (key == "vocab") {
		ReadVocab(m_vocab, value);
	} else if (key == "class") {
		ReadClass(m_class, value);
	} else if (key == "enable_class") {
	  m_enableClass = true;
    } else if (key == "enable_vocab") {
      m_enableVocab = true;
    } else {
      StatelessFeatureFunction::SetParameter(key, value);
    }
}

void GraphSegFeature::Evaluate(const PhraseBasedFeatureContextPg2s& context,
                ScoreComponentCollection* accumulator) const
{
  const MultiDiGraph& source = static_cast<const MultiDiGraph&>(context.GetSource());
  const set<int>& range = context.GetTranslationOption().GetSourceWordsRange().GetSet();
  const Pg2sHypothesis* prevHypo = context.GetHypothesis()->GetPrevHypo();
  boost::unordered_set<int, boost::hash<int> > histRange;
  const set<int>& prevRange = prevHypo ? prevHypo->GetCurrSourceWordsRange().GetSet() : set<int>();
  if (prevHypo) {
    const Pg2sHypothesis* histHypo = prevHypo->GetPrevHypo();
    if (histHypo) {
      const WordsBitmap& bitmap = histHypo->GetWordsBitmap();
      for (size_t i = 0; i < bitmap.GetSize(); i++) {
        if (bitmap.GetValue(i)) {
          histRange.insert((int)i);
        }
      }
    }
  }

  set<int>::const_iterator iterSet = range.begin();
  string direct = "", order="";
  for(; iterSet != range.end(); ++iterSet) {
    int nid = *iterSet;
    string word = GetVocab(source.get_word(nid));
    string cls = GetClass(source.get_word(nid));

    bool has_pred = source.has_pred(nid);
    bool has_succ = source.has_succ(nid);

    map<int, set<string> >::const_iterator iter2;
    if (has_pred)  {
      direct = "P";
      const map<int, set<string> > & pred = source.get_pred_edges(nid);
      iter2 = pred.begin();
      for (; iter2 != pred.end(); ++iter2) {
        int pid = iter2->first;
        if (prevRange.find(pid) != prevRange.end()) {
          order = "P";
        } else if (histRange.find(pid) != histRange.end()){
          order = "H";
        } else if (range.find(pid) != range.end()) {
          order = "C";
        } else {
          continue;
        }

        string pword = GetVocab(source.get_word(pid));
        string pcls = GetClass(source.get_word(pid));
        accumulator->PlusEquals(this, "WW@" + word+"@"+pword+"@"+direct+order, 1.0);
        accumulator->PlusEquals(this, "CC@" + cls+"@"+pcls+"@"+direct+order, 1.0);
        accumulator->PlusEquals(this, "WC@" + word+"@"+pcls+"@"+direct+order, 1.0);
        accumulator->PlusEquals(this, "CW@" + cls+"@"+pword+"@"+direct+order, 1.0);
      }
    }
    if (has_succ)  {
      direct = "S";
      const map<int, set<string> > & succ = source.get_succ_edges(nid);
      iter2 = succ.begin();
      for (; iter2 != succ.end(); ++iter2) {
        int sid = iter2->first;
        if (prevRange.find(sid) != prevRange.end()) {
          order = "P";
        } else if (histRange.find(sid) != histRange.end()){
          order = "H";
        } else if (range.find(sid) != range.end()) {
          order = "C";
        } else {
          continue;
        }
        string sword = GetVocab(source.get_word(sid));
        string scls = GetClass(source.get_word(sid));
        accumulator->PlusEquals(this, "WW@" + word+"@"+sword+"@"+direct+order, 1.0);
        accumulator->PlusEquals(this, "CC@" + cls+"@"+scls+"@"+direct+order, 1.0);
        accumulator->PlusEquals(this, "WC@" + word+"@"+scls+"@"+direct+order, 1.0);
        accumulator->PlusEquals(this, "CW@" + cls+"@"+sword+"@"+direct+order, 1.0);
      }
    }
  }
}

void GraphSegFeature::Evaluate(const PhraseBasedFeatureContextG2s& context,
                ScoreComponentCollection* accumulator) const
{
//TODO
}

bool GraphSegFeature::IsUseable(const FactorMask &mask) const
{
  return true;
}


void GraphSegFeature::ReadVocab(VocabType& vocab, const std::string& path) {
	string fileName = path;
  if(!FileExists(fileName) && FileExists(fileName+".gz")) {
    fileName += ".gz";
  }
  InputFileStream file(fileName);
  std::string line("");
  std::cerr << "Loading vocab into memory..." << path << endl;
  while(!getline(file, line).eof()) {
    vector<string> items = Tokenize(line);
    assert(items.size() == 3);
    vocab[items[1]] = items[2];
  }
  std::cerr << "done.\n";
}


void GraphSegFeature::ReadClass(VocabType& cls, const std::string& path) {
	string fileName = path;
  if(!FileExists(fileName) && FileExists(fileName+".gz")) {
    fileName += ".gz";
  }
  InputFileStream file(fileName);
  std::string line("");
  std::cerr << "Loading classes into memory..." << path << endl;
  while(!getline(file, line).eof()) {
    vector<string> items = Tokenize(line);
    assert(items.size() == 2);
    cls[items[0]] = items[1];
  }
  std::cerr << "done.\n";
}


}
