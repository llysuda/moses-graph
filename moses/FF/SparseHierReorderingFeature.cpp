#include <boost/algorithm/string.hpp>

#include "SparseHierReorderingFeature.h"
#include "moses/AlignmentInfo.h"
#include "moses/TargetPhrase.h"
#include "moses/Hypothesis.h"
#include "moses/TranslationOption.h"
#include "util/string_piece_hash.hh"
#include "util/exception.hh"

#include "moses/graph/InputSubgraph.h"
#include "moses/graph/WordsSet.h"
#include "moses/graph/MultiDiGraphInput.h"
#include "moses/InputFileStream.h"
#include "moses/Util.h"
#include "moses/graph/TransOptPg2s.h"
#include "moses/graph/Pg2sHypothesis.h"
#include "moses/graph/G2sTranslationOption.h"
#include "moses/graph/G2sHypothesis.h"

#include "moses/WordsRange.h"
#include "moses/WordsBitmap.h"

using namespace std;
using namespace Moses::Graph;

namespace Moses
{

SparseHierReorderingFeatureState::SparseHierReorderingFeatureState(const SparseHierReorderingFeatureState *prev, const WordsRange &range)
  : m_first(false), m_prevRange(range), m_coverage(prev->m_coverage)
{
  const WordsRange currWordsRange = range;
  m_coverage.SetValue(currWordsRange.GetStartPos(), currWordsRange.GetEndPos(), true);
}

SparseHierReorderingFeatureState::SparseHierReorderingFeatureState(size_t size)
  : m_first(true), m_prevRange(NOT_FOUND,NOT_FOUND), m_coverage(size) {}

int SparseHierReorderingFeatureState::Compare(const FFState& o) const
{
  if (&o == this)
      return 0;
  const SparseHierReorderingFeatureState* other = dynamic_cast<const SparseHierReorderingFeatureState*>(&o);
  CHECK(other != NULL);
  if (m_prevRange == other->m_prevRange) {
    return 0;
  } else if (m_prevRange < other->m_prevRange) {
    return -1;
  }
  return 1;
}

string SparseHierReorderingFeatureState::GetOrientationTypeMSLR(WordsRange currRange, WordsBitmap coverage) const
{
  if (currRange.GetStartPos() > m_prevRange.GetEndPos() &&
      (!coverage.GetValue(m_prevRange.GetEndPos()+1) || currRange.GetStartPos() == m_prevRange.GetEndPos()+1)) {
    return "M";
  } else if (currRange.GetEndPos() < m_prevRange.GetStartPos() &&
             (!coverage.GetValue(m_prevRange.GetStartPos()-1) || currRange.GetEndPos() == m_prevRange.GetStartPos()-1)) {
    return "S";
  } else if (currRange.GetStartPos() > m_prevRange.GetEndPos()) {
    return "DR";
  }
  return "DL";
}

string SparseHierReorderingFeatureState::GetOrientationTypeMSLRO(WordsRange currRange, WordsBitmap coverage) const
{
  if (currRange.GetStartPos() > m_prevRange.GetEndPos() &&
      (!coverage.GetValue(m_prevRange.GetEndPos()+1) || currRange.GetStartPos() == m_prevRange.GetEndPos()+1)) {
    return "M";
  } else if (currRange.GetEndPos() < m_prevRange.GetStartPos() &&
             (!coverage.GetValue(m_prevRange.GetStartPos()-1) || currRange.GetEndPos() == m_prevRange.GetStartPos()-1)) {
    return "S";
  } else if (currRange.GetStartPos() > m_prevRange.GetEndPos()) {
    return "DR";
  } else if (currRange.GetEndPos() < m_prevRange.GetStartPos()) {
    return "DL";
  }
  return "O";
}


//////////////////////////////////////////////
//////////////////////////////////////////////
//////////////////////////////////////////////


SparseHierReorderingFeature::SparseHierReorderingFeature(const std::string &line)
  :StatefulFeatureFunction("SparseHierReorderingFeature", 0, line),m_naive(false)
{
  std::cerr << "Initializing SparseHierReorderingFeature.." << std::endl;
  ReadParameters();
}

void SparseHierReorderingFeature::SetParameter(const std::string& key, const std::string& value)
{
	if (key == "vocab") {
		ReadVocab(m_vocab, value);
	} else if (key == "class") {
		ReadClass(m_class, value);
	} else if (key == "naive") {
	  m_naive = true;
    } else {
      StatefulFeatureFunction::SetParameter(key, value);
    }
}

const FFState* SparseHierReorderingFeature::EmptyHypothesisState(const InputType &input) const
{
  return new SparseHierReorderingFeatureState(input.GetSize());
}

FFState* SparseHierReorderingFeature::SparseHierReorderingFeature::Evaluate(
        const Moses::Graph::G2sHypothesis& cur_hypo,
        const FFState* prev_state,
        ScoreComponentCollection* accumulator) const
{
  return NULL;
//  const WordsRange& currWordsRange = cur_hypo.GetCurrSourceWordsRange().ToRange();
//  const SparseHierReorderingFeatureState* pstate = dynamic_cast<const SparseHierReorderingFeatureState*>(prev_state);
//  // keep track of the current coverage ourselves so we don't need the hypothesis
//  WordsBitmap coverage = pstate->m_coverage;
//  coverage.SetValue(currWordsRange.GetStartPos(), currWordsRange.GetEndPos(), true);
//  string reoType = pstate->GetOrientationTypeMSLRO(currWordsRange, coverage);
//  if (m_naive) {
//    reoType = pstate->GetOrientationTypeMSLR(currWordsRange, coverage);
//  }
//  // going left or going down
//  string direction = "R";
//  if (!m_naive && cur_hypo.GetPrevPointHypo() != NULL)
//    direction = "D";
//  string suffix = "_"+direction + "_" + reoType;
//  // source boundary words and class
//  const MultiDiGraph& source = static_cast<const MultiDiGraph&>(cur_hypo.GetInput());
//  const Phrase& targetPhrase = cur_hypo.GetCurrTargetPhrase();
//  string src_first_word = source.get_word(currWordsRange.GetStartPos());
//    string sfw = "SFW@" + GetVocab(src_first_word) + suffix;
//    string sfc = "SFC@" + GetClass(src_first_word) + suffix;
//  string src_last_word = source.get_word(currWordsRange.GetEndPos());
//    string slw = "SLW@" + GetVocab(src_last_word) + suffix;
//    string slc = "SLC@" + GetClass(src_last_word) + suffix;
//  string tgt_first_word = targetPhrase.GetWord(0).GetString(0).as_string();
//    string tfw = "TFW@" + GetVocab(tgt_first_word) + suffix;
//    string tfc = "TFC@" + GetClass(tgt_first_word) + suffix;
//  string tgt_last_word = targetPhrase.GetWord(targetPhrase.GetSize()-1).GetString(0).as_string();
//    string tlw = "TLW@" + GetVocab(tgt_last_word) + suffix;
//    string tlc = "TLC@" + GetClass(tgt_last_word) + suffix;
//
//  accumulator->PlusEquals(this, sfw, 1.0);
//  accumulator->PlusEquals(this, sfc, 1.0);
//  accumulator->PlusEquals(this, slw, 1.0);
//  accumulator->PlusEquals(this, slc, 1.0);
//  accumulator->PlusEquals(this, tfw, 1.0);
//  accumulator->PlusEquals(this, tfc, 1.0);
//  accumulator->PlusEquals(this, tlw, 1.0);
//  accumulator->PlusEquals(this, tlc, 1.0);
//
//  if (pstate->m_prevRange.GetStartPos() != NOT_FOUND){
//    string top_src_first_word = source.get_word(pstate->m_prevRange.GetStartPos());
//      string tsfw = "TSFW@" + GetVocab(top_src_first_word) + suffix;
//      string tsfc = "TSFC@" + GetClass(top_src_first_word);
//    string top_src_last_word = source.get_word(pstate->m_prevRange.GetEndPos());
//      string tslw = "TSLW@" + GetVocab(top_src_last_word) + suffix;
//      string tslc = "TSLC@" + GetClass(top_src_last_word) + suffix;
//    accumulator->PlusEquals(this, tsfw, 1.0);
//    accumulator->PlusEquals(this, tsfc, 1.0);
//    accumulator->PlusEquals(this, tslw, 1.0);
//    accumulator->PlusEquals(this, tslc, 1.0);
//  }
//
//  if (cur_hypo.GetPrevPointHypo() != NULL) {
//    const Phrase& tp = cur_hypo.GetPrevPointHypo()->GetCurrTargetPhrase();
//    size_t pos = cur_hypo.GetPrevPointPos();
//    string top_tgt_last_word = tp.GetWord(pos-1).GetString(0).as_string();
//      string ttlw = "TTLW@" + GetVocab(top_tgt_last_word) + suffix;
//      string ttlc = "TTLC@" + GetClass(top_tgt_last_word) + suffix;
//    string top_tgt_next_word = tp.GetWord(pos+1).GetString(0).as_string();
//      string ttnw = "TTNW@" + GetVocab(top_tgt_next_word) + suffix;
//      string ttnc = "TTNC@" + GetClass(top_tgt_next_word) + suffix;
//    accumulator->PlusEquals(this, ttlw, 1.0);
//    accumulator->PlusEquals(this, ttlc, 1.0);
//    accumulator->PlusEquals(this, ttnw, 1.0);
//    accumulator->PlusEquals(this, ttnc, 1.0);
//  } else if (!pstate->m_first){
//    const Phrase& tp = cur_hypo.GetPrevHypo()->GetCurrTargetPhrase();
//    string top_tgt_last_word = tp.GetWord(tp.GetSize()-1).GetString(0).as_string();
//      string ttlw = "TTLW@" + GetVocab(top_tgt_last_word);
//      string ttlc = "TTLC@" + GetClass(top_tgt_last_word);
//    accumulator->PlusEquals(this, ttlw, 1.0);
//    accumulator->PlusEquals(this, ttlc, 1.0);
//  } else {
//    //TODO
//  }
//  return new SparseHierReorderingFeatureState(pstate, cur_hypo.GetCurrSourceWordsRange().ToRange());
}

FFState* SparseHierReorderingFeature::SparseHierReorderingFeature::Evaluate(
        const Moses::Graph::Pg2sHypothesis& cur_hypo,
        const FFState* prev_state,
        ScoreComponentCollection* accumulator) const
{
  const WordsRange& currWordsRange = cur_hypo.GetCurrSourceWordsRange().ToRange();
  const SparseHierReorderingFeatureState* pstate = dynamic_cast<const SparseHierReorderingFeatureState*>(prev_state);
  // keep track of the current coverage ourselves so we don't need the hypothesis
  WordsBitmap coverage = pstate->m_coverage;
  coverage.SetValue(currWordsRange.GetStartPos(), currWordsRange.GetEndPos(), true);
  string reoType = pstate->GetOrientationTypeMSLRO(currWordsRange, coverage);
  if (m_naive) {
    reoType = pstate->GetOrientationTypeMSLR(currWordsRange, coverage);
  }

  string suffix = "_" + reoType;
  // source boundary words and class
  const MultiDiGraph& source = static_cast<const MultiDiGraph&>(cur_hypo.GetInput());
  const Phrase& targetPhrase = cur_hypo.GetCurrTargetPhrase();
  string src_first_word = source.get_word(currWordsRange.GetStartPos());
    string sfw = "SFW@" + GetVocab(src_first_word) + suffix;
    string sfc = "SFC@" + GetClass(src_first_word) + suffix;
  string src_last_word = source.get_word(currWordsRange.GetEndPos());
    string slw = "SLW@" + GetVocab(src_last_word) + suffix;
    string slc = "SLC@" + GetClass(src_last_word) + suffix;
  string tgt_first_word = targetPhrase.GetWord(0).GetString(0).as_string();
    string tfw = "TFW@" + GetVocab(tgt_first_word) + suffix;
    string tfc = "TFC@" + GetClass(tgt_first_word) + suffix;
  string tgt_last_word = targetPhrase.GetWord(targetPhrase.GetSize()-1).GetString(0).as_string();
    string tlw = "TLW@" + GetVocab(tgt_last_word) + suffix;
    string tlc = "TLC@" + GetClass(tgt_last_word) + suffix;

  accumulator->PlusEquals(this, sfw, 1.0);
  accumulator->PlusEquals(this, sfc, 1.0);
  accumulator->PlusEquals(this, slw, 1.0);
  accumulator->PlusEquals(this, slc, 1.0);
  accumulator->PlusEquals(this, tfw, 1.0);
  accumulator->PlusEquals(this, tfc, 1.0);
  accumulator->PlusEquals(this, tlw, 1.0);
  accumulator->PlusEquals(this, tlc, 1.0);

  if (pstate->m_prevRange.GetStartPos() != NOT_FOUND){
    string top_src_first_word = source.get_word(pstate->m_prevRange.GetStartPos());
      string tsfw = "TSFW@" + GetVocab(top_src_first_word) + suffix;
      string tsfc = "TSFC@" + GetClass(top_src_first_word);
    string top_src_last_word = source.get_word(pstate->m_prevRange.GetEndPos());
      string tslw = "TSLW@" + GetVocab(top_src_last_word) + suffix;
      string tslc = "TSLC@" + GetClass(top_src_last_word) + suffix;
    accumulator->PlusEquals(this, tsfw, 1.0);
    accumulator->PlusEquals(this, tsfc, 1.0);
    accumulator->PlusEquals(this, tslw, 1.0);
    accumulator->PlusEquals(this, tslc, 1.0);
  }

  if (!pstate->m_first){
    const Phrase& tp = cur_hypo.GetPrevHypo()->GetCurrTargetPhrase();
    string top_tgt_last_word = tp.GetWord(tp.GetSize()-1).GetString(0).as_string();
      string ttlw = "TTLW@" + GetVocab(top_tgt_last_word);
      string ttlc = "TTLC@" + GetClass(top_tgt_last_word);
    accumulator->PlusEquals(this, ttlw, 1.0);
    accumulator->PlusEquals(this, ttlc, 1.0);
  } else {
    //TODO
  }
  return new SparseHierReorderingFeatureState(pstate, cur_hypo.GetCurrSourceWordsRange().ToRange());
}

bool SparseHierReorderingFeature::IsUseable(const FactorMask &mask) const
{
  return true;
}


void SparseHierReorderingFeature::ReadVocab(VocabType& vocab, const std::string& path) {
	string fileName = path;
  if(!FileExists(fileName) && FileExists(fileName+".gz")) {
    fileName += ".gz";
  }
  InputFileStream file(fileName);
  std::string line("");
  std::cerr << "Loading vocab into memory..." << path << endl;
  while(!getline(file, line).eof()) {
    vector<string> items = Tokenize(line);
    assert(items.size() == 3);
    vocab[items[1]] = items[2];
  }
  std::cerr << "done.\n";
}


void SparseHierReorderingFeature::ReadClass(VocabType& cls, const std::string& path) {
	string fileName = path;
  if(!FileExists(fileName) && FileExists(fileName+".gz")) {
    fileName += ".gz";
  }
  InputFileStream file(fileName);
  std::string line("");
  std::cerr << "Loading classes into memory..." << path << endl;
  while(!getline(file, line).eof()) {
    vector<string> items = Tokenize(line);
    assert(items.size() == 2);
    cls[items[0]] = items[1];
  }
  std::cerr << "done.\n";
}


}
