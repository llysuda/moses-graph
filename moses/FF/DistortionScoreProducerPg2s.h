#pragma once

#include <stdexcept>
#include <string>
#include <set>
#include <vector>
#include "StatefulFeatureFunction.h"
#include "util/check.hh"

namespace Moses
{
class FFState;
class ScoreComponentCollection;
class Hypothesis;
class ChartHypothesis;
class WordsRange;

namespace Graph
{
class Pg2sHypothesis;
}

/** Calculates Distortion scores
 */
class DistortionScoreProducerPg2s : public StatefulFeatureFunction
{
public:
  DistortionScoreProducerPg2s(const std::string &line);

  bool IsUseable(const FactorMask &mask) const {
    return true;
  }

  static std::vector<float> CalculateDistortionScore(const Moses::Graph::Pg2sHypothesis& hypo,
                                        int prevEnd, const std::set<int> &curr, bool naive);

  virtual const FFState* EmptyHypothesisState(const InputType &input) const;

  virtual FFState* Evaluate(
    const Moses::Graph::Pg2sHypothesis& cur_hypo,
    const FFState* prev_state,
    ScoreComponentCollection* accumulator) const;

  virtual FFState* Evaluate(
      const Hypothesis& cur_hypo,
      const FFState* prev_state,
      ScoreComponentCollection* accumulator) const {
  	throw std::logic_error("DistortionScoreProducerPg2s not supported in pb decoder, yet");
  }

  virtual FFState* EvaluateChart(
    const ChartHypothesis& /* cur_hypo */,
    int /* featureID - used to index the state in the previous hypotheses */,
    ScoreComponentCollection*) const {
    throw std::logic_error("DistortionScoreProducerPg2s not supported in chart decoder, yet");
  }

//  static float CalculateDistortionScore(const Moses::Graph::Pg2sHypothesis& hypo,
//                                          const std::set<int> &prev, const std::set<int> &curr, const int FirstGapPosition);
//  virtual FFState* Evaluate(
//        const Moses::Graph::Pg2sHypothesis& cur_hypo,
//        const FFState* prev_state,
//        ScoreComponentCollection* accumulator) const;

};
}

