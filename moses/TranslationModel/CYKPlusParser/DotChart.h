// $Id$
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/
#pragma once

#include "moses/ChartCellLabel.h"
#include <string>
#include <vector>
#include "moses/Word.h"
#include "moses/Phrase.h"

namespace Moses
{

/** @todo what is this?
 */
class DottedRule
{
public:
  // used only to init dot stack.
  DottedRule()
    : m_cellLabel(NULL)
    , m_prev(NULL)
		, m_sourceLabel(NULL){}

  DottedRule(const ChartCellLabel &ccl, const DottedRule &prev)
    : m_cellLabel(&ccl)
    , m_prev(&prev)
  	, m_sourceLabel(NULL){}

  const WordsRange &GetWordsRange() const {
    return m_cellLabel->GetCoverage();
  }
  /*const Word &GetSourceWord() const {
    return m_cellLabel->GetLabel();
  }*/
  bool IsNonTerminal() const {
    return m_cellLabel->GetLabel().IsNonTerminal();
  }
  const DottedRule *GetPrev() const {
    return m_prev;
  }
  bool IsRoot() const {
    return m_prev == NULL;
  }
  const ChartCellLabel &GetChartCellLabel() const {
    return *m_cellLabel;
  }

/*  const std::string& GetLabel2Serg() const {
  	return m_label4serg;
  }
  void SetLabel2Serg(const std::string& label) {
		m_label4serg = label;
	}
*/
  std::vector<const WordsRange*> GetSegments() const {
  	std::vector<const WordsRange*> seqs;
  	const DottedRule *prev = this;
  	while (prev->GetPrev() != NULL) {
  		const WordsRange &range = prev->GetWordsRange();
  		if (range.GetNumWordsCovered() > 1)
  			seqs.insert(seqs.begin(),&range);
  		prev = prev->GetPrev();
  	}
  	return seqs;
  }

  std::vector<std::pair<const WordsRange*, const Word*> > GetSegmentsWithLabel() const {
  	std::vector<std::pair<const WordsRange*, const Word*> > seqs;
  	const DottedRule *prev = this;
  	while (prev->GetPrev() != NULL) {
  		const WordsRange &range = prev->GetWordsRange();
  		if (range.GetNumWordsCovered() > 1)
  			seqs.insert(seqs.begin(),std::make_pair(&range, &(prev->GetSourceLabel())));
  		prev = prev->GetPrev();
  	}
  	return seqs;
  }

  const Word& GetSourceLabel() const {
  	return *m_sourceLabel;
  }

  void SetSouceLabel(const Word& word) {
  	CHECK(m_sourceLabel == NULL);
  	m_sourceLabel = &word;
  }

private:
  const ChartCellLabel *m_cellLabel; // usually contains something, unless
  // it's the init processed rule
  const DottedRule *m_prev;
  const Word* m_sourceLabel;
  //std::string m_label4serg;
};

}
