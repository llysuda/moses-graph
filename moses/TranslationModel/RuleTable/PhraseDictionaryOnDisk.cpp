// vim:tabstop=2
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include "PhraseDictionaryOnDisk.h"
#include "moses/InputFileStream.h"
#include "moses/StaticData.h"
#include "moses/TargetPhraseCollection.h"
#include "moses/InputPath.h"
#include "moses/TranslationModel/CYKPlusParser/DotChartOnDisk.h"
#include "moses/TranslationModel/CYKPlusParser/ChartRuleLookupManagerOnDisk.h"
#include "moses/graph/BeamPlusParser/G2sRuleLookupManagerOnDisk.h"
#include "OnDiskPt/OnDiskWrapper.h"
#include "OnDiskPt/Word.h"

using namespace std;
using namespace Moses::Graph;

namespace Moses
{
PhraseDictionaryOnDisk::PhraseDictionaryOnDisk(const std::string &line)
  : MyBase("PhraseDictionaryOnDisk", line)
{
  ReadParameters();
}

PhraseDictionaryOnDisk::~PhraseDictionaryOnDisk()
{
}

void PhraseDictionaryOnDisk::Load()
{
  SetFeaturesToApply();
}

ChartRuleLookupManager *PhraseDictionaryOnDisk::CreateRuleLookupManager(
  const ChartParser &parser,
  const ChartCellCollectionBase &cellCollection)
{
  return new ChartRuleLookupManagerOnDisk(parser, cellCollection, *this,
                                          GetImplementation(),
                                          m_input,
                                          m_output, m_filePath);
}

G2sRuleLookupManager *PhraseDictionaryOnDisk::CreateRuleLookupManager(
  const G2sParser &parser)
{
  return new G2sRuleLookupManagerOnDisk(parser, *this, GetImplementation(),
																				m_input, m_output, m_filePath);
}

OnDiskPt::OnDiskWrapper &PhraseDictionaryOnDisk::GetImplementation()
{
  OnDiskPt::OnDiskWrapper* dict;
  dict = m_implementation.get();
  CHECK(dict);
  return *dict;
}

const OnDiskPt::OnDiskWrapper &PhraseDictionaryOnDisk::GetImplementation() const
{
  OnDiskPt::OnDiskWrapper* dict;
  dict = m_implementation.get();
  CHECK(dict);
  return *dict;
}

void PhraseDictionaryOnDisk::InitializeForInput(InputType const& source)
{
  const StaticData &staticData = StaticData::Instance();

  ReduceCache();

  OnDiskPt::OnDiskWrapper *obj = new OnDiskPt::OnDiskWrapper();
  if (!obj->BeginLoad(m_filePath))
    return;

  CHECK(obj->GetMisc("Version") == OnDiskPt::OnDiskWrapper::VERSION_NUM);
  CHECK(obj->GetMisc("NumSourceFactors") == m_input.size());
  CHECK(obj->GetMisc("NumTargetFactors") == m_output.size());
  CHECK(obj->GetMisc("NumScores") == m_numScoreComponents);

  m_implementation.reset(obj);
}

void PhraseDictionaryOnDisk::GetTargetPhraseCollectionBatch(const InputPathList &inputPathQueue) const
{
  InputPathList::const_iterator iter;
  for (iter = inputPathQueue.begin(); iter != inputPathQueue.end(); ++iter) {
    InputPath &inputPath = **iter;
    GetTargetPhraseCollectionBatch(inputPath);
  }
}

void PhraseDictionaryOnDisk::GetTargetPhraseCollectionBatch(InputPath &inputPath) const
{
    OnDiskPt::OnDiskWrapper &wrapper = const_cast<OnDiskPt::OnDiskWrapper&>(GetImplementation());
    const Phrase &phrase = inputPath.GetPhrase();
    const InputPath *prevInputPath = inputPath.GetPrevNode();

    const OnDiskPt::PhraseNode *prevPtNode = NULL;

    if (prevInputPath) {
      prevPtNode = static_cast<const OnDiskPt::PhraseNode*>(prevInputPath->GetPtNode(*this));
    } else {
      // Starting subphrase.
      assert(phrase.GetSize() == 1);
      prevPtNode = &wrapper.GetRootSourceNode();
    }

    if (prevPtNode) {
      Word lastWord = phrase.GetWord(phrase.GetSize() - 1);
      lastWord.OnlyTheseFactors(m_inputFactors);
      OnDiskPt::Word *lastWordOnDisk = wrapper.ConvertFromMoses(m_input, lastWord);

      if (lastWordOnDisk == NULL) {
        // OOV according to this phrase table. Not possible to extend
        inputPath.SetTargetPhrases(*this, NULL, NULL);
      } else {
        const OnDiskPt::PhraseNode *ptNode = prevPtNode->GetChild(*lastWordOnDisk, wrapper);
        if (ptNode) {
        	const TargetPhraseCollection *targetPhrases = GetTargetPhraseCollection(ptNode);
            inputPath.SetTargetPhrases(*this, targetPhrases, ptNode);
        } else {
          inputPath.SetTargetPhrases(*this, NULL, NULL);
        }

        delete lastWordOnDisk;
      }
    }
}

const TargetPhraseCollection *PhraseDictionaryOnDisk::GetTargetPhraseCollection(const OnDiskPt::PhraseNode *ptNode) const
{
	  const TargetPhraseCollection *ret;
	  if (m_maxCacheSize) {
	    size_t hash = (size_t) ptNode->GetFilePos();

	    std::map<size_t, std::pair<const TargetPhraseCollection*, clock_t> >::iterator iter;

	    {
	      // scope of read lock
	#ifdef WITH_THREADS
	      boost::shared_lock<boost::shared_mutex> read_lock(m_accessLock);
	#endif
	      iter = m_cache.find(hash);
	    }

	    if (iter == m_cache.end()) {
          // not in cache, need to look up from phrase table
	      ret = GetTargetPhraseCollectionNonCache(ptNode);
	      if (ret) {
	        ret = new TargetPhraseCollection(*ret);
	      }

	      std::pair<const TargetPhraseCollection*, clock_t> value(ret, clock());

	#ifdef WITH_THREADS
	      boost::unique_lock<boost::shared_mutex> lock(m_accessLock);
	#endif
	      m_cache[hash] = value;
	    }
	    else {
	    	// in cache. just use it
	    	std::pair<const TargetPhraseCollection*, clock_t> &value = iter->second;
	    	value.second = clock();

	        ret = value.first;
	    }
	  } else {
	    ret = GetTargetPhraseCollectionNonCache(ptNode);
	  }

	  return ret;
}

const TargetPhraseCollection *PhraseDictionaryOnDisk::GetTargetPhraseCollectionNonCache(const OnDiskPt::PhraseNode *ptNode) const
{
    OnDiskPt::OnDiskWrapper &wrapper = const_cast<OnDiskPt::OnDiskWrapper&>(GetImplementation());

    vector<float> weightT = StaticData::Instance().GetWeights(this);
    OnDiskPt::Vocab &vocab = wrapper.GetVocab();

    const OnDiskPt::TargetPhraseCollection *targetPhrasesOnDisk = ptNode->GetTargetPhraseCollection(m_tableLimit, wrapper);
    TargetPhraseCollection *targetPhrases
    	= targetPhrasesOnDisk->ConvertToMoses(m_input, m_output, *this, weightT, vocab, false);

    delete targetPhrasesOnDisk;

    return targetPhrases;
}

void PhraseDictionaryOnDisk::GetTargetPhraseCollectionBatch(const WordsSet&,
    		const OnDiskPt::PhraseNode*,
    		Moses::Graph::InputSubgraphList &phraseDictionaryQueue,
    		const Moses::Graph::MultiDiGraph& source,
				OnDiskPt::OnDiskWrapper& wrapper) const
{
	const StaticData& staticData = StaticData::Instance();
	bool incLabel = staticData.GetIncLabel();
	bool sDTU = staticData.GetSDTU();
	int size = (int)source.GetSize();

	string gapStr = "X";
	Word* gap = new Word();
	gap->SetFactor(0, FactorCollection::Instance().AddFactor(gapStr));
	OnDiskPt::Word *gapDisk = wrapper.ConvertFromMoses(m_input, *gap);

	vector<const OnDiskPt::PhraseNode*> allnodes;

	typedef pair<WordsSet, const OnDiskPt::PhraseNode*> PairType;
	vector< PairType > stack;
	WordsSet initSet;
	const OnDiskPt::PhraseNode* root = &wrapper.GetRootSourceNode();
	stack.push_back(PairType(initSet, root));
	while (stack.size() > 0) {
		PairType instance = stack.back();
		stack.pop_back();

		const WordsSet& range = instance.first;
		const OnDiskPt::PhraseNode* ptNode = instance.second;

		int i = 0;
		if (range.GetSize()>0)
			i = range.GetMax() + 1;

		const OnDiskPt::PhraseNode *ptNodeGap = NULL;
		if (sDTU && gapDisk != NULL) {
			ptNodeGap = ptNode->GetChild(*gapDisk, wrapper);
			allnodes.push_back(ptNodeGap);
		}

		int start = i;
		for(; i < size; i++) {
			const OnDiskPt::PhraseNode *prevNode = ptNode;
			if (start > 0 && sDTU && i >= start+1) {
				if (ptNodeGap == NULL)
					break;
				prevNode = ptNodeGap;
			}

			Word lastWord = source.GetWord(i);
			OnDiskPt::Word *lastWordOnDisk = wrapper.ConvertFromMoses(m_input, lastWord);
			if (lastWordOnDisk == NULL) {
				continue;
			}
			const OnDiskPt::PhraseNode * ptNode2 = prevNode->GetChild(*lastWordOnDisk, wrapper);
			if (ptNode2) {
			    allnodes.push_back(ptNode2);
				WordsSet nids2(range);
				nids2.Add(i, nids2.GetSize());
				if (sDTU || source.is_connected(nids2.GetSet())) {
					if (sDTU) {
						const TargetPhraseCollection* targetPhrases = GetTargetPhraseCollection(ptNode2);
						if(targetPhrases->GetSize() > 0) {
							Phrase p;
							for (set<int>::const_iterator iter=nids2.GetSet().begin(); iter!=nids2.GetSet().end(); iter++)
								p.AddWord(source.GetWord(*iter));
							NonTerminalSet nt;
							InputSubgraph* inputPath = new InputSubgraph(p, nt, nids2);
							inputPath->SetTargetPhrases(*this, targetPhrases, NULL);
							phraseDictionaryQueue.push_back(inputPath);
						}
					} else {
						// is a connected subgraph
						string structStr = source.get_structure_string(nids2.GetSortedVec(), incLabel);
						structStr = "X_"+structStr;

							Word labelWord(true);
							labelWord.SetFactor(0, FactorCollection::Instance().AddFactor(structStr));
							OnDiskPt::Word *labelWordOnDisk = wrapper.ConvertFromMoses(m_input, labelWord);

							if (labelWordOnDisk == NULL) {
								delete lastWordOnDisk;
								continue;
							}

							const OnDiskPt::PhraseNode *ptNode3 = ptNode2->GetChild(*labelWordOnDisk, wrapper);
							if (ptNode3) {
							    allnodes.push_back(ptNode3);
								const TargetPhraseCollection* targetPhrases = GetTargetPhraseCollection(ptNode3);
								if (targetPhrases->GetSize() > 0) {
									Phrase p;
									for (set<int>::const_iterator iter=nids2.GetSet().begin(); iter!=nids2.GetSet().end(); iter++)
										p.AddWord(source.GetWord(*iter));
									p.AddWord(labelWord);
									NonTerminalSet nt;
									InputSubgraph* inputPath = new InputSubgraph(p, nt, nids2);
									inputPath->SetTargetPhrases(*this, targetPhrases, NULL);
									phraseDictionaryQueue.push_back(inputPath);
								}
							}
							delete labelWordOnDisk;
						}
				}

				if (nids2.GetSize() < staticData.GetMaxPhraseLength())
					stack.push_back(PairType(nids2,ptNode2));
			}
			delete lastWordOnDisk;
		}
	}
	delete gap;
	if (gapDisk)
		delete gapDisk;
	while (allnodes.begin() != allnodes.end()) {
	  const OnDiskPt::PhraseNode *node = *(allnodes.begin());
	  allnodes.erase(allnodes.begin());
	  delete node;
	}
}

void PhraseDictionaryOnDisk::GetTargetPhraseCollectionBatch(Moses::Graph::InputSubgraphList &phraseDictionaryQueue, const InputType& source) const
{
	//TODO
	const MultiDiGraph &m_source = static_cast<const MultiDiGraph&>(source);
	OnDiskPt::OnDiskWrapper &wrapper = const_cast<OnDiskPt::OnDiskWrapper&>(GetImplementation());
	WordsSet nids;
	const OnDiskPt::PhraseNode* root = &wrapper.GetRootSourceNode();
	GetTargetPhraseCollectionBatch(nids, root, phraseDictionaryQueue, m_source, wrapper);
}

} // namespace

