// vim:tabstop=2

/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2006 University of Edinburgh

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include <fstream>
#include <string>
#include <iterator>
#include <algorithm>
#include "PhraseDictionaryMemory.h"
#include "moses/FactorCollection.h"
#include "moses/Word.h"
#include "moses/Util.h"
#include "moses/InputFileStream.h"
#include "moses/StaticData.h"
#include "moses/WordsRange.h"
#include "moses/UserMessage.h"
#include "moses/TranslationModel/RuleTable/LoaderFactory.h"
#include "moses/TranslationModel/RuleTable/Loader.h"
#include "moses/TranslationModel/CYKPlusParser/ChartRuleLookupManagerMemory.h"
#include "moses/InputPath.h"

#include "moses/Phrase.h"

#include "moses/graph/BeamPlusParser/G2sRuleLookupManagerMemory.h"

using namespace std;
using namespace Moses::Graph;

namespace Moses
{
PhraseDictionaryMemory::PhraseDictionaryMemory(const std::string &line)
  : RuleTableTrie("PhraseDictionaryMemory", line)
{
  ReadParameters();

  // caching for memory pt is pointless
  m_maxCacheSize = 0;

}

TargetPhraseCollection &PhraseDictionaryMemory::GetOrCreateTargetPhraseCollection(
  const Phrase &source
  , const TargetPhrase &target
  , const Word *sourceLHS)
{
  PhraseDictionaryNodeMemory &currNode = GetOrCreateNode(source, target, sourceLHS);
  return currNode.GetTargetPhraseCollection();
}

const TargetPhraseCollection *PhraseDictionaryMemory::GetTargetPhraseCollection(const Phrase& sourceOrig) const
{
  Phrase source(sourceOrig);
  source.OnlyTheseFactors(m_inputFactors);

  // exactly like CreateTargetPhraseCollection, but don't create
  const size_t size = source.GetSize();

  const PhraseDictionaryNodeMemory *currNode = &m_collection;
  for (size_t pos = 0 ; pos < size ; ++pos) {
    const Word& word = source.GetWord(pos);
    currNode = currNode->GetChild(word);
    if (currNode == NULL)
      return NULL;
  }

  return &currNode->GetTargetPhraseCollection();
}

PhraseDictionaryNodeMemory &PhraseDictionaryMemory::GetOrCreateNode(const Phrase &source
    , const TargetPhrase &target
    , const Word *sourceLHS)
{
  const size_t size = source.GetSize();

  const AlignmentInfo &alignmentInfo = target.GetAlignNonTerm();
  AlignmentInfo::const_iterator iterAlign = alignmentInfo.begin();

  PhraseDictionaryNodeMemory *currNode = &m_collection;
  for (size_t pos = 0 ; pos < size ; ++pos) {
    const Word& word = source.GetWord(pos);

    if (word.IsNonTerminal()) {
      // indexed by source label 1st
      const Word &sourceNonTerm = word;

      CHECK(iterAlign != alignmentInfo.end());
      CHECK(iterAlign->first == pos);
      size_t targetNonTermInd = iterAlign->second;
      ++iterAlign;
      const Word &targetNonTerm = target.GetWord(targetNonTermInd);

      currNode = currNode->GetOrCreateChild(sourceNonTerm, targetNonTerm);
    } else {
      currNode = currNode->GetOrCreateChild(word);
    }

    CHECK(currNode != NULL);
  }

  const StaticData& staticData = StaticData::Instance();
  if (staticData.GetInputType() == MultiDiGraphInput
			&& !staticData.GetSDTU()) {

  	Word label(*sourceLHS);
  	label.SetIsNonTerminal(false);
  	currNode = currNode->GetOrCreateChild(label);
  }

  return *currNode;
}

ChartRuleLookupManager *PhraseDictionaryMemory::CreateRuleLookupManager(
  const ChartParser &parser,
  const ChartCellCollectionBase &cellCollection)
{
  return new ChartRuleLookupManagerMemory(parser, cellCollection, *this);
}

G2sRuleLookupManager *PhraseDictionaryMemory::CreateRuleLookupManager(
  const G2sParser &parser)
{
  return new G2sRuleLookupManagerMemory(parser, *this);
}

void PhraseDictionaryMemory::SortAndPrune()
{
  if (GetTableLimit()) {
    m_collection.Sort(GetTableLimit());
  }
}

void PhraseDictionaryMemory::GetTargetPhraseCollectionBatch(const InputPathList &phraseDictionaryQueue) const
{
  InputPathList::const_iterator iter;
  for (iter = phraseDictionaryQueue.begin(); iter != phraseDictionaryQueue.end(); ++iter) {
    InputPath &node = **iter;
    const Phrase &phrase = node.GetPhrase();
    const InputPath *prevNode = node.GetPrevNode();

    const PhraseDictionaryNodeMemory *prevPtNode = NULL;

    if (prevNode) {
      prevPtNode = static_cast<const PhraseDictionaryNodeMemory*>(prevNode->GetPtNode(*this));
    } else {
      // Starting subphrase.
      assert(phrase.GetSize() == 1);
      prevPtNode = &GetRootNode();
    }

    if (prevPtNode) {
      Word lastWord = phrase.GetWord(phrase.GetSize() - 1);
      lastWord.OnlyTheseFactors(m_inputFactors);

      const PhraseDictionaryNodeMemory *ptNode = prevPtNode->GetChild(lastWord);
      if (ptNode) {
        const TargetPhraseCollection &targetPhrases = ptNode->GetTargetPhraseCollection();
        node.SetTargetPhrases(*this, &targetPhrases, ptNode);
      } else {
        node.SetTargetPhrases(*this, NULL, NULL);
      }
    }
  }
}

void PhraseDictionaryMemory::GetTargetPhraseCollectionBatch(WordsSet&,
		const PhraseDictionaryNodeMemory*,
		InputSubgraphList &phraseDictionaryQueue,
		const MultiDiGraph& source) const
{
	const StaticData& staticData = StaticData::Instance();
	bool incLabel = staticData.GetIncLabel();
	bool sDTU = staticData.GetSDTU();
	int size = (int)source.GetSize();

	string gapStr = "X";
	Word* gap = new Word();
	gap->SetFactor(0, FactorCollection::Instance().AddFactor(gapStr));

	typedef pair<WordsSet, const PhraseDictionaryNodeMemory*> PairType;
	vector< PairType > stack;
	WordsSet initSet;
	const PhraseDictionaryNodeMemory* root = &GetRootNode();
	stack.push_back(PairType(initSet, root));
	while (stack.size() > 0) {
		PairType instance = stack.back();
		stack.pop_back();

		const WordsSet& range = instance.first;
		const PhraseDictionaryNodeMemory* ptNode = instance.second;

		int i = 0;
		if (range.GetSize()>0)
			i = range.GetMax() + 1;

		const PhraseDictionaryNodeMemory *ptNodeGap = NULL;
		if (sDTU) {
			ptNodeGap = ptNode->GetChild(*gap);
		}

		int start = i;
		for(; i < size; i++) {
			const PhraseDictionaryNodeMemory *prevNode = ptNode;
			if (start > 0 && sDTU && i >= start+1) {
				if (ptNodeGap == NULL)
					break;
				prevNode = ptNodeGap;
			}

			const Word& lastWord = source.GetWord(i);
			const PhraseDictionaryNodeMemory * ptNode2 = prevNode->GetChild(lastWord);
			if (ptNode2) {
				WordsSet nids2(range);
				nids2.Add(i, nids2.GetSize());
				if (sDTU || source.is_connected(nids2.GetSet())) {
					if (sDTU) {
						const TargetPhraseCollection& targetPhrases = ptNode2->GetTargetPhraseCollection();
						if(targetPhrases.GetSize() > 0) {
							Phrase p;
							for (set<int>::const_iterator iter=nids2.GetSet().begin(); iter!=nids2.GetSet().end(); iter++)
								p.AddWord(source.GetWord(*iter));
							NonTerminalSet nt;
							InputSubgraph* inputPath = new InputSubgraph(p, nt, nids2);
							inputPath->SetTargetPhrases(*this, &targetPhrases, ptNode2);
							phraseDictionaryQueue.push_back(inputPath);
						}
					} else {
						// is a connected subgraph
						string structStr = source.get_structure_string(nids2.GetSortedVec(), incLabel);

							Word labelWord;
							labelWord.SetFactor(0, FactorCollection::Instance().AddFactor(structStr));

							const PhraseDictionaryNodeMemory *ptNode3 = ptNode2->GetChild(labelWord);
							if (ptNode3) {
								const TargetPhraseCollection& targetPhrases = ptNode3->GetTargetPhraseCollection();
								if (targetPhrases.GetSize() > 0) {
									Phrase p;
									for (set<int>::const_iterator iter=nids2.GetSet().begin(); iter!=nids2.GetSet().end(); iter++)
										p.AddWord(source.GetWord(*iter));
									p.AddWord(labelWord);
									NonTerminalSet nt;
									InputSubgraph* inputPath = new InputSubgraph(p, nt, nids2);
									inputPath->SetTargetPhrases(*this, &targetPhrases, ptNode3);
									phraseDictionaryQueue.push_back(inputPath);
								}
							}
					}
				}

				if (nids2.GetSize() < staticData.GetMaxPhraseLength())
					stack.push_back(PairType(nids2,ptNode2));
			}
		}
	}
	delete gap;
}

void PhraseDictionaryMemory::GetTargetPhraseCollectionBatch(Moses::Graph::InputSubgraphList &phraseDictionaryQueue, const InputType& source) const
{
	//TODO
	const MultiDiGraph &m_source = static_cast<const MultiDiGraph&>(source);
	WordsSet nids;
	const PhraseDictionaryNodeMemory* root = &GetRootNode();
	GetTargetPhraseCollectionBatch(nids, root, phraseDictionaryQueue, m_source);
}


TO_STRING_BODY(PhraseDictionaryMemory);

// friend
ostream& operator<<(ostream& out, const PhraseDictionaryMemory& phraseDict)
{
  typedef PhraseDictionaryNodeMemory::TerminalMap TermMap;
  typedef PhraseDictionaryNodeMemory::NonTerminalMap NonTermMap;

  const PhraseDictionaryNodeMemory &coll = phraseDict.m_collection;
  for (NonTermMap::const_iterator p = coll.m_nonTermMap.begin(); p != coll.m_nonTermMap.end(); ++p) {
    const Word &sourceNonTerm = p->first.first;
    out << sourceNonTerm;
  }
  for (TermMap::const_iterator p = coll.m_sourceTermMap.begin(); p != coll.m_sourceTermMap.end(); ++p) {
    const Word &sourceTerm = p->first;
    out << sourceTerm;
  }
  return out;
}

}
