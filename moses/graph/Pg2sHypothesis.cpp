// $Id$
// vim:tabstop=2
/***********************************************************************
Moses - factored phrase-based language decoder
Copyright (C) 2006 University of Edinburgh

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#include "util/check.hh"
#include <iostream>
#include <limits>
#include <vector>
#include <algorithm>

#include "TransOptPg2s.h"
#include "TransOptCollPg2s.h"
#include "moses/Util.h"
//#include "SquareMatrix.h"
//#include "LexicalReordering.h"
#include "moses/StaticData.h"
#include "moses/InputType.h"
#include "Pg2sManager.h"
#include "moses/FF/FFState.h"
#include "Pg2sHypothesis.h"
//#include "Hypothesis.h"

#include "moses/graph/SentenceStatsPg2s.h"

using namespace std;
using namespace Moses;

namespace Moses
{
namespace Graph
{

#ifdef USE_HYPO_POOL
ObjectPool<Pg2sHypothesis> Pg2sHypothesis::s_objectPool("Pg2sHypothesis", 300000);
#endif

Pg2sHypothesis::Pg2sHypothesis(Pg2sManager& manager, InputType const& source, const TransOptPg2s &initialTransOpt)
  : m_prevHypo(NULL)
  , m_sourceCompleted(source.GetSize(), manager.m_source.m_sourceCompleted)
  , m_sourceInput(source)
  , m_currTargetWordsRange(NOT_FOUND, NOT_FOUND)
  , m_wordDeleted(false)
  , m_totalScore(0.0f)
  , m_futureScore(0.0f)
  , m_ffStates(StatefulFeatureFunction::GetStatefulFeatureFunctions().size())
  , m_arcList(NULL)
  , m_transOpt(initialTransOpt)
  , m_manager(manager)
  , m_id(m_manager.GetNextHypoId())
	, m_currDep(-1,-1)
{
  // used for initial seeding of trans process
  // initialize scores
  //_hash_computed = false;
  //s_HypothesesCreated = 1;
  const vector<const StatefulFeatureFunction*>& ffs = StatefulFeatureFunction::GetStatefulFeatureFunctions();
  for (unsigned i = 0; i < ffs.size(); ++i)
    m_ffStates[i] = ffs[i]->EmptyHypothesisState(source);
  m_manager.GetSentenceStats().AddCreated();
}

/***
 * continue prevHypo by appending the phrases in transOpt
 */
Pg2sHypothesis::Pg2sHypothesis(const Pg2sHypothesis &prevHypo, const TransOptPg2s &transOpt)
  : m_prevHypo(&prevHypo)
  , m_sourceCompleted				(prevHypo.m_sourceCompleted )
  , m_sourceInput						(prevHypo.m_sourceInput)
  , m_currSourceWordsRange	(transOpt.GetSourceWordsRange())
  , m_currTargetWordsRange	( prevHypo.m_currTargetWordsRange.GetEndPos() + 1
                              ,prevHypo.m_currTargetWordsRange.GetEndPos() + transOpt.GetTargetPhrase().GetSize())
  , m_wordDeleted(false)
  , m_totalScore(0.0f)
  , m_futureScore(0.0f)
  , m_scoreBreakdown(prevHypo.GetScoreBreakdown())
  , m_ffStates(prevHypo.m_ffStates.size())
  , m_arcList(NULL)
  , m_transOpt(transOpt)
  , m_manager(prevHypo.GetManager())
  , m_id(m_manager.GetNextHypoId())
	, m_currDep(-1,-1)
{
  m_scoreBreakdown.PlusEquals(transOpt.GetScoreBreakdown());

  // assert that we are not extending our hypothesis by retranslating something
  // that this hypothesis has already translated!
  CHECK(!m_sourceCompleted.Overlap(m_currSourceWordsRange.GetSet()));

  //_hash_computed = false;
  m_sourceCompleted.SetValue(m_currSourceWordsRange.GetSet(), true);
  m_wordDeleted = transOpt.IsDeletionOption();
  m_manager.GetSentenceStats().AddCreated();
}

Pg2sHypothesis::~Pg2sHypothesis()
{
  for (unsigned i = 0; i < m_ffStates.size(); ++i)
    delete m_ffStates[i];

  if (m_arcList) {
    ArcList::iterator iter;
    for (iter = m_arcList->begin() ; iter != m_arcList->end() ; ++iter) {
      FREEHYPO(*iter);
    }
    m_arcList->clear();

    delete m_arcList;
    m_arcList = NULL;
  }

}

void Pg2sHypothesis::AddArc(Pg2sHypothesis *loserHypo)
{
  if (!m_arcList) {
    if (loserHypo->m_arcList) { // we don't have an arcList, but loser does
      this->m_arcList = loserHypo->m_arcList;  // take ownership, we'll delete
      loserHypo->m_arcList = 0;                // prevent a double deletion
    } else {
      this->m_arcList = new ArcList();
    }
  } else {
    if (loserHypo->m_arcList) {  // both have an arc list: merge. delete loser
      size_t my_size = m_arcList->size();
      size_t add_size = loserHypo->m_arcList->size();
      this->m_arcList->resize(my_size + add_size, 0);
      std::memcpy(&(*m_arcList)[0] + my_size, &(*loserHypo->m_arcList)[0], add_size * sizeof(Pg2sHypothesis *));
      delete loserHypo->m_arcList;
      loserHypo->m_arcList = 0;
    } else { // loserHypo doesn't have any arcs
      // DO NOTHING
    }
  }
  m_arcList->push_back(loserHypo);
}

/***
 * return the subclass of Pg2sHypothesis most appropriate to the given translation option
 */
Pg2sHypothesis* Pg2sHypothesis::CreateNext(const TransOptPg2s &transOpt, const Phrase* constraint) const
{
  return Create(*this, transOpt, constraint);
}


/***
 * return the subclass of Pg2sHypothesis most appropriate to the given translation option
 */
Pg2sHypothesis* Pg2sHypothesis::Create(const Pg2sHypothesis &prevHypo, const TransOptPg2s &transOpt, const Phrase* constrainingPhrase)
{

  // This method includes code for constraint decoding

  bool createHypothesis = true;

  if (constrainingPhrase != NULL) {

    size_t constraintSize = constrainingPhrase->GetSize();

    size_t start = 1 + prevHypo.GetCurrTargetWordsRange().GetEndPos();

    const Phrase &transOptPhrase = transOpt.GetTargetPhrase();
    size_t transOptSize = transOptPhrase.GetSize();

    size_t endpoint = start + transOptSize - 1;


    if (endpoint < constraintSize) {
      WordsRange range(start, endpoint);
      Phrase relevantConstraint = constrainingPhrase->GetSubString(range);

      if ( ! relevantConstraint.IsCompatible(transOptPhrase) ) {
        createHypothesis = false;

      }
    } else {
      createHypothesis = false;
    }

  }


  if (createHypothesis) {

#ifdef USE_HYPO_POOL
    Pg2sHypothesis *ptr = s_objectPool.getPtr();
    return new(ptr) Pg2sHypothesis(prevHypo, transOpt);
#else
    return new Pg2sHypothesis(prevHypo, transOpt);
#endif

  } else {
    // If the previous hypothesis plus the proposed translation option
    //    fail to match the provided constraint,
    //    return a null hypothesis.
    return NULL;
  }

}


/***
 * return the subclass of Pg2sHypothesis most appropriate to the given target phrase
 */

Pg2sHypothesis* Pg2sHypothesis::Create(Pg2sManager& manager, InputType const& m_source, const TransOptPg2s &initialTransOpt)
{
#ifdef USE_HYPO_POOL
  Pg2sHypothesis *ptr = s_objectPool.getPtr();
  return new(ptr) Pg2sHypothesis(manager, m_source, initialTransOpt);
#else
  return new Pg2sHypothesis(manager, m_source, initialTransOpt);
#endif
}

//Pg2sHypothesis* Pg2sHypothesis::CreateSR(const Pg2sHypothesis &prevHypo, const SRConfig* config)
//{
//#ifdef USE_HYPO_POOL
//    Pg2sHypothesis *ptr = s_objectPool.getPtr();
//    return new(ptr) Pg2sHypothesis(prevHypo, config);
//#else
//    return new Pg2sHypothesis(prevHypo, config);
//#endif
//}


/** check, if two hypothesis can be recombined.
    this is actually a sorting function that allows us to
    keep an ordered list of hypotheses. This makes recombination
    much quicker.
*/
int Pg2sHypothesis::RecombineCompare(const Pg2sHypothesis &compare) const
{
  // -1 = this < compare
  // +1 = this > compare
  // 0	= this ==compare
  int comp = m_sourceCompleted.Compare(compare.m_sourceCompleted);
  if (comp != 0)
    return comp;

  for (unsigned i = 0; i < m_ffStates.size(); ++i) {
    if (m_ffStates[i] == NULL || compare.m_ffStates[i] == NULL) {
      comp = m_ffStates[i] - compare.m_ffStates[i];
    } else {
      comp = m_ffStates[i]->Compare(*compare.m_ffStates[i]);
    }
    if (comp != 0) return comp;
  }

  if (comp != 0) return comp;
  // end

  return 0;
}

void Pg2sHypothesis::EvaluateWith(const StatefulFeatureFunction &sfff,
                              int state_idx)
{
  const StaticData &staticData = StaticData::Instance();
  if (! staticData.IsFeatureFunctionIgnored( sfff )) {
    m_ffStates[state_idx] = sfff.Evaluate(
                              *this,
                              m_prevHypo ? m_prevHypo->m_ffStates[state_idx] : NULL,
                              &m_scoreBreakdown);
  }
}

void Pg2sHypothesis::EvaluateWith(const StatelessFeatureFunction& slff)
{
  const StaticData &staticData = StaticData::Instance();
  if (! staticData.IsFeatureFunctionIgnored( slff )) {
    slff.Evaluate(PhraseBasedFeatureContextPg2s(this), &m_scoreBreakdown);
  }
}

/***
 * calculate the logarithm of our total translation score (sum up components)
 */
void Pg2sHypothesis::Evaluate(const SquareMatrix &futureScore)
{
  clock_t t=0; // used to track time

  // some stateless score producers cache their values in the translation
  // option: add these here
  // language model scores for n-grams completely contained within a target
  // phrase are also included here

  // compute values of stateless feature functions that were not
  // cached in the translation option
  const vector<const StatelessFeatureFunction*>& sfs =
    StatelessFeatureFunction::GetStatelessFeatureFunctions();
  for (unsigned i = 0; i < sfs.size(); ++i) {
    const StatelessFeatureFunction &ff = *sfs[i];
    EvaluateWith(ff);
  }

  const vector<const StatefulFeatureFunction*>& ffs =
    StatefulFeatureFunction::GetStatefulFeatureFunctions();
  for (unsigned i = 0; i < ffs.size(); ++i) {
    const StatefulFeatureFunction &ff = *ffs[i];
    const StaticData &staticData = StaticData::Instance();
    if (! staticData.IsFeatureFunctionIgnored(ff)) {
      m_ffStates[i] = ff.Evaluate(*this,
                                  m_prevHypo ? m_prevHypo->m_ffStates[i] : NULL,
                                  &m_scoreBreakdown);
    }
  }

  IFVERBOSE(2) {
    t = clock();  // track time excluding LM
  }

  // FUTURE COST

  	m_futureScore = futureScore.CalcFutureScore( m_sourceCompleted );

  // TOTAL
  m_totalScore = m_scoreBreakdown.GetWeightedScore() + m_futureScore;

  IFVERBOSE(2) {
    m_manager.GetSentenceStats().AddTimeOtherScore( clock()-t );
  }
}

const Pg2sHypothesis* Pg2sHypothesis::GetPrevHypo()const
{
  return m_prevHypo;
}

/**
 * print hypothesis information for pharaoh-style logging
 */
void Pg2sHypothesis::PrintHypothesis() const
{
  if (!m_prevHypo) {
    TRACE_ERR(endl << "NULL hypo" << endl);
    return;
  }
  TRACE_ERR(endl << "creating hypothesis "<< m_id <<" from "<< m_prevHypo->m_id<<" ( ");
  int end = (int)(m_prevHypo->GetCurrTargetPhrase().GetSize()-1);
  int start = end-1;
  if ( start < 0 ) start = 0;
  if ( m_prevHypo->m_currTargetWordsRange.GetStartPos() == NOT_FOUND ) {
    TRACE_ERR( "<s> ");
  } else {
    TRACE_ERR( "... ");
  }
  if (end>=0) {
    WordsRange range(start, end);
    TRACE_ERR( m_prevHypo->GetCurrTargetPhrase().GetSubString(range) << " ");
  }
  TRACE_ERR( ")"<<endl);
  TRACE_ERR( "\tbase score "<< (m_prevHypo->m_totalScore - m_prevHypo->m_futureScore) <<endl);
  //TRACE_ERR( "\tcovering "<<m_currSourceWordsRange.GetStartPos()<<"-"<<m_currSourceWordsRange.GetEndPos()
  //           <<": " << m_transOpt.GetInputPath().GetPhrase() << endl);

  TRACE_ERR( "\ttranslated as: "<<(Phrase&) GetCurrTargetPhrase()<<endl); // <<" => translation cost "<<m_score[ScoreType::PhraseTrans];

  if (m_wordDeleted) TRACE_ERR( "\tword deleted"<<endl);
  //	TRACE_ERR( "\tdistance: "<<GetCurrSourceWordsRange().CalcDistortion(m_prevHypo->GetCurrSourceWordsRange())); // << " => distortion cost "<<(m_score[ScoreType::Distortion]*weightDistortion)<<endl;
  //	TRACE_ERR( "\tlanguage model cost "); // <<m_score[ScoreType::LanguageModelScore]<<endl;
  //	TRACE_ERR( "\tword penalty "); // <<(m_score[ScoreType::WordPenalty]*weightWordPenalty)<<endl;
  TRACE_ERR( "\tscore "<<m_totalScore - m_futureScore<<" + future cost "<<m_futureScore<<" = "<<m_totalScore<<endl);
  TRACE_ERR(  "\tunweighted feature scores: " << m_scoreBreakdown << endl);
  //PrintLMScores();
}

void Pg2sHypothesis::CleanupArcList()
{
  // point this hypo's main hypo to itself
  SetWinningHypo(this);

  if (!m_arcList) return;

  /* keep only number of arcs we need to create all n-best paths.
   * However, may not be enough if only unique candidates are needed,
   * so we'll keep all of arc list if nedd distinct n-best list
   */
  const StaticData &staticData = StaticData::Instance();
  size_t nBestSize = staticData.GetNBestSize();
  bool distinctNBest = staticData.GetDistinctNBest() || staticData.UseMBR() || staticData.GetOutputSearchGraph() || staticData.GetOutputSearchGraphSLF() || staticData.GetOutputSearchGraphHypergraph() || staticData.UseLatticeMBR() ;

  if (!distinctNBest && m_arcList->size() > nBestSize * 5) {
    // prune arc list only if there too many arcs
    nth_element(m_arcList->begin()
                , m_arcList->begin() + nBestSize - 1
                , m_arcList->end()
                , CompareHypothesisTotalScore());

    // delete bad ones
    ArcList::iterator iter;
    for (iter = m_arcList->begin() + nBestSize ; iter != m_arcList->end() ; ++iter) {
      Pg2sHypothesis *arc = *iter;
      FREEHYPO(arc);
    }
    m_arcList->erase(m_arcList->begin() + nBestSize
                     , m_arcList->end());
  }

  // set all arc's main hypo variable to this hypo
  ArcList::iterator iter = m_arcList->begin();
  for (; iter != m_arcList->end() ; ++iter) {
    Pg2sHypothesis *arc = *iter;
    arc->SetWinningHypo(this);
  }
}

const TargetPhrase &Pg2sHypothesis::GetCurrTargetPhrase() const
{
  return m_transOpt.GetTargetPhrase();
}

void Pg2sHypothesis::GetOutputPhrase(Phrase &out) const
{
  if (m_prevHypo != NULL) {
    m_prevHypo->GetOutputPhrase(out);
  }
  out.Append(GetCurrTargetPhrase());
}

TO_STRING_BODY(Pg2sHypothesis)

// friend
ostream& operator<<(ostream& out, const Pg2sHypothesis& hypo)
{
  hypo.ToStream(out);
  // words bitmap
  out << "[" << hypo.m_sourceCompleted << "] ";

  // scores
  out << " [total=" << hypo.GetTotalScore() << "]";
  out << " " << hypo.GetScoreBreakdown();

  // alignment
  out << " " << hypo.GetCurrTargetPhrase().GetAlignNonTerm();

  /*
  const Pg2sHypothesis *prevHypo = hypo.GetPrevHypo();
  if (prevHypo)
  	out << endl << *prevHypo;
  */

  return out;
}


std::string Pg2sHypothesis::GetSourcePhraseStringRep(const vector<FactorType> factorsToPrint) const
{
  return m_transOpt.GetInputPath().GetPhrase().GetStringRep(factorsToPrint);
}

std::string Pg2sHypothesis::GetTargetPhraseStringRep(const vector<FactorType> factorsToPrint) const
{
  if (!m_prevHypo) {
    return "";
  }
  return GetCurrTargetPhrase().GetStringRep(factorsToPrint);
}

std::string Pg2sHypothesis::GetSourcePhraseStringRep() const
{
  vector<FactorType> allFactors;
  for(size_t i=0; i < MAX_NUM_FACTORS; i++) {
    allFactors.push_back(i);
  }
  return GetSourcePhraseStringRep(allFactors);
}
std::string Pg2sHypothesis::GetTargetPhraseStringRep() const
{
  vector<FactorType> allFactors;
  for(size_t i=0; i < MAX_NUM_FACTORS; i++) {
    allFactors.push_back(i);
  }
  return GetTargetPhraseStringRep(allFactors);
}

}
}

