// $Id$

/***********************************************************************
Moses - factored phrase-based language decoder
Copyright (C) 2006 University of Edinburgh

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#ifndef moses_PartialTranslOptCollPg2s_h
#define moses_PartialTranslOptCollPg2s_h

#include <list>
#include <iostream>
#include "TransOptPg2s.h"
#include "moses/Util.h"
#include "moses/StaticData.h"
#include "moses/FactorTypeSet.h"

namespace Moses
{
namespace Graph
{

/** Contains partial translation options, while these are constructed in the class TransOptPg2s.
 *  The factored translation model allows for multiple translation and
 *  generation steps during a single Hypothesis expansion. For efficiency,
 *  all these expansions are precomputed and stored as TransOptPg2s.
 *  The expansion process itself may be still explode, so efficient handling
 *  of partial translation options during expansion is required.
 *  This class assists in this tasks by implementing pruning.
 *  This implementation is similar to the one in HypothesisStack.
 */
class PartialTranslOptCollPg2s
{
  friend std::ostream& operator<<(std::ostream& out, const PartialTranslOptCollPg2s& possibleTranslation);

protected:
  typedef std::vector<TransOptPg2s*> Coll;
  Coll m_list;
  float m_bestScore; /**< score of the best translation option */
  float m_worstScore; /**< score of the worse translation option */
  size_t m_maxSize; /**< maximum number of translation options allowed */
  size_t m_totalPruned; /**< number of options pruned */

public:
  typedef Coll::iterator iterator;
  typedef Coll::const_iterator const_iterator;

  const_iterator begin() const {
    return m_list.begin();
  }
  const_iterator end() const {
    return m_list.end();
  }

  PartialTranslOptCollPg2s();

  /** destructor, cleans out list */
  ~PartialTranslOptCollPg2s() {
    RemoveAllInColl( m_list );
  }

  void AddNoPrune(TransOptPg2s *partialTranslOpt);
  void Add(TransOptPg2s *partialTranslOpt);
  void Prune();

  /** returns list of translation options */
  const std::vector<TransOptPg2s*>& GetList() const {
    return m_list;
  }

  /** clear out the list */
  void DetachAll() {
    m_list.clear();
  }

  /** return number of pruned partial hypotheses */
  size_t GetPrunedCount() {
    return m_totalPruned;
  }

};

}
}
#endif
