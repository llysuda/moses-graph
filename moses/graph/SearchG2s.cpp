#include "SearchG2s.h"

#include "moses/Timer.h"
#include "G2sManager.h"

using namespace std;

namespace Moses
{
namespace Graph
{

size_t StackNum(size_t sourceSize, int maxGapCount)
{
  if (maxGapCount < 0) {
    return (sourceSize+1)*(sourceSize+2)/2;
  }
  if (maxGapCount == 0) return sourceSize + 1;
  // maxGapCount > 0
  size_t total = 0;
  for(size_t i = 0; i <= sourceSize; i++) {
    size_t count = sourceSize+1-i;
    total += maxGapCount+1 > count ? count : maxGapCount+1;
  }
  return total;
}

size_t StackNum(size_t sourceSize, bool gapBeam)
{
  if (gapBeam) {
    return sourceSize*2;
  }
  return sourceSize+1;
}

vector<size_t> CalcIndex(size_t sourceSize, int maxGapCount)
{
  vector<size_t> ret(sourceSize+1,0);
  size_t total = 0;
  for (size_t i = 0; i <= sourceSize; i++) {
    if (i > 0)
      ret[i] = total;
    if (maxGapCount < 0) {
      total += sourceSize+1-i;
    } else if (maxGapCount == 0) {
      total += 1;
    } else {
      // maxGapCount > 0
      size_t count = sourceSize+1-i;
      total += maxGapCount+1 > count ? count : maxGapCount+1;
    }
  }
  return ret;
}

/**
 * Organizing main function
 *
 * /param source input sentence
 * /param transOptColl collection of translation options to be used for this sentence
 */
SearchG2s::SearchG2s(G2sManager& manager, const InputType &source, const G2sTranslationOptionColl &transOptColl)
  : m_source(source)
	,m_manager(manager)
  ,m_hypoStackColl(source.GetSize()+1)
  ,m_start(clock())
  ,interrupted_flag(0)
  ,m_transOptColl(transOptColl)
{
  VERBOSE(1, "Translating: " << m_source << endl);
  const StaticData &staticData = StaticData::Instance();

  // only if constraint decoding (having to match a specified output)
  long sentenceID = source.GetTranslationId();
  m_constraint = staticData.GetConstrainingPhrase(sentenceID);
  if (m_constraint) {
    VERBOSE(1, "Search constraint to output: " << *m_constraint<<endl);
  }

  // initialize the stacks: create data structure and set limits
  std::vector < G2sHypothesisStackNormal >::iterator iterStack;
  for (size_t ind = 0 ; ind < m_hypoStackColl.size() ; ++ind) {
    G2sHypothesisStackNormal *sourceHypoColl = new G2sHypothesisStackNormal(m_manager);
    sourceHypoColl->SetMaxHypoStackSize(staticData.GetMaxHypoStackSize(),staticData.GetMinHypoStackDiversity());
    sourceHypoColl->SetBeamWidth(staticData.GetBeamWidth());

    m_hypoStackColl[ind] = sourceHypoColl;
  }
}

SearchG2s::~SearchG2s()
{
  RemoveAllInColl(m_hypoStackColl);
}

/**
 * Main decoder loop that translates a sentence by expanding
 * hypotheses stack by stack, until the end of the sentence.
 */
void SearchG2s::ProcessSentence()
{
  const StaticData &staticData = StaticData::Instance();
  SentenceStatsG2s &stats = m_manager.GetSentenceStats();
  clock_t t=0; // used to track time for steps

  // initial seed hypothesis: nothing translated, no words produced
  G2sHypothesis *hypo = G2sHypothesis::Create(m_manager,m_source, m_initialTransOpt);
  m_hypoStackColl[0]->AddPrune(hypo);

  // go through each stack
  std::vector < G2sHypothesisStack* >::iterator iterStack;
  for (iterStack = m_hypoStackColl.begin() ; iterStack != m_hypoStackColl.end() ; ++iterStack) {
    // check if decoding ran out of time
    double _elapsed_time = GetUserTime();
    if (_elapsed_time > staticData.GetTimeoutThreshold()) {
      VERBOSE(1,"Decoding is out of time (" << _elapsed_time << "," << staticData.GetTimeoutThreshold() << ")" << std::endl);
      interrupted_flag = 1;
      return;
    }
    G2sHypothesisStackNormal &sourceHypoColl = *static_cast<G2sHypothesisStackNormal*>(*iterStack);

    // the stack is pruned before processing (lazy pruning):
    VERBOSE(3,"processing hypothesis from next stack");
    IFVERBOSE(2) {
      t = clock();
    }
    sourceHypoColl.PruneToSize(staticData.GetMaxHypoStackSize());
    VERBOSE(3,std::endl);
    sourceHypoColl.CleanupArcList();
    IFVERBOSE(2) {
      stats.AddTimeStack( clock()-t );
    }

    // go through each hypothesis on the stack and try to expand it
    G2sHypothesisStackNormal::const_iterator iterHypo;
    for (iterHypo = sourceHypoColl.begin() ; iterHypo != sourceHypoColl.end() ; ++iterHypo) {
      G2sHypothesis &hypothesis = **iterHypo;
      ProcessOneHypothesis(hypothesis); // expand the hypothesis
    }
    // some logging
    IFVERBOSE(2) {
      OutputHypoStackSize();
    }

    // this stack is fully expanded;
    actual_hypoStack = &sourceHypoColl;
  }

  // some more logging
  IFVERBOSE(2) {
    m_manager.GetSentenceStats().SetTimeTotal( clock()-m_start );
  }
  VERBOSE(2, m_manager.GetSentenceStats());
}


/** Find all translation options to expand one hypothesis, trigger expansion
 * this is mostly a check for overlap with already covered words, and for
 * violation of reordering limits.
 * \param hypothesis hypothesis to be expanded upon
 */
void SearchG2s::ProcessOneHypothesis(const G2sHypothesis &hypothesis)
{
	const MultiDiGraph& graph = static_cast<const MultiDiGraph&>(m_source);
	int maxDistortion = StaticData::Instance().GetMaxDistortion();
	const WordsBitmap& hypoBitmap	= hypothesis.GetWordsBitmap();
	const size_t hypoFirstGapPos    = hypoBitmap.GetFirstGapPos();

	G2sTranslationOptionColl::const_iterator iter;
	for(iter = m_transOptColl.begin(); iter != m_transOptColl.end(); iter++) {
		const WordsSet& range = iter->first;
		if (iter->second->GetSize() == 0 || hypoBitmap.Overlap(range.GetSet()))
          continue;
        if (maxDistortion >= 0 && !m_source.within_distance(range.GetSet(), hypoFirstGapPos, maxDistortion))
          continue;

		ExpandAllHypotheses(hypothesis, range);
	}

	return;
}


/**
 * Expand a hypothesis given a list of translation options
 * \param hypothesis hypothesis to be expanded upon
 * \param startPos first word position of span covered
 * \param endPos last word position of span covered
 */

void SearchG2s::ExpandAllHypotheses(const G2sHypothesis &hypothesis, const WordsSet& nids)
{
  // early discarding: check if hypothesis is too bad to build
  // this idea is explained in (Moore&Quirk, MT Summit 2007)
  float expectedScore = 0.0f;

  if (StaticData::Instance().UseEarlyDiscarding()) {
      // expected score is based on score of current hypothesis
      expectedScore = hypothesis.GetScore();

      // add new future score estimate
      expectedScore += m_transOptColl.GetFutureScore().CalcFutureScore( hypothesis.GetWordsBitmap(), nids.GetSet());
    }

  size_t inputSize = m_source.GetSize();
  const WordsBitmap& hypoBitmap = hypothesis.GetWordsBitmap();

//
//  // loop through all translation options
  const G2sTranslationOptionList &transOptList = *(m_transOptColl.GetTranslationOptionList(nids));
  size_t size = transOptList.GetSize();
  size_t valid_count = 0;
//
  const set<int>& currRange = nids.GetSet();
  set<int> nextRange, prevRange;

  int s = (int)inputSize;

  for(int i = 0; i < s; i++) {
    if (hypoBitmap.GetValue(i))
      prevRange.insert(i);
    else if (currRange.find(i) == currRange.end())
      nextRange.insert(i);
  }

  const MultiDiGraph& graph = static_cast<const MultiDiGraph&>(m_source);
  string gapLHS = "X#";

  gapLHS += graph.GetBetweenLink(currRange, nextRange);

    for (size_t i = 0; i < size; i++) {
      const G2sTranslationOptions& transOpts = transOptList.Get(i);

      string lhs = transOpts.GetLHS();
	  if (lhs != "X#X" && lhs != gapLHS) continue;
	  
      valid_count++;
      G2sTranslationOptions::const_iterator iter = transOpts.begin();
      for (; iter != transOpts.end() ; ++iter) {
        const G2sTranslationOption &transOpt = **iter;
        ExpandHypothesis(hypothesis, transOpt, expectedScore);
      }
    }

}

/**
 * Expand one hypothesis with a translation option.
 * this involves initial creation, scoring and adding it to the proper stack
 * \param hypothesis hypothesis to be expanded upon
 * \param transOpt translation option (phrase translation)
 *        that is applied to create the new hypothesis
 * \param expectedScore base score for early discarding
 *        (base hypothesis score plus future score estimation)
 */
void SearchG2s::ExpandHypothesis(const G2sHypothesis &hypothesis, const G2sTranslationOption &transOpt, float expectedScore)
{
  const StaticData &staticData = StaticData::Instance();
  SentenceStatsG2s &stats = m_manager.GetSentenceStats();
  clock_t t=0; // used to track time for steps

  G2sHypothesis *newHypo;

  if (! staticData.UseEarlyDiscarding()) {
    // simple build, no questions asked
    IFVERBOSE(2) {
      t = clock();
    }

    newHypo = hypothesis.CreateNext(transOpt, m_constraint);

    IFVERBOSE(2) {
      stats.AddTimeBuildHyp( clock()-t );
    }

    if (newHypo==NULL) return;
    newHypo->Evaluate(m_transOptColl.GetFutureScore()); // future score
  } else
    // early discarding: check if hypothesis is too bad to build
  {
    // worst possible score may have changed -> recompute
    size_t wordsTranslated = hypothesis.GetWordsBitmap().GetNumWordsCovered() + transOpt.GetSize();
    float allowedScore = m_hypoStackColl[wordsTranslated]->GetWorstScore();
//    if (staticData.GetMinHypoStackDiversity()) {
//      WordsBitmapID id = hypothesis.GetWordsBitmap().GetIDPlus(transOpt.GetStartPos(), transOpt.GetEndPos());
//      float allowedScoreForBitmap = m_hypoStackColl[wordsTranslated]->GetWorstScoreForBitmap( id );
//      allowedScore = std::min( allowedScore, allowedScoreForBitmap );
//    }
    allowedScore += staticData.GetEarlyDiscardingThreshold();

    // add expected score of translation option
    expectedScore += transOpt.GetFutureScore();
    // TRACE_ERR("EXPECTED diff: " << (newHypo->GetTotalScore()-expectedScore) << " (pre " << (newHypo->GetTotalScore()-expectedScorePre) << ") " << hypothesis.GetTargetPhrase() << " ... " << transOpt.GetTargetPhrase() << " [" << expectedScorePre << "," << expectedScore << "," << newHypo->GetTotalScore() << "]" << endl);

    // check if transOpt score push it already below limit
    if (expectedScore < allowedScore) {
      IFVERBOSE(2) {
        stats.AddNotBuilt();
      }
      return;
    }

    // build the hypothesis without scoring
    IFVERBOSE(2) {
      t = clock();
    }

		newHypo = hypothesis.CreateNext(transOpt, m_constraint);
//		newHypo->Evaluate(transOpt.GetFutureScore());
    IFVERBOSE(2) {
      stats.AddTimeBuildHyp( clock()-t );
    }

    // ... and check if that is below the limit
    if (expectedScore < allowedScore) {
      IFVERBOSE(2) {
        stats.AddEarlyDiscarded();
      }
      FREEHYPO( newHypo );
      return;
    }

  }

  // logging for the curious
  IFVERBOSE(3) {
  	newHypo->PrintHypothesis();
  }

  // add to hypothesis stack
	size_t wordsTranslated = newHypo->GetWordsBitmap().GetNumWordsCovered();
	IFVERBOSE(2) {
		t = clock();
	}
	m_hypoStackColl[wordsTranslated]->AddPrune(newHypo);

	IFVERBOSE(2) {
		stats.AddTimeStack( clock()-t );
	}
}

const std::vector < G2sHypothesisStack* >& SearchG2s::GetHypothesisStacks() const
{
  return m_hypoStackColl;
}

/**
 * Find best hypothesis on the last stack.
 * This is the end point of the best translation, which can be traced back from here
 */
const G2sHypothesis *SearchG2s::GetBestHypothesis() const
{
  if (interrupted_flag == 0) {
    const G2sHypothesisStackNormal &hypoColl = *static_cast<G2sHypothesisStackNormal*>(m_hypoStackColl.back());
    return hypoColl.GetBestHypothesis();
  } else {
    const G2sHypothesisStackNormal &hypoColl = *actual_hypoStack;
    return hypoColl.GetBestHypothesis();
  }
}

/**
 * Logging of hypothesis stack sizes
 */
void SearchG2s::OutputHypoStackSize()
{
  std::vector < G2sHypothesisStack* >::const_iterator iterStack = m_hypoStackColl.begin();
  TRACE_ERR( "Stack sizes: " << (int)(*iterStack)->size());
  for (++iterStack; iterStack != m_hypoStackColl.end() ; ++iterStack) {
    TRACE_ERR( ", " << (int)(*iterStack)->size());
  }
  TRACE_ERR( endl);
}

}
}
