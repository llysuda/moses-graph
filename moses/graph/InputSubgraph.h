#pragma once

#include <map>
#include <iostream>
#include <list>
#include <set>
#include "moses/Phrase.h"
#include "moses/WordsBitmap.h"
#include "moses/NonTerminal.h"

#include "moses/graph/WordsSet.h"

namespace Moses
{

class PhraseDictionary;
class TargetPhraseCollection;
class ScoreComponentCollection;
class TargetPhrase;

namespace Graph
{

class InputSubgraph;

typedef std::list<InputSubgraph*> InputSubgraphList;

/** Each node contains
1. substring used to searching the phrase table
2. the source range it covers
3. a list of InputSubgraph that it is a prefix of
This is for both sentence input, and confusion network/lattices
*/
class InputSubgraph
{
  friend std::ostream& operator<<(std::ostream& out, const InputSubgraph &obj);
typedef std::vector< std::set<size_t> > StructType;
protected:
  const InputSubgraph *m_prevNode;
  Phrase m_phrase;
  WordsSet m_range;
  const ScoreComponentCollection *m_inputScore;
  std::map<const PhraseDictionary*, std::pair<const TargetPhraseCollection*, const void*> > m_targetPhrases;
  const NonTerminalSet m_sourceNonTerms;

  std::vector<size_t> m_placeholders;

  StructType ruleSourceNT;
  StructType inputSourceNT;

  bool SetPlaceholders(TargetPhrase *targetPhrase) const;
public:

//  size_t index;
  explicit InputSubgraph()
    : m_prevNode(NULL)
    , m_inputScore(NULL)
  {
  }

  InputSubgraph(const Phrase &phrase, const NonTerminalSet &sourceNonTerms, const WordsSet &range, const InputSubgraph *prevNode
            ,const ScoreComponentCollection *inputScore);
  InputSubgraph(const Phrase &phrase, const NonTerminalSet &sourceNonTerms, const WordsSet &range);

  ~InputSubgraph();

  const Phrase &GetPhrase() const {
    return m_phrase;
  }
  const NonTerminalSet &GetNonTerminalSet() const {
    return m_sourceNonTerms;
  }
  const WordsSet &GetWordsRange() const {
    return m_range;
  }
  const Word &GetLastWord() const;

  const InputSubgraph *GetPrevNode() const {
    return m_prevNode;
  }

  void SetTargetPhrases(const PhraseDictionary &phraseDictionary
                        , const TargetPhraseCollection *targetPhrases
                        , const void *ptNode);
  const TargetPhraseCollection *GetTargetPhrases(const PhraseDictionary &phraseDictionary) const;

  // pointer to internal node in phrase-table. Since this is implementation dependent, this is a void*
  const void *GetPtNode(const PhraseDictionary &phraseDictionary) const;
  const ScoreComponentCollection *GetInputScore() const {
    return m_inputScore;
  }

  void ParseStructString(const std::string& str, StructType& ret);

  void SetSourceStruct(const std::string& rsnt, const std::string& isnt) {
    ParseStructString(rsnt, ruleSourceNT);
    ParseStructString(isnt, inputSourceNT);
  }

  const StructType& GetRuleSourceNT() const {
    return ruleSourceNT;
  }

  const StructType& GetInputSourceNT() const {
    return inputSourceNT;
  }

};

}
}

