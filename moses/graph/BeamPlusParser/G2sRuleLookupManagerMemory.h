/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2011 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once
#ifndef moses_G2sRuleLookupManagerMemory_h
#define moses_G2sRuleLookupManagerMemory_h

#include <vector>
#include <string>
#include <vector>

#ifdef USE_BOOST_POOL
#include <boost/pool/object_pool.hpp>
#endif

#include "G2sRuleLookupManagerCYKPlus.h"
#include "moses/NonTerminal.h"
#include "moses/TranslationModel/PhraseDictionaryMemory.h"
#include "moses/TranslationModel/PhraseDictionaryNodeMemory.h"

namespace Moses
{
namespace Graph
{

class G2sTranslationOptionColl;
class WordsSet;

//! Implementation of G2sRuleLookupManager for in-memory rule tables.
class G2sRuleLookupManagerMemory : public G2sRuleLookupManagerCYKPlus
{
public:
  G2sRuleLookupManagerMemory(const G2sParser &parser,
                               const PhraseDictionaryMemory &ruleTable);

  ~G2sRuleLookupManagerMemory();

  virtual void GetG2sRuleCollection(
  		G2sTranslationOptionColl &outColl);

  void GetG2sRuleCollection(
  		WordsSet& nids, int count,
  		const PhraseDictionaryNodeMemory* currNode,
			G2sTranslationOptionColl &outColl,
  		const MultiDiGraph& source) const;

private:
  const PhraseDictionaryMemory &m_ruleTable;
};

}  // namespace Moses
}

#endif
