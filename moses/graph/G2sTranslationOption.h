#pragma once

#include "moses/ScoreComponentCollection.h"
#include <vector>
#include <string>
#include <map>
#include "moses/TargetPhrase.h"

#include "WordsSet.h"

namespace Moses
{
class TargetPhrase;
class InputType;
class PhraseDictionary;
//class Moses::GenerationDictionary;
class FeatureFunction;
class LexicalReordering;

namespace Graph
{
class InputSubgraph;

class G2sTranslationOption
{
protected:
  const Phrase* m_sourcePhrase;
  TargetPhrase m_targetPhrase;
  ScoreComponentCollection m_scoreBreakdown;
  WordsSet m_wordsRange;
  float m_futureScore;
  typedef std::map<const LexicalReordering*, Scores> _ScoreCacheMap;
    _ScoreCacheMap m_lexReorderingScores;

   std::string m_lhs;

public:
  explicit G2sTranslationOption();

  G2sTranslationOption(const TargetPhrase &targetPhrase, const WordsSet& range, const Phrase* sourcePhrase);

  void SetMembers(const std::string& lhs) {
    m_lhs = lhs;
  }


  const std::string& GetSourceLHS() const {
    return m_lhs;
  }

  const TargetPhrase &GetTargetPhrase() const {
    return m_targetPhrase;
  }

  const Phrase& GetSourcePhrase() const {
    return *m_sourcePhrase;
  }


  size_t GetSize() const {
  	return m_wordsRange.GetSize();
  }

  float GetFutureScore() const {
  	return m_futureScore;
  }

  const ScoreComponentCollection &GetScores() const {
    return m_scoreBreakdown;
  }

  const WordsSet& GetSourceWordsRange() const {
  	return m_wordsRange;
  }

  bool IsDeletionOption() const {
  	return m_targetPhrase.GetSize() == 0;
  }

  std::string GetWordString(size_t pos) const {
    return m_targetPhrase.GetWord(pos).GetString(0).as_string();
  }

  bool IsUNK() const {
    return m_targetPhrase.GetWord(0).IsOOV();
  }

  bool TargetLabelMatch(const std::string& label) const {
    return m_targetPhrase.GetTargetLHS().GetString(0).as_string() == label;
  }

  void Evaluate(const InputType &input);


  /** returns cached scores */
    inline const Scores *GetLexReorderingScores(const LexicalReordering *scoreProducer) const {
      _ScoreCacheMap::const_iterator it = m_lexReorderingScores.find(scoreProducer);
      if(it == m_lexReorderingScores.end())
        return NULL;
      else
        return &(it->second);
    }

    void CacheLexReorderingScores(const LexicalReordering &scoreProducer, const Scores &score);

    /** returns cached scores */

    inline size_t GetStartPos() const {
      return m_wordsRange.GetStartPos();
    }

    /** return end index of source phrase */
    inline size_t GetEndPos() const {
      return m_wordsRange.GetEndPos();
    }

};


//XXX: This doesn't look at the alignment. Is this correct?
inline size_t hash_value(const G2sTranslationOption& translationOption)
{
  size_t  seed = 0;
  boost::hash_combine(seed, translationOption.GetTargetPhrase());
  boost::hash_combine(seed, translationOption.GetStartPos());
  boost::hash_combine(seed, translationOption.GetEndPos());
  return seed;
}

}
}

