// $Id$
// vim:tabstop=2

/***********************************************************************
Moses - factored phrase-based language decoder
Copyright (C) 2006 University of Edinburgh

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#ifndef moses_graph_G2sHypothesis_h
#define moses_graph_G2sHypothesis_h

#include <vector>
#include <map>
#include <iostream>
#include <memory>
#include <vector>
#include "moses/Phrase.h"
#include "moses/TypeDef.h"
#include "moses/WordsBitmap.h"
#include "moses/Sentence.h"
#include "moses/GenerationDictionary.h"
#include "moses/ScoreComponentCollection.h"
#include "moses/InputType.h"
#include "moses/ObjectPool.h"

//#include "moses/Hypothesis.h"
#include "moses/graph/WordsSet.h"
#include "G2sTranslationOption.h"

namespace Moses
{

class FFState;
class StatelessFeatureFunction;
class StatefulFeatureFunction;
class WordsRange;
class SquareMatrix;
class StaticData;
//class Hypothesis;

namespace Graph
{

class G2sTranslationOption;
class G2sHypothesis;
class G2sManager;

typedef std::vector<G2sHypothesis*> ArcListG2s;


class G2sGap
{
public:
  const G2sHypothesis* m_hypo;
  const size_t m_position;
  std::vector<const FFState*> m_state;
};

/** Used to store a state in the beam search
    for the best translation. With its link back to the previous hypothesis
    m_prevHypo, we can trace back to the sentence start to read of the
    (partial) translation to this point.

		The expansion of hypotheses is handled in the class Manager, which
    stores active hypothesis in the search in hypothesis stacks.
***/
class G2sHypothesis
{
  friend std::ostream& operator<<(std::ostream&, const G2sHypothesis&);
public:
  typedef std::pair<const G2sHypothesis*, size_t> PointsType;
  typedef std::vector< PointsType > PointsCollType;

protected:
  static ObjectPool<G2sHypothesis> s_objectPool;

  const G2sHypothesis* m_prevHypo; /*! backpointer to previous hypothesis (from which this one was created) */
//	const Phrase			&m_targetPhrase; /*! target phrase being created at the current decoding step */
  WordsBitmap				m_sourceCompleted; /*! keeps track of which words have been translated so far */
  //TODO: how to integrate this into confusion network framework; what if
  //it's a confusion network in the end???
  InputType const&  m_sourceInput;
  WordsSet				m_currSourceWordsRange; /*! source word positions of the last phrase that was used to create this hypothesis */
  WordsRange        m_currTargetWordsRange; /*! target word positions of the last phrase that was used to create this hypothesis */
  bool							m_wordDeleted;
  float							m_totalScore;  /*! score so far */
  float							m_futureScore; /*! estimated future cost to translate rest of sentence */
  ScoreComponentCollection m_scoreBreakdown; /*! scores for this hypothesis */
  std::vector<const FFState*> m_ffStates;
  const G2sHypothesis 	*m_winningHypo;
  ArcListG2s 					*m_arcList; /*! all arcs that end at the same trellis point as this hypothesis */
  const G2sTranslationOption &m_transOpt;
  G2sManager& m_manager;


  std::set<std::string> m_nextLabels, m_fatherLabels;

  int m_id; /*! numeric ID of this hypothesis, used for logging */

  /*! used by initial seeding of the translation process */
  G2sHypothesis(G2sManager& manager, InputType const& source, const G2sTranslationOption &initialTransOpt);
  /*! used when creating a new hypothesis using a translation option (phrase translation) */
  G2sHypothesis(const G2sHypothesis &prevHypo, const G2sTranslationOption &transOpt);

public:

  const std::set<std::string>& GetNextLabels() const {
    return m_nextLabels;
  }

  const std::set<std::string>& GetFatherLabels() const {
    return m_fatherLabels;
  }

  void InsertNextLabels(const std::set<std::string>& labels) {
    m_nextLabels.insert(labels.begin(), labels.end());
  }

  void InsertFatherLabels(const std::set<std::string>& labels) {
    m_fatherLabels.insert(labels.begin(), labels.end());
  }


  static ObjectPool<G2sHypothesis> &GetObjectPool() {
    return s_objectPool;
  }

  ~G2sHypothesis();

  /** return the subclass of Hypothesis most appropriate to the given translation option */
  static G2sHypothesis* Create(const G2sHypothesis &prevHypo, const G2sTranslationOption &transOpt, const Phrase* constraint);

  static G2sHypothesis* Create(G2sManager& manager, const WordsBitmap &initialCoverage);

  /** return the subclass of Hypothesis most appropriate to the given target phrase */
  static G2sHypothesis* Create(G2sManager& manager, InputType const& source, const G2sTranslationOption &initialTransOpt);

  /** return the subclass of Hypothesis most appropriate to the given translation option */
  G2sHypothesis* CreateNext(const G2sTranslationOption &transOpt, const Phrase* constraint) const;

//  G2sHypothesis* CreateNext() const;
//  static G2sHypothesis* Create(const G2sHypothesis &prevHypo);

  void PrintHypothesis() const;

  const InputType& GetInput() const {
    return m_sourceInput;
  }

  /** return target phrase used to create this hypothesis */
//	const Phrase &GetCurrTargetPhrase() const
  const TargetPhrase &GetCurrTargetPhrase() const;

//  const Phrase& GetCurrPhrase() const;

  /** return input positions covered by the translation option (phrasal translation) used to create this hypothesis */
  inline const WordsSet& GetCurrSourceWordsRange() const {
    return m_currSourceWordsRange;
  }

  inline const WordsRange &GetCurrTargetWordsRange() const {
    return m_currTargetWordsRange;
  }

  G2sManager& GetManager() const {
    return m_manager;
  }

  /** output length of the translation option used to create this hypothesis */
  inline size_t GetCurrTargetLength() const {
    return m_currTargetWordsRange.GetNumWordsCovered();
  }

  void Evaluate(const Moses::SquareMatrix &futureScore);

  int GetId()const {
    return m_id;
  }

  const G2sHypothesis* GetPrevHypo() const;

  /** length of the partial translation (from the start of the sentence) */
  inline size_t GetSize() const {
    return m_currTargetWordsRange.GetEndPos() + 1;
  }

//  size_t GetTrueTargetStartPos() const {
//  	return m_targetStartPos;
//  }

  std::string GetSourcePhraseStringRep(const std::vector<FactorType> factorsToPrint) const;
  std::string GetTargetPhraseStringRep(const std::vector<FactorType> factorsToPrint) const;
  std::string GetSourcePhraseStringRep() const;
  std::string GetTargetPhraseStringRep() const;

  /** curr - pos is relative from CURRENT hypothesis's starting index
   * (ie, start of sentence would be some negative number, which is
   * not allowed- USE WITH CAUTION) */
  inline const Word &GetCurrWord(size_t pos) const {
    return GetCurrTargetPhrase().GetWord(pos);
  }
  inline const Factor *GetCurrFactor(size_t pos, FactorType factorType) const {
    return GetCurrTargetPhrase().GetFactor(pos, factorType);
  }
  /** recursive - pos is relative from start of sentence */
  inline const Word &GetWord(size_t pos) const {
    const G2sHypothesis *hypo = this;
    while (pos < hypo->GetCurrTargetWordsRange().GetStartPos()) {
      hypo = hypo->GetPrevHypo();
      CHECK(hypo != NULL);
    }
    return hypo->GetCurrWord(pos - hypo->GetCurrTargetWordsRange().GetStartPos());
  }
  inline const Factor* GetFactor(size_t pos, FactorType factorType) const {
    return GetWord(pos)[factorType];
  }

  /***
   * \return The bitmap of source words we cover
   */
  inline const WordsBitmap &GetWordsBitmap() const {
    return m_sourceCompleted;
  }

  inline bool IsSourceCompleted() const {
    return m_sourceCompleted.IsComplete();
  }

  int RecombineCompare(const G2sHypothesis &compare) const;

  void GetOutputPhrase(Phrase &out) const;

  void GetPrefixPhrase(Phrase &out) const;
  void GetSuffixPhrase(Phrase &out) const;

  void ToStream(std::ostream& out) const {
    Phrase ret;
    GetOutputPhrase(ret);
    out << ret;
  }

  void ToStringStream(std::stringstream& out) const {
  	ToStream(out);
//    if (m_prevHypo != NULL) {
//      m_prevHypo->ToStream(out);
//    }
//    out << (Phrase) GetCurrTargetPhrase();
  }

  std::string GetOutputString() const {
    std::stringstream out;
    ToStringStream(out);
    return out.str();
  }

  TO_STRING();

  inline void SetWinningHypo(const G2sHypothesis *hypo) {
    m_winningHypo = hypo;
  }
  inline const G2sHypothesis *GetWinningHypo() const {
    return m_winningHypo;
  }

  void AddArc(G2sHypothesis *loserHypo);
  void CleanupArcList();

  //! returns a list alternative previous hypotheses (or NULL if n-best support is disabled)
  inline const ArcListG2s* GetArcList() const {
    return m_arcList;
  }
  const ScoreComponentCollection& GetScoreBreakdown() const {
    return m_scoreBreakdown;
  }
  float GetTotalScore() const {
    return m_totalScore;
  }
  float GetScore() const {
    return m_totalScore-m_futureScore;
  }
  const FFState* GetFFState(int idx) const {
    return m_ffStates[idx];
  }
  void SetFFState(int idx, FFState* state) {
    m_ffStates[idx] = state;
  }

  // Added by oliver.wilson@ed.ac.uk for async lm stuff.
  void EvaluateWith(const StatefulFeatureFunction &sfff, int state_idx);
  void EvaluateWith(const StatelessFeatureFunction &slff);

  //! target span that trans opt would populate if applied to this hypo. Used for alignment check
  size_t GetNextStartPos(const G2sTranslationOption &transOpt) const;

  std::vector<std::vector<unsigned int> > *GetLMStats() const {
    return NULL;
  }

  const G2sTranslationOption &GetTranslationOption() const {
    return m_transOpt;
  }
};

std::ostream& operator<<(std::ostream& out, const G2sHypothesis& hypothesis);

// sorting helper
struct CompareG2sHypothesisTotalScore {
  bool operator()(const G2sHypothesis* hypo1, const G2sHypothesis* hypo2) const {
    return hypo1->GetTotalScore() > hypo2->GetTotalScore();
  }
};

#ifdef USE_HYPO_POOL

#define FREEHYPO(hypo) \
{ \
	ObjectPool<Hypothesis> &pool = Hypothesis::GetObjectPool(); \
	pool.freeObject(hypo); \
} \
 
#else
#define FREEHYPO(hypo) delete hypo
#endif

/** defines less-than relation on hypotheses.
* The particular order is not important for us, we need just to figure out
* which hypothesis are equal based on:
*   the last n-1 target words are the same
*   and the covers (source words translated) are the same
* Directly using RecombineCompare is unreliable because the Compare methods
* of some states are based on archictecture-dependent pointer comparisons.
* That's why we use the hypothesis IDs instead.
*/
class G2sHypothesisRecombinationOrderer
{
public:
  bool operator()(const G2sHypothesis* hypoA, const G2sHypothesis* hypoB) const {
    return (hypoA->RecombineCompare(*hypoB) < 0);
  }
};

}
}
#endif
