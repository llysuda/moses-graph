// $Id$

/***********************************************************************
Moses - factored phrase-based language decoder
Copyright (C) 2006 University of Edinburgh

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#ifndef moses_graph_TransOptCollPg2s_h
#define moses_graph_TransOptCollPg2s_h

#include <list>
#include <boost/unordered_map.hpp>
#include "moses/TypeDef.h"
#include "moses/SquareMatrix.h"
#include "moses/WordsBitmap.h"
#include "PartialTranslOptCollPg2s.h"
#include "moses/DecodeStep.h"
#include "TransOptListPg2s.h"
#include "TransOptPg2s.h"
#include "moses/Phrase.h"
#include "moses/graph/WordsSet.h"

namespace Moses
{

class LanguageModel;
class FactorCollection;
//class GenerationDictionary;
class InputType;
class FactorMask;
class Word;
class DecodeGraph;
class PhraseDictionary;
class Phrase;

namespace Graph
{
//class InputPath;
class InputSubgraph;

/** Contains all phrase translations applicable to current input type (a sentence or confusion network).
 * A key insight into efficient decoding is that various input
 * conditions (trelliss, factored input, normal text, xml markup)
 * all lead to the same decoding algorithm: hypotheses are expanded
 * by applying phrase translations, which can be precomputed.
 *
 * The precomputation of a collection of instances of such TranslationOption
 * depends on the input condition, but they all are presented to
 * decoding algorithm in the same form, using this class.
 *
 * This is a abstract class, and cannot be instantiated directly. Instantiate 1 of the inherited
 * classes instead, for a particular input type
 **/
class TransOptCollPg2s
{
	friend std::ostream& operator<<(std::ostream& out, const TransOptCollPg2s& coll);
	TransOptCollPg2s(const TransOptCollPg2s&); /*< no copy constructor */

protected:
  //std::vector< std::vector< TransOptListPg2s > >	m_collection; /*< contains translation options */
  std::map<WordsSet, TransOptListPg2s> m_collection;
  InputType const			&m_source; /*< reference to the input */
  SquareMatrix				m_futureScore; /*< matrix of future costs for contiguous parts (span) of the input */
  const size_t				m_maxNoTransOptPerCoverage; /*< maximum number of translation options per input span */
  const float				m_translationOptionThreshold; /*< threshold for translation options with regard to best option for input span */
  std::vector<const Phrase*> m_unksrcs;
  InputSubgraphList m_phraseDictionaryQueue;

  void CalcFutureScore();
  void CalcFutureScoreFancy();

  //! Force a creation of a translation option where there are none for a particular source position.
  void ProcessUnknownWord();
  //! special handling of ONE unknown words.
  //virtual void ProcessOneUnknownWord(const InputPath &inputPath, size_t sourcePos, size_t length = 1, const Scores *inputScores = NULL);

  //! pruning: only keep the top n (m_maxNoTransOptPerCoverage) elements */
  void Prune();

  //! sort all trans opt in each list for cube pruning */
  void Sort();

  //! list of trans opt for a particular span
 // TransOptListPg2s &GetTranslationOptionList(const std::set<int>& range);
  //const TransOptListPg2s &GetTranslationOptionList(const std::set<int>& range) const;
  //void Add(TransOptPg2s *translationOption);

  //! implemented by inherited class, called by this class
  void ProcessUnknownWord(size_t sourcePos);

  void EvaluateWithSource();

  void CacheLexReordering();

  void GetTargetPhraseCollectionBatch();

  void CreateTranslationOptionsForRange(
    const DecodeGraph &decodeGraph
    , size_t startPos
    , size_t endPos
    , bool adhereTableLimit
    , size_t graphInd
    , InputSubgraph &inputPath);

  void SetInputScore(const InputSubgraph &inputPath, PartialTranslOptCollPg2s &oldPtoc);

public:
  TransOptCollPg2s(const InputType& src, size_t maxNoTransOptPerCoverage,
                                float translationOptionThreshold);
  ~TransOptCollPg2s();

	typedef std::map<WordsSet, TransOptListPg2s>::const_iterator const_iterator;
	typedef std::map<WordsSet, TransOptListPg2s>::iterator iterator;
	//! iterators
	const_iterator begin() const {
		return m_collection.begin();
	}
	const_iterator end() const {
		return m_collection.end();
	}

  //! input sentence/confusion network
  const InputType& GetSource() const {
    return m_source;
  }

  //!List of unknowns (OOVs)
  const std::vector<const Phrase*>& GetUnknownSources() const {
    return m_unksrcs;
  }

  //! get length/size of source input
  size_t GetSize() const {
    return m_source.GetSize();
  };

  //! Create all possible translations from the phrase tables
  void CreateTranslationOptions();
//  //! Create translation options that exactly cover a specific input span.
  void CreateTranslationOptionsForRange(const DecodeGraph &decodeStepList
      , const Moses::Graph::InputSubgraph& inputPath
      , bool adhereTableLimit
      , size_t graphInd);


  //! returns future cost matrix for sentence
  inline virtual const SquareMatrix &GetFutureScore() const {
    return m_futureScore;
  }

//  //! list of trans opt for a particular span
//  const TransOptListPg2s &GetTranslationOptionList(const WordsRange &coverage) const {
//    return GetTranslationOptionList(coverage.GetStartPos(), coverage.GetEndPos());
//  }


  void Add(TransOptPg2s *translationOption);
  TransOptListPg2s &GetTranslationOptionList(const WordsSet& nids);
  const TransOptListPg2s &GetTranslationOptionList(const WordsSet& nids) const;
  void ProcessOneUnknownWord(const InputSubgraph &inputPath,size_t sourcePos, size_t length, const Scores *inputScores);

  TO_STRING();
};

}
}
#endif

