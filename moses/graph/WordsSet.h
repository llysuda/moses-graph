// $Id$

/***********************************************************************
Moses - factored phrase-based language decoder
Copyright (C) 2006 University of Edinburgh

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#ifndef moses_graph_WordsSet_h
#define moses_graph_WordsSet_h

#include <iostream>
#include <set>
#include <map>
#include <algorithm>
#include "moses/TypeDef.h"
#include "moses/Util.h"

#include "moses/WordsRange.h"

#ifdef WIN32
#undef max
#endif

namespace Moses
{
namespace Graph
{

/***
 * Efficient version of WordsBitmap for contiguous ranges
 */
class WordsSet
{
  friend std::ostream& operator << (std::ostream& out, const WordsSet& range);

  std::set<int> m_set;
  std::map<int, int> m_sortIndex;
public:
  inline WordsSet()
  	:m_set(),m_sortIndex(){}
//  inline WordsSet(const std::set<int>& wset) : m_set(wset) {
//
//  }
  inline WordsSet(const WordsSet &copy)
    : m_set(copy.m_set)
  	, m_sortIndex(copy.m_sortIndex){
  }

  inline size_t GetStartPos() const {
    if (m_set.size() == 0)
      return NOT_FOUND;
    return *m_set.begin();
  }
  inline size_t GetEndPos() const {
    if (m_set.size() == 0)
      return NOT_FOUND;
    return *m_set.rbegin();
  }

  int GetMin() const {
  	return *m_set.begin();
  }

  int GetMax() const {
		return *m_set.rbegin();
	}

  void Add (int pos, int index) {
  	m_set.insert(pos);
  	m_sortIndex[pos] = index;
  }

  std::map<int, int> GetSortIndex() const {
  	return m_sortIndex;
  }

  void Remove(int pos) {
  	m_set.erase(pos);
  	m_sortIndex.erase(pos);
  }

  bool HasIndex (int pos) const {
  	return m_set.find(pos) != m_set.end();
  }

  const std::set<int>& GetSet() const {
  	return m_set;
  }

  size_t GetSize() const {
  	return m_set.size();
  }

  std::vector<int> GetSortedVec() const {
  	std::vector<int> range;
  	range.resize(m_set.size());
  	std::map<int,int>::const_iterator iter;
  	for(iter = m_sortIndex.begin(); iter != m_sortIndex.end(); iter++) {
  		int nid = iter->first;
  		int id = iter->second;
  		range[id] = nid;
  	}
  	return range;
  }
  //! count of words translated
  inline size_t GetNumWordsCovered() const {
//    return (m_startPos == NOT_FOUND) ? 0 : m_endPos - m_startPos + 1;
  	return m_set.size();
  }

  //! transitive comparison
  inline bool operator<(const WordsSet& x) const {
//    return (m_startPos<x.m_startPos
//            || (m_startPos==x.m_startPos && m_endPos<x.m_endPos));
  	return m_set < x.m_set;
//  	size_t size = m_set.size();
//  	size_t xsize = x.m_set.size();
//
//  	if (size == 0 && xsize == 0)
//  		return true;
//  	if (size == 0)
//  		return false;
//  	if (xsize == 0)
//  		return true;
//
//  	return (size > xsize || (size == xsize && *(m_set.begin()) < *(x.m_set.begin())));
  }

  // equality operator
  inline bool operator==(const WordsSet& x) const {
//    return (m_startPos==x.m_startPos && m_endPos==x.m_endPos);
  	if (m_set.size() != x.m_set.size())
  		return false;
  	for (std::set<int>::const_iterator iter = m_set.begin();
  			iter != m_set.end(); ++iter)
  		if (x.m_set.find(*iter) == x.m_set.end())
  			return false;
  	return true;
  }
  // Whether two word ranges overlap or not
  inline bool Overlap(const WordsSet& x) const {
  	if (m_set.size() == 0 || x.m_set.size() == 0)
  	  return false;
  	for (std::set<int>::const_iterator iter = m_set.begin();
				iter != m_set.end(); ++iter)
			if (x.m_set.find(*iter) != x.m_set.end())
				return true;
		return false;
  }

  WordsRange ToRange() const {
    return WordsRange(GetStartPos(), GetEndPos());
  }

//  inline size_t GetNumWordsBetween(const WordsSet& x) const {
//    CHECK(!Overlap(x));
//
//    if (x.m_endPos < m_startPos) {
//      return m_startPos - x.m_endPos - 1;
//    }
//
//    return x.m_startPos - m_endPos - 1;
//  }


  TO_STRING();
};


}
}
#endif
