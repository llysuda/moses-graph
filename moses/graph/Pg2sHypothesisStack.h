#ifndef moses_Pg2sHypothesisStack_h
#define moses_Pg2sHypothesisStack_h

#include <vector>
#include <set>

#include "Pg2sHypothesis.h"
#include "moses/WordsBitmap.h"

namespace Moses
{
namespace Graph
{

class Pg2sManager;

/** abstract unique set of hypotheses that cover a certain number of words,
 *  ie. a stack in phrase-based decoding
 */
class Pg2sHypothesisStack
{

protected:
  typedef std::set< Pg2sHypothesis*, Pg2sHypothesisRecombinationOrderer > _HCType;
  _HCType m_hypos; /**< contains hypotheses */
  Pg2sManager& m_manager;

public:
  Pg2sHypothesisStack(Pg2sManager& manager): m_manager(manager) {}
  typedef _HCType::iterator iterator;
  typedef _HCType::const_iterator const_iterator;
  //! iterators
  const_iterator begin() const {
    return m_hypos.begin();
  }
  const_iterator end() const {
    return m_hypos.end();
  }
  size_t size() const {
    return m_hypos.size();
  }
  virtual inline float GetWorstScore() const {
    return -std::numeric_limits<float>::infinity();
  };
  virtual float GetWorstScoreForBitmap( WordsBitmapID ) {
    return -std::numeric_limits<float>::infinity();
  };
  virtual float GetWorstScoreForBitmap( const WordsBitmap& ) {
    return -std::numeric_limits<float>::infinity();
  };

  virtual ~Pg2sHypothesisStack();
  virtual bool AddPrune(Pg2sHypothesis *hypothesis) = 0;
  virtual const Pg2sHypothesis *GetBestHypothesis() const = 0;
  virtual std::vector<const Pg2sHypothesis*> GetSortedList() const = 0;

  //! remove hypothesis pointed to by iterator but don't delete the object
  virtual void Detach(const Pg2sHypothesisStack::iterator &iter);
  /** destroy Pg2sHypothesis pointed to by iterator (object pool version) */
  virtual void Remove(const Pg2sHypothesisStack::iterator &iter);

};

}
}
#endif
