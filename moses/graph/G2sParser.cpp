// $Id$
// vim:tabstop=2
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include "G2sParser.h"

#include "moses/StaticData.h"
#include "MultiDiGraphInput.h"
#include "moses/Sentence.h"
#include "moses/FF/UnknownWordPenaltyProducer.h"

#include "util/string_piece.hh"
#include "moses/Util.h"
#include <string>
#include "G2sTranslationOptionColl.h"
#include "G2sTranslationOptionList.h"
#include "G2sRuleLookupManager.h"

using namespace std;
using namespace Moses;

namespace Moses
{
namespace Graph
{

extern bool g_debug;

G2sParserUnknown::G2sParserUnknown() {}

G2sParserUnknown::~G2sParserUnknown()
{
  RemoveAllInColl(m_unksrcs);
  RemoveAllInColl(m_cacheTargetPhraseCollection);
}

//void G2sParserUnknown::Process(int NTCount, const WordsSet &range, G2sTranslationOptionColl &to)
//{
//  std::string line = "";
//  std::string align = "";
//  for(int i = 0; i < NTCount; i++) {
//    line += "[X][X] ";
//    align += SPrint<int>(i)+"-"+SPrint<int>(i)+" ";
//  }
//  line += "[X]";
//  align.erase(align.size()-1);
//
//  StringPiece linePiece(line.c_str());
//
//  // unknown word, add as trans opt
//  const StaticData &staticData = StaticData::Instance();
//  const UnknownWordPenaltyProducer *unknownWordPenaltyProducer = staticData.GetUnknownWordPenaltyProducer();
//
//  Word *targetLHS;
//  TargetPhrase *targetPhrase = new TargetPhrase();
//  targetPhrase->CreateFromString(Output,staticData.GetInputFactorOrder(), linePiece, staticData.GetFactorDelimiter(),&targetLHS);
//
//  StringPiece alignString(align.c_str());
//  targetPhrase->SetAlignmentInfo(alignString);
//  targetPhrase->SetTargetLHS(targetLHS);
//  float unknownScore = FloorScore(TransformScore(2.718));
//  targetPhrase->GetScoreBreakdown().Assign(unknownWordPenaltyProducer, unknownScore);
//  targetPhrase->Evaluate(*targetPhrase);
//  to.AddPhraseOOV(*targetPhrase, m_cacheTargetPhraseCollection, range);
//}

void G2sParserUnknown::Process(const Word &sourceWord, const WordsSet &range, G2sTranslationOptionColl &to)
{
  // unknown word, add as trans opt
  const StaticData &staticData = StaticData::Instance();
  const UnknownWordPenaltyProducer *unknownWordPenaltyProducer = staticData.GetUnknownWordPenaltyProducer();

  size_t isDigit = 0;
  if (staticData.GetDropUnknown()) {
    const Factor *f = sourceWord[0]; // TODO hack. shouldn't know which factor is surface
    const StringPiece s = f->GetString();
    isDigit = s.find_first_of("0123456789");
    if (isDigit == string::npos)
      isDigit = 0;
    else
      isDigit = 1;
    // modify the starting bitmap
  }

  Phrase* unksrc = new Phrase(1);
  unksrc->AddWord() = sourceWord;
  Word &newWord = unksrc->GetWord(0);
  newWord.SetIsOOV(true);

  m_unksrcs.push_back(unksrc);

  //TranslationOption *transOpt;
  if (! staticData.GetDropUnknown() || isDigit) {
    // loop
    const UnknownLHSList &lhsList = staticData.GetUnknownLHS();
    UnknownLHSList::const_iterator iterLHS;
    for (iterLHS = lhsList.begin(); iterLHS != lhsList.end(); ++iterLHS) {
      const string &targetLHSStr = iterLHS->first;
      float prob = iterLHS->second;

      // lhs
      //const Word &sourceLHS = staticData.GetInputDefaultNonTerminal();
      Word *targetLHS = new Word(true);

      targetLHS->CreateFromString(Output, staticData.GetOutputFactorOrder(), targetLHSStr, true);
      CHECK(targetLHS->GetFactor(0) != NULL);

      // add to dictionary
      TargetPhrase *targetPhrase = new TargetPhrase();
      Word &targetWord = targetPhrase->AddWord();
      targetWord.CreateUnknownWord(sourceWord);

      // scores
      float unknownScore = FloorScore(TransformScore(prob));

      targetPhrase->GetScoreBreakdown().Assign(unknownWordPenaltyProducer, unknownScore);
      targetPhrase->Evaluate(*unksrc);

      targetPhrase->SetTargetLHS(targetLHS);
      targetPhrase->SetAlignmentInfo("0-0");

      // chart rule
      to.AddPhraseOOV(*targetPhrase, m_cacheTargetPhraseCollection, range);
    } // for (iterLHS
  } else {
    // drop source word. create blank trans opt
    float unknownScore = FloorScore(-numeric_limits<float>::infinity());

    TargetPhrase *targetPhrase = new TargetPhrase();
    // loop
    const UnknownLHSList &lhsList = staticData.GetUnknownLHS();
    UnknownLHSList::const_iterator iterLHS;
    for (iterLHS = lhsList.begin(); iterLHS != lhsList.end(); ++iterLHS) {
      const string &targetLHSStr = iterLHS->first;
      //float prob = iterLHS->second;

      Word *targetLHS = new Word(true);
      targetLHS->CreateFromString(Output, staticData.GetOutputFactorOrder(), targetLHSStr, true);
      CHECK(targetLHS->GetFactor(0) != NULL);

      targetPhrase->GetScoreBreakdown().Assign(unknownWordPenaltyProducer, unknownScore);
      targetPhrase->Evaluate(*unksrc);

      targetPhrase->SetTargetLHS(targetLHS);

      targetPhrase->AddWord(*targetLHS);

      // chart rule
      to.AddPhraseOOV(*targetPhrase, m_cacheTargetPhraseCollection, range);
    }
  }
}

G2sParser::G2sParser(InputType const &source) :
  m_decodeGraphList(StaticData::Instance().GetDecodeGraphs()),
  m_source(source)
{
  const StaticData &staticData = StaticData::Instance();

  staticData.InitializeForInput(source);
  const std::vector<PhraseDictionary*> &dictionaries = staticData.GetPhraseDictionaries();
  m_ruleLookupManagers.reserve(dictionaries.size());
  for (std::vector<PhraseDictionary*>::const_iterator p = dictionaries.begin();
       p != dictionaries.end(); ++p) {
    const PhraseDictionary *dict = *p;
    PhraseDictionary *nonConstDict = const_cast<PhraseDictionary*>(dict);
    m_ruleLookupManagers.push_back(nonConstDict->CreateRuleLookupManager(*this));
  }
}

G2sParser::~G2sParser()
{
  RemoveAllInColl(m_ruleLookupManagers);
//  StaticData::Instance().CleanUpAfterSentenceProcessing(m_source);
}

void G2sParser::Create(G2sTranslationOptionColl &to)
{
  assert(m_decodeGraphList.size() == m_ruleLookupManagers.size());

  StaticData & staticData = StaticData::InstanceNonConst();

  std::vector <DecodeGraph*>::const_iterator iterDecodeGraph;
  std::vector <G2sRuleLookupManager*>::const_iterator iterRuleLookupManagers = m_ruleLookupManagers.begin();
  for (iterDecodeGraph = m_decodeGraphList.begin(); iterDecodeGraph != m_decodeGraphList.end(); ++iterDecodeGraph, ++iterRuleLookupManagers) {
    const DecodeGraph &decodeGraph = **iterDecodeGraph;
    assert(decodeGraph.GetSize() == 1);
    G2sRuleLookupManager &ruleLookupManager = **iterRuleLookupManagers;
    ruleLookupManager.GetG2sRuleCollection(to);
  }

  for (size_t i = 0; i < GetSize(); i++) {
  	WordsSet range;
  	range.Add(i, 0);
  	const G2sTranslationOptionList* list = to.GetTranslationOptionList(range);
		if (list == NULL || list->GetSize() == 0) {
			// create unknown words for 1 word coverage where we don't have any trans options
			const Word &sourceWord = m_source.GetWord(i);
			m_unknown.Process(sourceWord, range, to);
		}
  }

//  if (!staticData.GetGapGlue())
//    to.RemoveUnlikely();
  to.Evaluate(m_source);
  to.ApplyThreshold();
  to.CalcFutureScore();
  to.CacheLexReordering();
}


size_t G2sParser::GetSize() const
{
  return m_source.GetSize();
}

long G2sParser::GetTranslationId() const
{
  return m_source.GetTranslationId();
}
} // namespace Moses
}
