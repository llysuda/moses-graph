#ifndef moses_SearchPg2s_h
#define moses_SearchPg2s_h

#include <vector>
#include <set>
#include "moses/Search.h"
#include "Pg2sHypothesisStackNormal.h"
#include "moses/Timer.h"
#include "TransOptCollPg2s.h"
#include "moses/TypeDef.h"
#include "moses/Phrase.h"

#include "SentenceStatsPg2s.h"
#include "WordsSet.h"
#include "MultiDiGraphInput.h"

using namespace Moses;

namespace Moses
{

class InputType;

namespace Graph
{

class Pg2sManager;
class TransOptCollPg2s;

/** Functions and variables you need to decoder an input using the phrase-based decoder (NO cube-pruning)
 *  Instantiated by the Manager class
 */
class SearchPg2s
{
protected:
	const Phrase *m_constraint;
	Pg2sManager& m_manager;
	InputSubgraph m_inputPath; // for initial hypo
	TransOptPg2s m_initialTransOpt;
  const InputType &m_source;
  std::vector < Pg2sHypothesisStack* > m_hypoStackColl; /**< stacks to store hypotheses (partial translations) */
  // no of elements = no of words in source + 1
  clock_t m_start; /**< starting time, used for logging */
  size_t interrupted_flag; /**< flag indicating that decoder ran out of time (see switch -time-out) */
  Pg2sHypothesisStackNormal* actual_hypoStack; /**actual (full expanded) stack of hypotheses*/
  const TransOptCollPg2s &m_transOptColl; /**< pre-computed list of translation options for the phrases in this sentence */

  // functions for creating hypotheses
  void ProcessOneHypothesis(const Pg2sHypothesis &hypothesis);
  void ExpandAllHypotheses(const Pg2sHypothesis &hypothesis, const WordsSet& nids);
  virtual void ExpandHypothesis(const Pg2sHypothesis &hypothesis,const TransOptPg2s &transOpt, float expectedScore);

public:
  SearchPg2s(Pg2sManager& manager, const InputType &source, const TransOptCollPg2s &transOptColl);
  ~SearchPg2s();

  void ProcessSentence();

  void OutputHypoStackSize();
  void OutputHypoStack(int stack);

  virtual const std::vector < Pg2sHypothesisStack* >& GetHypothesisStacks() const;
  virtual const Pg2sHypothesis *GetBestHypothesis() const;
};

}
}

#endif
