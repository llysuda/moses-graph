/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include "G2sTranslationOptionColl.h"

#include <algorithm>
#include <iostream>
#include <vector>
#include "moses/StaticData.h"
#include "G2sTranslationOptions.h"
#include "WordsSet.h"
#include "moses/InputType.h"
#include "InputSubgraph.h"
#include "moses/LexicalReordering.h"
#include "MultiDiGraphInput.h"

using namespace std;

namespace Moses
{
namespace Graph
{

G2sTranslationOptionColl::G2sTranslationOptionColl(size_t ruleLimit, const InputType &input)
  : m_ruleLimit(ruleLimit)
	, m_input(input)
	, m_futureScore(input.GetSize())
{
}

//G2sTranslationOptionColl::G2sTranslationOptionColl(const G2sTranslationOptionColl& coll)
//	: m_ruleLimit(coll.m_ruleLimit)
//	, m_input(coll.m_input)
//	, m_coll (coll.m_coll)
//{
//
//}

G2sTranslationOptionColl::~G2sTranslationOptionColl()
{
 CollType::iterator iter=m_coll.begin();
 for(; iter != m_coll.end(); ++iter)
	 delete iter->second;
 m_coll.clear();
}

void G2sTranslationOptionColl::CalcFutureScore()
{
	size_t size = m_input.GetSize(); // the width of the matrix
	bool useGlue = true;//StaticData::Instance().GetGapGlue(); //only use terminal rules for future cost estimation

//	vector< vector<G2sTranslationOptionList*> > dtu;
	vector< vector< vector<WordsSet> > > dtuRange;
//	dtu.resize(size);
	dtuRange.resize(size);

	for(size_t row=0; row<size; row++) {
		for(size_t col=row; col<size; col++) {
			vector<WordsSet> dummy;
			dtuRange[row].push_back(dummy);
			m_futureScore.SetScore(row, col, -numeric_limits<float>::infinity());
		}
	}

	// walk all the translation options and record the cheapest option for each span
	CollType::iterator iter;
	size_t count = 0;
	for(iter = m_coll.begin(); iter != m_coll.end(); iter++) {
		G2sTranslationOptionList &transOptList = *(iter->second);
		const set<int>& range = iter->first.GetSet();

		int min = *(std::min_element(range.begin(), range.end()));
		int max = *(std::max_element(range.begin(), range.end()));

		int prev = -1;
		bool continous = true;
		for(set<int>::const_iterator it = range.begin();
				it != range.end(); it++) {
			if (prev != -1 && *it != prev+1) {
				continous = false;
				break;
			}
			prev = *it;
		}
		if (!continous) {
//			dtu[min][max-min] = &transOptList;
			dtuRange[min][max-min].push_back(iter->first);
			continue;
		}

		size_t s = transOptList.GetSize();
//		if (useGlue) s = 1;
		s = 1;
		for(size_t i = 0; i < s; i++) {
			const G2sTranslationOptions &transOpts = transOptList.Get(i);
			G2sTranslationOptions::const_iterator iter2;
			for (iter2 = transOpts.begin() ; iter2 != transOpts.end() ; ++iter2) {
				const G2sTranslationOption& transOpt = **iter2;
				float score = transOpt.GetFutureScore();
				if (score > m_futureScore.GetScore(min, max))
					m_futureScore.SetScore(min, max, score);
			}
		}
	}

	// now fill all the cells in the strictly upper triangle
	//   there is no way to modify the diagonal now, in the case
	//   where no translation option covers a single-word span,
	//   we leave the +inf in the matrix
	// like in chart parsing we want each cell to contain the highest score
	// of the full-span trOpt or the sum of scores of joining two smaller spans

	for(size_t colstart = 1; colstart < size ; colstart++) {
		for(size_t diagshift = 0; diagshift < size-colstart ; diagshift++) {
			size_t startPos = diagshift;
			size_t endPos = colstart+diagshift;

			// dtu
			const vector<WordsSet>& ranges = dtuRange[startPos][endPos-startPos];
			size_t sizeDTU = ranges.size();
			if (sizeDTU > 0) {
				for(size_t d = 0; d < sizeDTU; d++) {
					float scoreDTU = 0.0f;
					const WordsSet& range = ranges[d];
					const set<int>& bitSet = range.GetSet();
					int prev = -1;
					for(set<int>::const_iterator it = bitSet.begin(); it != bitSet.end(); it++) {
						if (prev != -1 && *it > prev+1) {
							scoreDTU += m_futureScore.GetScore(prev+1, *it-1);
						}
						prev = *it;
					}
					G2sTranslationOptionList &transOptList = *(m_coll.find(range)->second);
					size_t s = transOptList.GetSize();
//					if (useGlue) s = 1;
					s = 1;
					for(size_t i = 0; i < s; i++) {
						const G2sTranslationOptions &transOpts = transOptList.Get(i);
						G2sTranslationOptions::const_iterator iter2;
						for (iter2 = transOpts.begin() ; iter2 != transOpts.end() ; ++iter2) {
							const G2sTranslationOption& transOpt = **iter2;
							float score = transOpt.GetFutureScore() + scoreDTU;
							if (score > m_futureScore.GetScore(startPos, endPos))
								m_futureScore.SetScore(startPos, endPos, score);
						}
					}
				}
			}

			for(size_t joinAt = startPos; joinAt < endPos ; joinAt++)  {
				float joinedScore = m_futureScore.GetScore(startPos, joinAt)
														+ m_futureScore.GetScore(joinAt+1, endPos);
				if (joinedScore > m_futureScore.GetScore(startPos, endPos))
					m_futureScore.SetScore(startPos, endPos, joinedScore);
			}
		}
	}
}

void G2sTranslationOptionColl::Add(const TargetPhraseCollection &tpc,
                                     const WordsSet &range,
                                     const Phrase& sourcePhrase)
{
	if (m_coll.find(range) == m_coll.end()) {
		m_coll[range] = new G2sTranslationOptionList(m_ruleLimit, m_input);
	}
	G2sTranslationOptionList* list = m_coll[range];
	list->Add(tpc, range, sourcePhrase);
}

void G2sTranslationOptionColl::AddPhraseOOV(TargetPhrase &phrase, std::list<TargetPhraseCollection*> &waste_memory, const WordsSet &range)
{
	if (m_coll.find(range) == m_coll.end()) {
		m_coll[range] = new G2sTranslationOptionList(m_ruleLimit, m_input);
	}
	G2sTranslationOptionList* list = m_coll[range];
	list->AddPhraseOOV(phrase, waste_memory, range);
}

void G2sTranslationOptionColl::RemoveUnlikely()
{
  // remove unlikely translationoptions
  const MultiDiGraph& graph = static_cast<const MultiDiGraph&>(m_input);
  CollType::iterator iter = m_coll.begin();
  map<string, vector<const WordsSet*> > labelSet;
  for(; iter != m_coll.end(); ++iter) {
    string label = graph.GetLabel(iter->first.GetSet());
    if (labelSet.find(label) == labelSet.end())
      labelSet[label] = vector<const WordsSet*>();
    labelSet[label].push_back(&(iter->first));
  }
  iter = m_coll.begin();
  for(; iter != m_coll.end(); ++iter) {
    const set<int>& range = iter->first.GetSet();
    G2sTranslationOptionList &transOptList = *(iter->second);
    vector<size_t> delIndexes;
    // o is glue rule
    for (size_t i = 1; i < transOptList.GetSize(); i++) {
      const G2sTranslationOptions& transOpts = transOptList.Get(i);
      map<string, vector<const WordsSet*> >::const_iterator lit = labelSet.find(transOpts.GetNextLHS());
      if (lit == labelSet.end()) {
        delIndexes.push_back(i);
        continue;
      }
      const vector<const WordsSet*>& rangeVec = lit->second;
      bool valid = false;
      for (size_t j = 0; j < rangeVec.size(); j++) {
        set<int> frange = rangeVec[j]->GetSet();
        frange.insert(range.begin(), range.end());
        string label = graph.GetLabel(frange);
        if (label == transOpts.GetLHS()) {
          valid = true;
          break;
        }
      }
      if (!valid)
        delIndexes.push_back(i);
    }
    for(vector<size_t>::const_reverse_iterator diter=delIndexes.rbegin();
        diter != delIndexes.rend(); ++diter)
      transOptList.Remove(*diter);
  }
}

void G2sTranslationOptionColl::ApplyThreshold()
{
  CollType::iterator iter = m_coll.begin();
  for(; iter != m_coll.end(); ++iter)
      iter->second->ApplyThreshold();
}

void G2sTranslationOptionColl::Evaluate(const InputType &input)
{
  CollType::iterator iter = m_coll.begin();
  for(; iter != m_coll.end(); ++iter)
      iter->second->Evaluate(input);
}

void G2sTranslationOptionColl::CacheLexReordering()
{
  size_t size = m_input.GetSize();

  const std::vector<const StatefulFeatureFunction*> &ffs = StatefulFeatureFunction::GetStatefulFeatureFunctions();
  std::vector<const StatefulFeatureFunction*>::const_iterator iter;
  for (iter = ffs.begin(); iter != ffs.end(); ++iter) {
    const StatefulFeatureFunction &ff = **iter;
    if (typeid(ff) == typeid(LexicalReordering)) {
      const LexicalReordering &lexreordering = static_cast<const LexicalReordering&>(ff);

      CollType::iterator iter = m_coll.begin();
      for(; iter != m_coll.end(); iter++) {
        G2sTranslationOptionList &transOptList = *(iter->second);
        const WordsSet& range = iter->first;
        Word glueLabel;
        bool isglue = true;
        for (size_t i = 0; i < transOptList.GetSize(); i++) {
          G2sTranslationOptions& transOpts = transOptList.Get(i);
          G2sTranslationOptions::iterator iter = transOpts.begin();
          for (; iter != transOpts.end() ; ++iter) {
            G2sTranslationOption &transOpt = **iter;
            Phrase sourcePhrase = transOpt.GetSourcePhrase();

            if (isglue) {
              glueLabel = sourcePhrase.GetWord(sourcePhrase.GetSize()-1);
              isglue = false;
            }

            assert(sourcePhrase.GetSize() > 1);
            sourcePhrase.RemoveWord(sourcePhrase.GetSize()-1);
            sourcePhrase.AddWord(glueLabel);

            TargetPhrase tp = transOpt.GetTargetPhrase();
            Scores score = lexreordering.GetProb(sourcePhrase
                                                 , tp);

            if (!score.empty())
              transOpt.CacheLexReorderingScores(lexreordering, score);
          }
        }
      }

    } // if (typeid(ff) == typeid(LexicalReordering)) {
  } // for (iter = ffs.begin(); iter != ffs.end(); ++iter) {
}

}
}
