#ifndef moses_graph_TransOptListPg2s_h
#define moses_graph_TransOptListPg2s_h

#include <typeinfo>
#include <algorithm>
#include <vector>
#include "util/check.hh"
#include <iostream>
#include "moses/Util.h"

namespace Moses
{
namespace Graph
{

class TransOptPg2s;

/** wrapper around vector of translation options
 */
class TransOptListPg2s
{
  friend std::ostream& operator<<(std::ostream& out, const TransOptListPg2s& coll);

protected:
  typedef std::vector<TransOptPg2s*> CollType;
  CollType m_coll;
  float bestFutureScore;

public:
  typedef CollType::iterator iterator;
  typedef CollType::const_iterator const_iterator;
  const_iterator begin() const {
    return m_coll.begin();
  }
  const_iterator end() const {
    return m_coll.end();
  }
  iterator begin() {
    return m_coll.begin();
  }
  iterator end() {
    return m_coll.end();
  }

  TransOptListPg2s();
  TransOptListPg2s(const TransOptListPg2s &copy);
  ~TransOptListPg2s();

  void resize(size_t newSize) {
    m_coll.resize(newSize);
  }
  size_t size() const {
    return m_coll.size();
  }

  const TransOptPg2s *Get(size_t ind) const {
    CHECK(ind < m_coll.size());
    return m_coll[ind];
  }
  void Remove( size_t ind ) {
    CHECK(ind < m_coll.size());
    m_coll.erase( m_coll.begin()+ind );
  }
  void Add(TransOptPg2s *transOpt);

  float GetBestFutureCost() {
    return bestFutureScore;
  }

  TO_STRING();

};

}
}

#endif
