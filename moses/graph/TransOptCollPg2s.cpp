// $Id$
// vim:tabstop=2

/***********************************************************************
Moses - factored phrase-based language decoder
Copyright (C) 2006 University of Edinburgh

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#include "TransOptCollPg2s.h"

#include <typeinfo>
#include <algorithm>
#include <queue>

#include "MultiDiGraphInput.h"
#include "moses/DecodeStep.h"
#include "moses/LM/Base.h"
#include "moses/FactorCollection.h"
#include "moses/InputType.h"
#include "moses/LexicalReordering.h"
#include "moses/Util.h"
#include "moses/StaticData.h"
#include "moses/DecodeStepTranslation.h"
#include "moses/DecodeStepGeneration.h"
#include "moses/DecodeGraph.h"
#include "moses/FF/UnknownWordPenaltyProducer.h"
#include "InputSubgraph.h"
#include "moses/Phrase.h"

using namespace std;
using namespace Moses;

namespace Moses
{
namespace Graph
{

/** helper for pruning */
bool CompareTranslationOptionPg2s(const TransOptPg2s *a, const TransOptPg2s *b)
{
  return a->GetFutureScore() > b->GetFutureScore();
}

/** constructor; since translation options are indexed by coverage span, the corresponding data structure is initialized here
	* This fn should be called by inherited classes
*/
TransOptCollPg2s::TransOptCollPg2s(
  InputType const& src, size_t maxNoTransOptPerCoverage, float translationOptionThreshold)
  : m_source(src)
  ,m_futureScore(src.GetSize())
  ,m_maxNoTransOptPerCoverage(maxNoTransOptPerCoverage)
  ,m_translationOptionThreshold(translationOptionThreshold)
{

}

/** destructor, clears out data structures */
TransOptCollPg2s::~TransOptCollPg2s()
{
  RemoveAllInColl(m_phraseDictionaryQueue);
}

void TransOptCollPg2s::Prune()
{
  // quit, if max size, threshold
  if (m_maxNoTransOptPerCoverage == 0 && m_translationOptionThreshold == -std::numeric_limits<float>::infinity())
    return;

  // bookkeeping for how many options used, pruned
  size_t total = 0;
  size_t totalPruned = 0;

  // loop through all spans
  size_t size = m_source.GetSize();
  iterator iter = m_collection.begin();
	for(; iter != m_collection.end(); iter++) {
//		const set<int>& range = iter->first;
		TransOptListPg2s& fullList = iter->second;
		total += fullList.size();

		// size pruning
		if (m_maxNoTransOptPerCoverage > 0 &&
				fullList.size() > m_maxNoTransOptPerCoverage) {
			// sort in vector
			nth_element(fullList.begin(), fullList.begin() + m_maxNoTransOptPerCoverage, fullList.end(), CompareTranslationOptionPg2s);
			totalPruned += fullList.size() - m_maxNoTransOptPerCoverage;

			// delete the rest
			for (size_t i = m_maxNoTransOptPerCoverage ; i < fullList.size() ; ++i) {
				delete fullList.Get(i);
			}
			fullList.resize(m_maxNoTransOptPerCoverage);
		}

		// threshold pruning
		if (fullList.size() > 1 && m_translationOptionThreshold != -std::numeric_limits<float>::infinity()) {
			// first, find the best score
			float bestScore = -std::numeric_limits<float>::infinity();
			for (size_t i=0; i < fullList.size() ; ++i) {
				if (fullList.Get(i)->GetFutureScore() > bestScore)
					bestScore = fullList.Get(i)->GetFutureScore();
			}
			//std::cerr << "best score for span " << startPos << "-" << endPos << " is " << bestScore << "\n";
			// then, remove items that are worse than best score + threshold
			for (size_t i=0; i < fullList.size() ; ++i) {
				if (fullList.Get(i)->GetFutureScore() < bestScore + m_translationOptionThreshold) {
					//std::cerr << "\tremoving item " << i << ", score " << fullList.Get(i)->GetFutureScore() << ": " << fullList.Get(i)->GetTargetPhrase() << "\n";
					delete fullList.Get(i);
					fullList.Remove(i);
					total--;
					totalPruned++;
					i--;
				}
				//else
				//{
				//	std::cerr << "\tkeeping item " << i << ", score " << fullList.Get(i)->GetFutureScore() << ": " << fullList.Get(i)->GetTargetPhrase() << "\n";
				//}
			}
		} // end of threshold pruning
	}

  VERBOSE(2,"       Total translation options: " << total << std::endl
          << "Total translation options pruned: " << totalPruned << std::endl);
}

/** Force a creation of a translation option where there are none for a particular source position.
* ie. where a source word has not been translated, create a translation option by
*				1. not observing the table limits on phrase/generation tables
*				2. using the handler ProcessUnknownWord()
* Call this function once translation option collection has been filled with translation options
*
* This function calls for unknown words is complicated by the fact it must handle different input types.
* The call stack is
*		Base::ProcessUnknownWord()
*			Inherited::ProcessUnknownWord(position)
*				Base::ProcessOneUnknownWord()
*
*/

void TransOptCollPg2s::ProcessUnknownWord()
{
  const vector<DecodeGraph*>& decodeGraphList = StaticData::Instance().GetDecodeGraphs();
  size_t size = m_source.GetSize();
  // try to translation for coverage with no trans by expanding table limit
  for (size_t graphInd = 0 ; graphInd < decodeGraphList.size() ; graphInd++) {
    const DecodeGraph &decodeGraph = *decodeGraphList[graphInd];
    for (size_t pos = 0 ; pos < size ; ++pos) {
    	WordsSet nids;
    	nids.Add((int)pos, 0);
      TransOptListPg2s &fullList = GetTranslationOptionList(nids);
      size_t numTransOpt = fullList.size();
      if (numTransOpt == 0) {
				//CreateTranslationOptionsForRange(decodeGraph, pos, pos, false, graphInd);
				// TODO consider discarded translation options
				const vector <DecodeGraph*> &decodeGraphList = StaticData::Instance().GetDecodeGraphs();

				bool adhereTableLimit = false;

				// loop over all decoding graphs, each generates translation options
				for (size_t graphInd = 0 ; graphInd < decodeGraphList.size() ; graphInd++) {
					if (decodeGraphList.size() > 1) {
						VERBOSE(3,"Creating translation options from decoding graph " << graphInd << endl);
					}

					const DecodeGraph &decodeGraph = *decodeGraphList[graphInd];

					Phrase* p = new Phrase();
					p->AddWord(m_source.GetWord(pos));
					NonTerminalSet nt;
					InputSubgraph* inputPath = new InputSubgraph(*p, nt, nids);
					CreateTranslationOptionsForRange(decodeGraph, *inputPath ,adhereTableLimit , graphInd);

					m_phraseDictionaryQueue.push_back(inputPath);

					delete p;
				}
      }
    }
  }

  bool alwaysCreateDirectTranslationOption = StaticData::Instance().IsAlwaysCreateDirectTranslationOption();
  // create unknown words for 1 word coverage where we don't have any trans options
  for (size_t pos = 0 ; pos < size ; ++pos) {
  	WordsSet nids;
  	nids.Add((int)pos, 0);
    TransOptListPg2s &fullList = GetTranslationOptionList(nids);
    if (fullList.size() == 0 || alwaysCreateDirectTranslationOption)
    	ProcessUnknownWord(pos);
  }
}

/** special handling of ONE unknown words. Either add temporarily add word to translation table,
	* or drop the translation.
	* This function should be called by the ProcessOneUnknownWord() in the inherited class
	* At the moment, this unknown word handler is a bit of a hack, if copies over each factor from source
	* to target word, or uses the 'UNK' factor.
	* Ideally, this function should be in a class which can be expanded upon, for example,
	* to create a morphologically aware handler.
	*
	* \param sourceWord the unknown word
	* \param sourcePos
	* \param length length covered by this word (may be > 1 for lattice input)
	* \param inputScores a set of scores associated with unknown word (input scores from latties/CNs)
 */
//void TransOptCollPg2s::ProcessOneUnknownWord(const InputSubgraph &inputPath,size_t sourcePos, size_t length, const Scores *inputScores)
//
//{
//  const StaticData &staticData = StaticData::Instance();
//  const UnknownWordPenaltyProducer *unknownWordPenaltyProducer = staticData.GetUnknownWordPenaltyProducer();
//  float unknownScore = FloorScore(TransformScore(0));
//  const Word &sourceWord = inputPath.GetPhrase().GetWord(0);
//
//  // unknown word, add as trans opt
//  FactorCollection &factorCollection = FactorCollection::Instance();
//
//  size_t isDigit = 0;
//
//  const Factor *f = sourceWord[0]; // TODO hack. shouldn't know which factor is surface
//  const StringPiece s = f->GetString();
//  bool isEpsilon = (s=="" || s==EPSILON);
//  if (StaticData::Instance().GetDropUnknown()) {
//
//
//    isDigit = s.find_first_of("0123456789");
//    if (isDigit == string::npos)
//      isDigit = 0;
//    else
//      isDigit = 1;
//    // modify the starting bitmap
//  }
//
//  TargetPhrase targetPhrase;
//
//  if (!(staticData.GetDropUnknown() || isEpsilon) || isDigit) {
//    // add to dictionary
//
//    Word &targetWord = targetPhrase.AddWord();
//    targetWord.SetIsOOV(true);
//
//    for (unsigned int currFactor = 0 ; currFactor < MAX_NUM_FACTORS ; currFactor++) {
//      FactorType factorType = static_cast<FactorType>(currFactor);
//
//      const Factor *sourceFactor = sourceWord[currFactor];
//      if (sourceFactor == NULL)
//        targetWord[factorType] = factorCollection.AddFactor(UNKNOWN_FACTOR);
//      else
//        targetWord[factorType] = factorCollection.AddFactor(sourceFactor->GetString());
//    }
//    //create a one-to-one alignment between UNKNOWN_FACTOR and its verbatim translation
//
//    targetPhrase.SetAlignmentInfo("0-0");
//
//  } else {
//    // drop source word. create blank trans opt
//
//    //targetPhrase.SetAlignment();
//
//  }
//
//  targetPhrase.GetScoreBreakdown().Assign(unknownWordPenaltyProducer, unknownScore);
//
//  // source phrase
//  const Phrase &sourcePhrase = inputPath.GetPhrase();
//  m_unksrcs.push_back(&sourcePhrase);
//  WordsRange range(sourcePos, sourcePos + length - 1);
//
//  targetPhrase.Evaluate(sourcePhrase);
//
//  TransOptPg2s *transOpt = new TransOptPg2s(range, targetPhrase);
//  transOpt->SetInputPath(inputPath);
//  Add(transOpt);
//
//
//}



/** Create all possible translations from the phrase tables
 * for a particular input sentence. This implies applying all
 * translation and generation steps. Also computes future cost matrix.
 */
void TransOptCollPg2s::CreateTranslationOptions()
{
  // loop over all substrings of the source sentence, look them up
  // in the phraseDictionary (which is the- possibly filtered-- phrase
  // table loaded on initialization), generate TransOptPg2s objects
  // for all phrases

  // there may be multiple decoding graphs (factorizations of decoding)
  const vector <DecodeGraph*> &decodeGraphList = StaticData::Instance().GetDecodeGraphs();
  const vector <size_t> &decodeGraphBackoff = StaticData::Instance().GetDecodeGraphBackoff();

  bool adhereTableLimit = true;

  // length of the sentence
  const size_t size = m_source.GetSize();

  // loop over all decoding graphs, each generates translation options
  for (size_t graphInd = 0 ; graphInd < decodeGraphList.size() ; graphInd++) {
    if (decodeGraphList.size() > 1) {
      VERBOSE(3,"Creating translation options from decoding graph " << graphInd << endl);
    }

    const DecodeGraph &decodeGraph = *decodeGraphList[graphInd];

    // collect phrases by searching phrase-able
    list <const DecodeStep* >::const_iterator iterStep;
		for (iterStep = decodeGraph.begin(); iterStep != decodeGraph.end() ; ++iterStep) {
			const DecodeStep &decodeStep = **iterStep;
			const DecodeStepTranslation *transStep = dynamic_cast<const DecodeStepTranslation *>(&decodeStep);
			if (transStep) {
				const PhraseDictionary &phraseDictionary = *transStep->GetPhraseDictionaryFeature();
				phraseDictionary.GetTargetPhraseCollectionBatch(m_phraseDictionaryQueue, m_source);
			}
		}

		for (InputSubgraphList::const_iterator iter = m_phraseDictionaryQueue.begin();
				iter != m_phraseDictionaryQueue.end(); iter++) {
			const InputSubgraph& inputPath = **iter;
			CreateTranslationOptionsForRange(decodeGraph,inputPath ,adhereTableLimit , graphInd);
			// TRACE_ERR( "Early translation options pruned: " << totalEarlyPruned << endl);
		} // if ((StaticData::Instance().GetXmlInputType() != XmlExclusive) || !HasXmlOptionsOverlappingRange(startPos,endPos))

	}

  VERBOSE(2,"Translation Option Collection\n " << *this << endl);

  //UpdateSubgraphMatrix();

  ProcessUnknownWord();

  EvaluateWithSource();

  // Prune
  Prune();

  Sort();

  CalcFutureScore();

  // Cached lex reodering costs
  CacheLexReordering();
}

void TransOptCollPg2s::CalcFutureScore()
{
	size_t size = m_source.GetSize(); // the width of the matrix

//	vector< vector< vector<TransOptListPg2s*> > > dtu;
	vector< vector< vector<WordsSet> > > dtuRange;
//	dtu.resize(size);
	dtuRange.resize(size);

	for(size_t row=0; row<size; row++) {
		for(size_t col=row; col<size; col++) {
			vector<WordsSet> dummy;
			dtuRange[row].push_back(dummy);
			m_futureScore.SetScore(row, col, -numeric_limits<float>::infinity());
		}
	}

	// walk all the translation options and record the cheapest option for each span
	std::map<WordsSet, TransOptListPg2s>::iterator iter;
	for(iter = m_collection.begin(); iter != m_collection.end(); iter++) {
		TransOptListPg2s &transOptList = iter->second;
		const WordsSet& range = iter->first;

		int min = range.GetMin();
		int max = range.GetMax();

		int prev = -1;
		bool continous = true;
		for(set<int>::const_iterator it = range.GetSet().begin();
				it != range.GetSet().end(); it++) {
			if (prev != -1 && *it != prev+1) {
				continous = false;
				break;
			}
			prev = *it;
		}
		if (!continous) {
			dtuRange[min][max-min].push_back(range);
			continue;
		}

			float score = transOptList.GetBestFutureCost();
			if (score > m_futureScore.GetScore(min, max))
				m_futureScore.SetScore(min, max, score);
	}

	// now fill all the cells in the strictly upper triangle
	//   there is no way to modify the diagonal now, in the case
	//   where no translation option covers a single-word span,
	//   we leave the +inf in the matrix
	// like in chart parsing we want each cell to contain the highest score
	// of the full-span trOpt or the sum of scores of joining two smaller spans

	for(size_t colstart = 1; colstart < size ; colstart++) {
		for(size_t diagshift = 0; diagshift < size-colstart ; diagshift++) {
			size_t startPos = diagshift;
			size_t endPos = colstart+diagshift;

			// dtu
			const vector<WordsSet >& ranges = dtuRange[startPos][endPos-startPos];
			size_t sizeDTU = ranges.size();
			if (sizeDTU > 0) {
				for(size_t d = 0; d < sizeDTU; d++) {
					float scoreDTU = 0.0f;
					const WordsSet& range = ranges[d];
					int prev = -1;
					for(set<int>::const_iterator it = range.GetSet().begin(); it != range.GetSet().end(); it++) {
						if (prev != -1 && *it > prev+1) {
							scoreDTU += m_futureScore.GetScore(prev+1, *it-1);
						}
						prev = *it;
					}
					TransOptListPg2s &transOptList = m_collection.find(range)->second;
						float score = transOptList.GetBestFutureCost() + scoreDTU;
						if (score > m_futureScore.GetScore(startPos, endPos))
							m_futureScore.SetScore(startPos, endPos, score);
				}
			}

			for(size_t joinAt = startPos; joinAt < endPos ; joinAt++)  {
				float joinedScore = m_futureScore.GetScore(startPos, joinAt)
														+ m_futureScore.GetScore(joinAt+1, endPos);
				if (joinedScore > m_futureScore.GetScore(startPos, endPos))
					m_futureScore.SetScore(startPos, endPos, joinedScore);
			}
		}
	}
}


typedef pair<WordsSet, float> WordsSetScore;
class WordsSetScoreComparator
{
public:
  WordsSetScoreComparator() {}
  bool operator() (const WordsSetScore& lhs, const WordsSetScore&rhs) const
  {
    return (lhs.second<rhs.second);
  }
};

void TransOptCollPg2s::CalcFutureScoreFancy()
{
    size_t size = m_source.GetSize(); // the width of the matrix
    vector< vector< vector<WordsSet> > > dtuRange;
    dtuRange.resize(size);

    for(size_t row=0; row<size; row++) {
        for(size_t col=row; col<size; col++) {
            vector<WordsSet> dummy;
            dtuRange[row].push_back(dummy);
            m_futureScore.SetScore(row, col, -numeric_limits<float>::infinity());
        }
    }

    // walk all the translation options and record the cheapest option for each span
    std::map<WordsSet, TransOptListPg2s>::iterator iter;
    for(iter = m_collection.begin(); iter != m_collection.end(); iter++) {
        TransOptListPg2s &transOptList = iter->second;
        const WordsSet& range = iter->first;

        int min = range.GetMin();
        int max = range.GetMax();

        int prev = -1;
        bool continous = true;
        for(set<int>::const_iterator it = range.GetSet().begin();
                it != range.GetSet().end(); it++) {
            if (prev != -1 && *it != prev+1) {
                continous = false;
                break;
            }
            prev = *it;
        }
        if (!continous) {
            dtuRange[min][max-min].push_back(range);
            continue;
        }

        float score = transOptList.GetBestFutureCost();
        if (score > m_futureScore.GetScore(min, max))
            m_futureScore.SetScore(min, max, score);
    }

    // now fill all the cells in the strictly upper triangle
    //   there is no way to modify the diagonal now, in the case
    //   where no translation option covers a single-word span,
    //   we leave the +inf in the matrix
    // like in chart parsing we want each cell to contain the highest score
    // of the full-span trOpt or the sum of scores of joining two smaller spans

    for(size_t colstart = 1; colstart < size ; colstart++) {
        for(size_t diagshift = 0; diagshift < size-colstart ; diagshift++) {
            size_t startPos = diagshift;
            size_t endPos = colstart+diagshift;

            // dtu
            const vector<WordsSet >& ranges = dtuRange[startPos][endPos-startPos];
            size_t sizeDTU = ranges.size();
            if (sizeDTU > 0) {
              for(size_t d = 0; d < sizeDTU; d++) {
                float scoreDTU = 0.0f;
                const WordsSet& range = ranges[d];
                int prev = -1;
                for(set<int>::const_iterator it = range.GetSet().begin(); it != range.GetSet().end(); it++) {
                    if (prev != -1 && *it > prev+1) {
                        scoreDTU += m_futureScore.GetScore(prev+1, *it-1);
                    }
                    prev = *it;
                }
                TransOptListPg2s &transOptList = m_collection.find(range)->second;
                float score = transOptList.GetBestFutureCost() + scoreDTU;
                if (score > m_futureScore.GetScore(startPos, endPos))
                    m_futureScore.SetScore(startPos, endPos, score);
              }
            }

            for(size_t joinAt = startPos; joinAt < endPos ; joinAt++)  {
              float joinedScore = m_futureScore.GetScore(startPos, joinAt)
                                                      + m_futureScore.GetScore(joinAt+1, endPos);
              if (joinedScore > m_futureScore.GetScore(startPos, endPos))
                  m_futureScore.SetScore(startPos, endPos, joinedScore);
            }

            // combine DTUs for fancy calculation
            // use greedy search for speed-up
            if (endPos-startPos >= 3) {
              //collect <wordset,score> pair
              priority_queue<WordsSetScore, vector<WordsSetScore>, WordsSetScoreComparator> queue;
              for (size_t s = startPos; s < endPos; s++) {
                for(size_t e = s+2; e <= endPos; e++) {
                  const vector<WordsSet >& ranges = dtuRange[s][e-s];
                  for(size_t d = 0; d < ranges.size(); d++) {
                    const WordsSet& range = ranges[d];
                    TransOptListPg2s &transOptList = m_collection.find(range)->second;
                    float score = transOptList.GetBestFutureCost();
                    queue.push(make_pair(range,score/range.GetSize()));
                  }
                }
              }
              // greedy search
              float bestScore = m_futureScore.GetScore(startPos, endPos);
              WordsBitmap bitmap(size);
              float score = 0.0f;
              while (queue.size() > 0) {
                const WordsSetScore& ele = queue.top();
                const set<int>& nids = ele.first.GetSet();
                if (*nids.begin() >= startPos && *nids.rbegin() <= endPos && !bitmap.Overlap(nids)) {
                  bitmap.SetValue(nids,true);
                  score += ele.second * nids.size();
                  // collect scores
                  float futureScore = 0.0f;
                  const size_t notInGap= numeric_limits<size_t>::max();
                  size_t startGap = notInGap;
                  for(size_t currPos = startPos ; currPos <= endPos ; currPos++) {
                    // start of a new gap?
                    if(bitmap.GetValue(currPos) == false && startGap == notInGap) {
                      startGap = currPos;
                    }
                    // end of a gap?
                    else if(bitmap.GetValue(currPos) == true && startGap != notInGap) {
                      futureScore += m_futureScore.GetScore(startGap, currPos - 1);
                      startGap = notInGap;
                    }
                  }
                  // coverage ending with gap?
                  if (startGap != notInGap) {
                    futureScore += m_futureScore.GetScore(startGap, endPos);
                  }
                  //
                  futureScore += score;
                  if (futureScore > bestScore)
                    bestScore = futureScore;
                }
                queue.pop();
              }
              // update
              if (bestScore > m_futureScore.GetScore(startPos, endPos)) {
                cerr << bestScore << " -> " << m_futureScore.GetScore(startPos, endPos) << endl;;
                m_futureScore.SetScore(startPos, endPos, bestScore);
              }
            }
            // end fancy calculation
        }
    }
}

void TransOptCollPg2s::CreateTranslationOptionsForRange(const DecodeGraph &decodeStepList
    , const Moses::Graph::InputSubgraph& inputPath
    , bool adhereTableLimit
    , size_t graphInd)
{
	PartialTranslOptCollPg2s* oldPtoc = new PartialTranslOptCollPg2s();
	size_t totalEarlyPruned = 0;
	list <const DecodeStep* >::const_iterator iterStep = decodeStepList.begin();
	const DecodeStep &decodeStep = **iterStep;
	const PhraseDictionary &phraseDictionary = *decodeStep.GetPhraseDictionaryFeature();
	const TargetPhraseCollection *targetPhrases = inputPath.GetTargetPhrases(phraseDictionary);

	const WordsSet& range = inputPath.GetWordsRange();

	static_cast<const DecodeStepTranslation&>(decodeStep).ProcessInitialTranslation
		(m_source, *oldPtoc
		 , range, adhereTableLimit
		 , inputPath, targetPhrases);

	SetInputScore(inputPath, *oldPtoc);

	// do rest of decode steps
	int indexStep = 0;

	for (++iterStep ; iterStep != decodeStepList.end() ; ++iterStep) {

		const DecodeStep *decodeStep = *iterStep;
		PartialTranslOptCollPg2s* newPtoc = new PartialTranslOptCollPg2s();

		// go thru each intermediate trans opt just created
		const vector<TransOptPg2s*>& partTransOptList = oldPtoc->GetList();
		vector<TransOptPg2s*>::const_iterator iterPartialTranslOpt;
		for (iterPartialTranslOpt = partTransOptList.begin() ; iterPartialTranslOpt != partTransOptList.end() ; ++iterPartialTranslOpt) {
			TransOptPg2s &inputPartialTranslOpt = **iterPartialTranslOpt;

			if (const DecodeStepTranslation *translateStep = dynamic_cast<const DecodeStepTranslation*>(decodeStep) ) {
				const PhraseDictionary &phraseDictionary = *translateStep->GetPhraseDictionaryFeature();
				const TargetPhraseCollection *targetPhrases = inputPath.GetTargetPhrases(phraseDictionary);
				translateStep->Process(inputPartialTranslOpt
															 , *decodeStep
															 , *newPtoc
															 , this
															 , adhereTableLimit
															 , targetPhrases);
			}
		}

		// last but 1 partial trans not required anymore
		totalEarlyPruned += newPtoc->GetPrunedCount();
		delete oldPtoc;
		oldPtoc = newPtoc;

		indexStep++;
	} // for (++iterStep

	// add to fully formed translation option list
	PartialTranslOptCollPg2s &lastPartialTranslOptColl	= *oldPtoc;
	const vector<TransOptPg2s*>& partTransOptList = lastPartialTranslOptColl.GetList();
	vector<TransOptPg2s*>::const_iterator iterColl;
	for (iterColl = partTransOptList.begin() ; iterColl != partTransOptList.end() ; ++iterColl) {
		TransOptPg2s *transOpt = *iterColl;
		if (StaticData::Instance().GetXmlInputType() != XmlConstraint) {
			Add(transOpt);
		}
	}

	lastPartialTranslOptColl.DetachAll();
	totalEarlyPruned += oldPtoc->GetPrunedCount();
	delete oldPtoc;
}

void TransOptCollPg2s::SetInputScore(const InputSubgraph &inputPath, PartialTranslOptCollPg2s &oldPtoc)
{
  const ScoreComponentCollection *inputScore = inputPath.GetInputScore();
  if (inputScore == NULL) {
    return;
  }

  const std::vector<TransOptPg2s*> &transOpts = oldPtoc.GetList();
  for (size_t i = 0; i < transOpts.size(); ++i) {
    TransOptPg2s &transOpt = *transOpts[i];

    ScoreComponentCollection &scores = transOpt.GetScoreBreakdown();
    scores.PlusEquals(*inputScore);

  }
}

void TransOptCollPg2s::EvaluateWithSource()
{
//  const size_t size = m_source.GetSize();
//  for (size_t startPos = 0 ; startPos < size ; ++startPos) {
//    size_t maxSize = m_source.GetSize() - startPos;
//    size_t maxSizePhrase = StaticData::Instance().GetMaxPhraseLength();
//    maxSize = std::min(maxSize, maxSizePhrase);
//
//    for (size_t endPos = startPos ; endPos < startPos + maxSize ; ++endPos) {
//      TranslationOptionList &transOptList = GetTranslationOptionList(startPos, endPos);
//
//      TranslationOptionList::const_iterator iterTransOpt;
//      for(iterTransOpt = transOptList.begin() ; iterTransOpt != transOptList.end() ; ++iterTransOpt) {
//        TransOptPg2s &transOpt = **iterTransOpt;
//        transOpt.Evaluate(m_source);
//      }
//    }
//  }
	iterator iter = m_collection.begin();
	for(; iter != m_collection.end(); iter++) {
		const WordsSet& range = iter->first;
		TransOptListPg2s& transOptList = iter->second;
		TransOptListPg2s::const_iterator iterTransOpt;
		for(iterTransOpt = transOptList.begin() ; iterTransOpt != transOptList.end() ; ++iterTransOpt) {
			TransOptPg2s &transOpt = **iterTransOpt;
			transOpt.Evaluate(m_source);
		}
	}
}

void TransOptCollPg2s::Sort()
{
//  size_t size = m_source.GetSize();
//  for (size_t startPos = 0 ; startPos < size; ++startPos) {
//    size_t maxSize = size - startPos;
//    size_t maxSizePhrase = StaticData::Instance().GetMaxPhraseLength();
//    maxSize = std::min(maxSize, maxSizePhrase);
//
//    for (size_t endPos = startPos ; endPos < startPos + maxSize; ++endPos) {
//      TranslationOptionList &transOptList = GetTranslationOptionList(startPos, endPos);
//      std::sort(transOptList.begin(), transOptList.end(), CompareTranslationOption);
//    }
//  }

  iterator iter = m_collection.begin();
	for(; iter != m_collection.end(); iter++) {
//		const set<int>& range = iter->first;
		TransOptListPg2s& transOptList = iter->second;
		std::sort(transOptList.begin(), transOptList.end(), CompareTranslationOptionPg2s);
	}
}



/** Add translation option to the list
 * \param translationOption translation option to be added */
void TransOptCollPg2s::Add(TransOptPg2s *translationOption)
{
  const WordsSet &coverage = translationOption->GetSourceWordsRange();
  //CHECK(coverage.GetEndPos() - coverage.GetStartPos() < m_collection[coverage.GetStartPos()].size());
  m_collection[coverage].Add(translationOption);
}

TO_STRING_BODY(TransOptCollPg2s);

std::ostream& operator<<(std::ostream& out, const TransOptCollPg2s& coll)
{
//  size_t size = coll.GetSize();
//  for (size_t startPos = 0 ; startPos < size ; ++startPos) {
//    size_t maxSize = size - startPos;
//    size_t maxSizePhrase = StaticData::Instance().GetMaxPhraseLength();
//    maxSize = std::min(maxSize, maxSizePhrase);
//
//    for (size_t endPos = startPos ; endPos < startPos + maxSize ; ++endPos) {
//      const TranslationOptionList& fullList = coll.GetTranslationOptionList(startPos, endPos);
//      size_t sizeFull = fullList.size();
//      for (size_t i = 0; i < sizeFull; i++) {
//        out << *fullList.Get(i) << std::endl;
//      }
//    }
//  }
	out << "TransOptCollPg2s output" << endl;
//  iterator iter = begin();
//	for(; iter != end(); iter++) {
//		const set<int>& range = iter->first;
//		TransOptListPg2s& transOptList = iter->second();
//		std::sort(transOptList.begin(), transOptList.end(), CompareTranslationOption);
//	}

  //std::vector< std::vector< TranslationOptionList > >::const_iterator i = coll.m_collection.begin();
  //size_t j = 0;
  //for (; i!=coll.m_collection.end(); ++i) {
  //out << "s[" << j++ << "].size=" << i->size() << std::endl;
  //}

  return out;
}


//! list of trans opt for a particular span
TransOptListPg2s &TransOptCollPg2s::GetTranslationOptionList(const WordsSet& nids)
{
//  size_t maxSize = endPos - startPos;
//  size_t maxSizePhrase = StaticData::Instance().GetMaxPhraseLength();
//  maxSize = std::min(maxSize, maxSizePhrase);
//
//  CHECK(maxSize < m_collection[startPos].size());
//  return m_collection[startPos][maxSize];
	std::map<WordsSet, TransOptListPg2s>::iterator iter = m_collection.find(nids);
	if (iter == m_collection.end()) {
		m_collection[nids] = TransOptListPg2s();
		return m_collection.find(nids)->second;
	} else
		return iter->second;
}
const TransOptListPg2s &TransOptCollPg2s::GetTranslationOptionList(const WordsSet& nids) const
{
	return m_collection.find(nids)->second;
}

//void TransOptCollPg2s::GetTargetPhraseCollectionBatch()
//{
//  const vector <DecodeGraph*> &decodeGraphList = StaticData::Instance().GetDecodeGraphs();
//  for (size_t graphInd = 0 ; graphInd < decodeGraphList.size() ; graphInd++) {
//    const DecodeGraph &decodeGraph = *decodeGraphList[graphInd];
//
//    list <const DecodeStep* >::const_iterator iterStep;
//    for (iterStep = decodeGraph.begin(); iterStep != decodeGraph.end() ; ++iterStep) {
//      const DecodeStep &decodeStep = **iterStep;
//      const DecodeStepTranslation *transStep = dynamic_cast<const DecodeStepTranslation *>(&decodeStep);
//      if (transStep) {
//        const PhraseDictionary &phraseDictionary = *transStep->GetPhraseDictionaryFeature();
//        phraseDictionary.GetTargetPhraseCollectionBatch(m_phraseDictionaryQueue);
//      }
//    }
//  }
//}


/* forcibly create translation option for a particular source word.
	* For text, this function is easy, just call the base class' ProcessOneUnknownWord()
*/
void TransOptCollPg2s::ProcessUnknownWord(size_t sourcePos)
{
//  const InputSubgraph &inputPath = GetInputPath(sourcePos, sourcePos);
//  ProcessOneUnknownWord(inputPath,sourcePos);
	Phrase* p = new Phrase();
	p->AddWord(m_source.GetWord(sourcePos));
	NonTerminalSet nt;
	WordsSet range;
	range.Add((int)sourcePos,0);

	InputSubgraph* inputPath = new InputSubgraph(*p, nt, range);

	m_phraseDictionaryQueue.push_back(inputPath);

	const StaticData &staticData = StaticData::Instance();
	const UnknownWordPenaltyProducer *unknownWordPenaltyProducer = staticData.GetUnknownWordPenaltyProducer();
	float unknownScore = FloorScore(TransformScore(0));
	const Word &sourceWord = inputPath->GetPhrase().GetWord(0);

	// unknown word, add as trans opt
	FactorCollection &factorCollection = FactorCollection::Instance();

	size_t isDigit = 0;

	const Factor *f = sourceWord[0]; // TODO hack. shouldn't know which factor is surface
	const StringPiece s = f->GetString();
	bool isEpsilon = (s=="" || s==EPSILON);
	if (StaticData::Instance().GetDropUnknown()) {


		isDigit = s.find_first_of("0123456789");
		if (isDigit == string::npos)
			isDigit = 0;
		else
			isDigit = 1;
		// modify the starting bitmap
	}

	TargetPhrase targetPhrase;

	if (!(staticData.GetDropUnknown() || isEpsilon) || isDigit) {
		// add to dictionary

		Word &targetWord = targetPhrase.AddWord();
		targetWord.SetIsOOV(true);

		for (unsigned int currFactor = 0 ; currFactor < MAX_NUM_FACTORS ; currFactor++) {
			FactorType factorType = static_cast<FactorType>(currFactor);

			const Factor *sourceFactor = sourceWord[currFactor];
			if (sourceFactor == NULL)
				targetWord[factorType] = factorCollection.AddFactor(UNKNOWN_FACTOR);
			else
				targetWord[factorType] = factorCollection.AddFactor(sourceFactor->GetString());
		}
		//create a one-to-one alignment between UNKNOWN_FACTOR and its verbatim translation

		targetPhrase.SetAlignmentInfo("0-0");

	} else {
		// drop source word. create blank trans opt

		//targetPhrase.SetAlignment();

	}

	targetPhrase.GetScoreBreakdown().Assign(unknownWordPenaltyProducer, unknownScore);

	// source phrase
	const Phrase &sourcePhrase = inputPath->GetPhrase();
	m_unksrcs.push_back(&sourcePhrase);
	//WordsRange range(sourcePos, sourcePos + length - 1);

	targetPhrase.Evaluate(sourcePhrase);

	TransOptPg2s *transOpt = new TransOptPg2s(range, targetPhrase);
	transOpt->SetInputPath(*inputPath);
	Add(transOpt);

	delete p;
}

void TransOptCollPg2s::CacheLexReordering()
{
  size_t size = m_source.GetSize();

  const std::vector<const StatefulFeatureFunction*> &ffs = StatefulFeatureFunction::GetStatefulFeatureFunctions();
  std::vector<const StatefulFeatureFunction*>::const_iterator iter;
  for (iter = ffs.begin(); iter != ffs.end(); ++iter) {
    const StatefulFeatureFunction &ff = **iter;
    if (typeid(ff) == typeid(LexicalReordering)) {
      const LexicalReordering &lexreordering = static_cast<const LexicalReordering&>(ff);

      iterator iter = m_collection.begin();
      for(; iter != m_collection.end(); iter++) {
          const WordsSet& range = iter->first;
          TransOptListPg2s& transOptList = iter->second;
          TransOptListPg2s::const_iterator iterTransOpt;
          for(iterTransOpt = transOptList.begin() ; iterTransOpt != transOptList.end() ; ++iterTransOpt) {
              TransOptPg2s &transOpt = **iterTransOpt;
              const Phrase &sourcePhrase = transOpt.GetInputPath().GetPhrase();
              Scores score = lexreordering.GetProb(sourcePhrase
                                                   , transOpt.GetTargetPhrase());
              if (!score.empty())
                transOpt.CacheLexReorderingScores(lexreordering, score);
          }
      }

    } // if (typeid(ff) == typeid(LexicalReordering)) {
  } // for (iter = ffs.begin(); iter != ffs.end(); ++iter) {
}


}
} // namespace

