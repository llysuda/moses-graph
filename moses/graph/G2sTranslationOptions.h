/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once

#include "moses/TargetPhrase.h"
#include "moses/TargetPhraseCollection.h"
#include "WordsSet.h"

#include "util/check.hh"
#include <vector>
#include <boost/shared_ptr.hpp>
#include <string>

#include "G2sTranslationOption.h"

namespace Moses
{

class InputType;

namespace Graph
{

class G2sTranslationOption;

/** Similar to a DottedRule, but contains a direct reference to a list
 * of translations and provdes an estimate of the best score. For a specific range in the input sentence
 */
class G2sTranslationOptions
{
public:
  typedef std::vector<boost::shared_ptr<G2sTranslationOption> > CollType;

  /** Constructor
      \param targetPhraseColl @todo dunno
      \param stackVec @todo dunno
      \param wordsRange the range in the source sentence this translation option covers
      \param score @todo dunno
   */
  G2sTranslationOptions(const TargetPhraseCollection &targetPhraseColl,
                          const WordsSet &wordsRange,
                          float score,
                          const Phrase& sourcePhrase);
  ~G2sTranslationOptions();

  static float CalcEstimateOfBestScore(const TargetPhraseCollection &);

  //! @todo isn't the translation suppose to just contain 1 target phrase, not a whole collection of them?
  const CollType &GetTargetPhrases() const {
    return m_collection;
  }

  //! the range in the source sentence this translation option covers
  const WordsSet &GetSourceWordsSet() const {
    return *m_wordsRange;
  }

  /** return an estimate of the best score possible with this translation option.
    * the estimate is the sum of the top target phrase's estimated score plus the
    * scores of the best child hypotheses.
    */
  inline float GetEstimateOfBestScore() const {
    return m_estimateOfBestScore;
  }

  void Evaluate(const InputType &input);

  typedef CollType::const_iterator const_iterator;
	typedef CollType::iterator iterator;
	//! iterators
	const_iterator begin() const {
		return m_collection.begin();
	}
	const_iterator end() const {
		return m_collection.end();
	}

	//! iterators
    iterator begin() {
        return m_collection.begin();
    }
    iterator end() {
        return m_collection.end();
    }

    const std::string& GetLHS() const {
      return m_lhs;
    }

    const std::string& GetNextLHS() const {
      return m_nextLHS;
    }

private:

  CollType m_collection;
  const WordsSet* m_wordsRange;
  float m_estimateOfBestScore;
  Phrase m_sourcePhrase;
  std::string m_lhs;
  std::string m_nextLHS;
};

}
}
