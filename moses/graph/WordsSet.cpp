/***********************************************************************
Moses - factored phrase-based language decoder
Copyright (C) 2006 University of Edinburgh

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#include "WordsSet.h"

namespace Moses
{
namespace Graph
{

TO_STRING_BODY(WordsSet);

std::ostream& operator << (std::ostream& out, const WordsSet& range)
{
  out << "[..";
  const std::set<int>& wset = range.GetSet();
  for(std::set<int>::const_iterator iter = wset.begin();
  		iter != wset.end(); ++iter)
  	out << *iter << "..";
  out << "]";
  return out;
}

}
}

