/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include "G2sTranslationOptionList.h"

#include <algorithm>
#include <iostream>
#include <vector>
#include "moses/StaticData.h"
#include "G2sTranslationOptions.h"
#include "WordsSet.h"
#include "moses/InputType.h"
#include "InputSubgraph.h"

using namespace std;

namespace Moses
{
namespace Graph
{

G2sTranslationOptionList::G2sTranslationOptionList(size_t ruleLimit, const InputType &input)
  : m_size(0)
  , m_ruleLimit(ruleLimit)
{
  m_scoreThreshold = std::numeric_limits<float>::infinity();
}

G2sTranslationOptionList::~G2sTranslationOptionList()
{
  RemoveAllInColl(m_collection);
}

void G2sTranslationOptionList::Clear()
{
  m_size = 0;
  m_scoreThreshold = std::numeric_limits<float>::infinity();
}

class G2sTranslationOptionOrderer
{
public:
  bool operator()(const G2sTranslationOptions* itemA, const G2sTranslationOptions* itemB) const {
    return itemA->GetEstimateOfBestScore() > itemB->GetEstimateOfBestScore();
  }
};

void G2sTranslationOptionList::Add(const TargetPhraseCollection &tpc,
                                     const WordsSet &range,
                                     const Phrase& sourcePhrase)
{
  if (tpc.IsEmpty()) {
    return;
  }

  size_t isGlueRule = 0, hasGlueRule = 0;
  string nt = sourcePhrase.GetWord(sourcePhrase.GetSize()-1).GetString(0).as_string();
  size_t pos = nt.find('@');
  if (pos != NOT_FOUND && nt.substr(0,pos) == "X#X") {
    isGlueRule = 1;
    hasGlueRule = 1;
  } else if (m_size > 0 && m_collection[0]->GetLHS() == "X#X") {
    hasGlueRule = 1;
  }

  float score = G2sTranslationOptions::CalcEstimateOfBestScore(tpc);

  // If the rule limit has already been reached then don't add the option
  // unless it is better than at least one existing option.
  if (isGlueRule == 0 && m_size > m_ruleLimit && score < m_scoreThreshold) {
    return;
  }

  if (isGlueRule == 1) {
    m_collection.insert(m_collection.begin(),
                        new G2sTranslationOptions(tpc,
                            range, score, sourcePhrase));
  } else {
  // Add the option to the list.
  if (m_size >= m_collection.size()) {
    // m_collection has reached capacity: create a new object.
    m_collection.push_back(new G2sTranslationOptions(tpc,
                           range, score, sourcePhrase));
  } else {
    // Overwrite an unused object.
    *(m_collection[m_size]) = G2sTranslationOptions(tpc,
                              range, score, sourcePhrase);
  }
  }
  ++m_size;

  // If the rule limit hasn't been exceeded then update the threshold.
  if (isGlueRule == 1 || m_size <= m_ruleLimit) {
    m_scoreThreshold = (score < m_scoreThreshold) ? score : m_scoreThreshold;
  }

  // Prune if bursting
  if (m_size == m_ruleLimit * 2) {
    std::nth_element(m_collection.begin() + hasGlueRule,
                     m_collection.begin() + m_ruleLimit - 1,
                     m_collection.begin() + m_size,
                     G2sTranslationOptionOrderer());
    m_scoreThreshold = m_collection[m_ruleLimit-1]->GetEstimateOfBestScore();
    // glueRule is always at the start position
    if (m_scoreThreshold > m_collection[0]->GetEstimateOfBestScore())
      m_scoreThreshold = m_collection[0]->GetEstimateOfBestScore();
    //
    m_size = m_ruleLimit;
  }
}

void G2sTranslationOptionList::AddPhraseOOV(TargetPhrase &phrase, std::list<TargetPhraseCollection*> &waste_memory, const WordsSet &range)
{
  TargetPhraseCollection *tpc = new TargetPhraseCollection();
  tpc->Add(&phrase);
  waste_memory.push_back(tpc);
  Phrase source = phrase;
  Word labelWord(true);
  labelWord.SetFactor(0, FactorCollection::Instance().AddFactor("X"));
  source.AddWord(labelWord);
  labelWord.SetFactor(0, FactorCollection::Instance().AddFactor("X#X@-1"));
  source.AddWord(labelWord);
  Add(*tpc, range, source);
}

void G2sTranslationOptionList::ApplyThreshold()
{
  if (m_size > m_ruleLimit) {
    // Something's gone wrong if the list has grown to m_ruleLimit * 2
    // without being pruned.
    assert(m_size < m_ruleLimit * 2);
    // Reduce the list to the best m_ruleLimit options.  The remaining
    // options can be overwritten on subsequent calls to Add().
    std::nth_element(m_collection.begin()+1,
                     m_collection.begin()+m_ruleLimit,
                     m_collection.begin()+m_size,
                     G2sTranslationOptionOrderer());
    m_size = m_ruleLimit;
  }

  // keep only those over best + threshold

  float scoreThreshold = -std::numeric_limits<float>::infinity();

  CollType::const_iterator iter;
  for (iter = m_collection.begin(); iter != m_collection.begin()+m_size; ++iter) {
    const G2sTranslationOptions *transOpt = *iter;
    float score = transOpt->GetEstimateOfBestScore();
    scoreThreshold = (score > scoreThreshold) ? score : scoreThreshold;
  }

  scoreThreshold += StaticData::Instance().GetTranslationOptionThreshold();

  CollType::iterator bound = std::partition(m_collection.begin()+1,
                             m_collection.begin()+m_size,
                             ScoreThresholdPred(scoreThreshold));

  m_size = std::distance(m_collection.begin(), bound);
}

void G2sTranslationOptionList::Evaluate(const InputType &input)
{
  CollType::iterator iter;
  for (iter = m_collection.begin(); iter != m_collection.end(); ++iter) {
    G2sTranslationOptions &transOpts = **iter;
    transOpts.Evaluate(input);
  }
}

}
}
