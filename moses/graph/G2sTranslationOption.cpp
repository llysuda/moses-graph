#include "G2sTranslationOptions.h"
#include "moses/InputType.h"
#include "InputSubgraph.h"
#include "moses/Util.h"
#include "moses/LexicalReordering.h"

using namespace std;

namespace Moses
{
namespace Graph
{

G2sTranslationOption::G2sTranslationOption()
	: m_targetPhrase()
	, m_wordsRange()
	, m_futureScore(0.0f)
    , m_sourcePhrase(NULL)
{
	//
}

G2sTranslationOption::G2sTranslationOption(const TargetPhrase &targetPhrase, const WordsSet& range, const Phrase* sourcePhrase)
  :m_targetPhrase(targetPhrase)
	,m_wordsRange(range)
  ,m_scoreBreakdown(targetPhrase.GetScoreBreakdown())
	,m_futureScore(targetPhrase.GetFutureScore())
    , m_sourcePhrase(sourcePhrase)
{
}

void G2sTranslationOption::Evaluate(const InputType &input)
{
  const std::vector<FeatureFunction*> &ffs = FeatureFunction::GetFeatureFunctions();

  for (size_t i = 0; i < ffs.size(); ++i) {
    const FeatureFunction &ff = *ffs[i];
    ff.Evaluate(input, m_wordsRange.GetSet(), m_scoreBreakdown);
  }
}

void G2sTranslationOption::CacheLexReorderingScores(const LexicalReordering &producer, const Scores &score)
{
  m_lexReorderingScores[&producer] = score;
}

}
}

