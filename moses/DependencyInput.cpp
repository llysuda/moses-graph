// $Id$

#include "DependencyInput.h"
#include "StaticData.h"
#include "Util.h"
#include "XmlOption.h"
#include <map>
#include <set>

using namespace std;

namespace Moses
{

void DependencyInput::InitFid() {
	m_fid.push_back(0);
	m_pos.push_back("");
	m_word.push_back("<s>");
	for(size_t i = 1; i < GetSize()-1; i++) {
		const Word& word = GetWord(i);
		const Factor* factor = word[2];
		CHECK(factor);
		int fid = Scan<int>(factor->GetString().as_string());
		if (fid < 0) {
			fid = 9999;
		} else {
			fid++;
		}
		m_fid.push_back(fid);
		m_pos.push_back(word.GetString(1).as_string());
		m_word.push_back(word.GetString(0).as_string());
		//m_words[i].SetFactor(1, FactorCollection::Instance().AddFactor("-1"));
	}
	m_fid.push_back(0);
	m_pos.push_back("");
	m_word.push_back("</s>");
}

void DependencyInput::ResetWords() {
	for(size_t i = 0; i < GetSize(); i++) {
		for (size_t j = 1; j <=3; j++)
			Phrase::SetFactor(i,j,NULL);
	}
}



map<size_t, size_t> DependencyInput::GetExternalNodes(size_t start, size_t end) const {
	// connected
	// max 2 external node = max 1 unfull subtree
	map<size_t, size_t> nodes;
	map<size_t, size_t> external_nodes;

	for(size_t i = start; i <= end; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];

		if (eid >= GetSize()-1 || eid < 1) {
			external_nodes[eid] = 1;
		}
		nodes[sid] = 1;
		nodes[eid] = 1;
	}

	for(size_t i = 1; i < start; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	for(size_t i = end+1; i < GetSize()-1; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	return external_nodes;
}

bool DependencyInput::valid(size_t start, size_t end, size_t startHole, size_t endHole) const {
	// connected
	// max 2 external node = max 1 unfull subtree
	map<size_t, size_t> nodes;
	map<size_t, size_t> external_nodes;

	for(size_t i = startHole; i <= endHole; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];

		if (eid > endHole || eid < startHole) {
			external_nodes[eid] = 1;
		}
		nodes[sid] = 1;
		nodes[eid] = 1;
	}
	if (nodes.size() > 1 + endHole-startHole+1)
		return false;
	//

	for(size_t i = start; i < startHole; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	for(size_t i = endHole+1; i <= end; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	if (external_nodes.size() > 2)
		return false;
	return true;
}

std::string DependencyInput::GetDepString(const WordsRange &range, const std::vector<std::pair<size_t, size_t> > &holes) const {
  std::vector<std::pair<const WordsRange*, const Word*> > hholes;
  for (size_t i = 0; i < holes.size(); i++) {
    hholes.push_back(make_pair(new WordsRange(holes[i].first, holes[i].second), new Word()));
  }
  return GetDepString(range, hholes);
}

std::string DependencyInput::GetDepString(const WordsRange &range, const std::vector<std::pair<const WordsRange*, const Word*> > &holes) const
{
	const StaticData& staticData = StaticData::Instance();

//	if (staticData.GetNoDep())
//		return "";

//	bool relaxdep = staticData.GetRelaxDep();
//	bool ignorePhraseDep = staticData.GetIgnorePhraseDep();
//	bool useConnection = staticData.GetUseConnection();
//	bool contextAwareLabel = staticData.GetContextAwareLabel();

	if (range.GetStartPos() < 1 || range.GetEndPos() >= GetSize()-1)
		return "";

	if (!HasMoreNT(range.GetStartPos(), range.GetEndPos())) {
		return "INVALID";
	}


	if (range.GetNumWordsCovered() == 1) {
	  return "-1";
	}
	// words sequence
	if (holes.size() == 0) {
		string ret = "";
		for (size_t i = range.GetStartPos(); i <= range.GetEndPos(); i++) {
			size_t fid = m_fid[i];

            if (fid < range.GetStartPos() || fid > range.GetEndPos())
                ret += "-1";
            else
                ret += SPrint<size_t>(fid-range.GetStartPos());
            ret += ":";

		}
		return ret.erase(ret.size()-1);
	}

	// sequence with holes
	std::vector<std::pair<const WordsRange*, const Word*> >::const_iterator iterHoleList = holes.begin();
	assert(iterHoleList != holes.end());
	size_t startS = range.GetStartPos();
	size_t endS = range.GetEndPos();
	//
	map<size_t,size_t> newPos;
	int wordCount = 0;
	for(size_t currPos = startS; currPos <= endS; currPos++) {
		bool isHole = false;
		if (iterHoleList != holes.end()) {
			const WordsRange &hole = *(iterHoleList->first);
			isHole = hole.GetStartPos() == currPos;
		}

		if (isHole) {
			const WordsRange &hole = *(iterHoleList->first);

			for(size_t i = currPos; i <= hole.GetEndPos(); i++)
				newPos[i] = wordCount;
			currPos = hole.GetEndPos();
			++iterHoleList;
		} else {
			newPos[currPos] = wordCount;
		}
		wordCount++;
	}
	CHECK(iterHoleList == holes.end());

	iterHoleList = holes.begin();
	string out = "";
	wordCount = 0;
	for(size_t currPos = startS; currPos <= endS; currPos++) {
		bool isHole = false;
		if (iterHoleList != holes.end()) {
			const WordsRange &hole = *(iterHoleList->first);
			isHole = hole.GetStartPos() == currPos;
		}

		int fid = -1;
		if (isHole) {
			const WordsRange &hole = *(iterHoleList->first);
			fid = GetSpanFid(currPos, hole.GetEndPos());
			if (fid < 0) {
				cerr << "no fid specified" << endl;
				assert(false);
			}
			if (fid < startS || fid > endS)
				fid = -1;
			else
				fid = newPos[fid];

			currPos = hole.GetEndPos();
			++iterHoleList;
		} else {
			fid = m_fid[currPos];
			if (fid < startS || fid > endS)
				fid = -1;
			else
				fid = newPos[fid];
		}
		out += SPrint<int>(fid) + ":";
		wordCount++;
	}
	CHECK(out != "");
	return out.erase(out.size()-1);
}

bool DependencyInput::valid(size_t start, size_t end) const {
	if (start == 0 || start == GetSize()-1 || end == 0 || end == GetSize()-1)
		return false;
	// connected
	// max 2 external node = max 1 unfull subtree
	map<size_t, size_t> nodes;
	map<size_t, size_t> external_nodes;

	for(size_t i = start; i <= end; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];

		if (eid < 1 || eid >= GetSize()-1) {
			external_nodes[eid] = 1;
		}
		nodes[sid] = 1;
		nodes[eid] = 1;
	}
	if (nodes.size() > 1 + end-start+1)
		return false;
	//

	for(size_t i = 1; i < start; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	for(size_t i = end+1; i < GetSize()-1; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	if (external_nodes.size() > 2)
		return false;
	return true;
}


bool DependencyInput::IsConnected(size_t start, size_t end) const {
	map<size_t, size_t> nodes;

	for(size_t i = start; i <= end; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		nodes[sid] = 1;
		nodes[eid] = 1;
	}
	if (nodes.size() > 1 + end-start+1)
		return false;
	return true;
}

void DependencyInput::ExpandNonTerm() {
	size_t length = GetSize();
	m_sourceChart.resize(length);
	for (size_t pos = 0; pos < length; ++pos) {
		m_sourceChart[pos].resize(length - pos);
	}

	for (size_t start=1; start < length-1; start++){
		for (size_t end = start; end < length-1; end++) {
			if (valid(start, end)){

					vector<string> labels = GetSpanLabel(start,end);

					for (size_t i = 0; i < labels.size(); i++) {
						string label = labels[i];
						TreeInput::AddChartLabel(start, end, label,StaticData::Instance().GetInputFactorOrder());
					}
			}
		}
	}
	for (size_t start=0; start <= length-1; start++){
		for (size_t end = start; end <= length-1; end++) {
			TreeInput::AddChartLabel(start, end, StaticData::Instance().GetInputDefaultNonTerminal()
																,StaticData::Instance().GetInputFactorOrder());
		}
	}

}


int DependencyInput::GetSpanFid(size_t start, size_t end) const {
	set<size_t> fids;
	for(size_t i = start; i <= end; i++) {
		size_t fid = m_fid[i];
		if(fid < start || fid > end) {
			fids.insert(fid);
		}
	}
	if (fids.size()>1)
	  return -1;
	CHECK(fids.size() == 1);
	return *(fids.begin());
}

std::vector<size_t> DependencyInput::GetHeads(size_t startPos, size_t endPos) const
{
	vector<size_t> heads;
	for(size_t i = startPos; i <= endPos; i++) {
		size_t fid = m_fid[i];
		if(fid < startPos || fid > endPos) {
			heads.push_back(i);
		}
	}
	CHECK(heads.size() >= 1);
	return heads;
}

WordsRange DependencyInput::GetSourceSpan(size_t head) const
{
	size_t minPos = head, maxPos = head;
	vector<size_t> kids = GetKids(minPos, maxPos);
	for (int i = 0; i < kids.size(); i++) {
		WordsRange span = GetSourceSpan(kids[i]);
		if (span.GetStartPos() < minPos)
			minPos = span.GetStartPos();
		if (span.GetEndPos() > maxPos)
			maxPos = span.GetEndPos();
	}
	return WordsRange(minPos, maxPos);
}


vector<string> DependencyInput::GetSpanLabel(size_t start, size_t end) const {

	if (StaticData::Instance().GetSingleNT()) {
		return vector<string>(1,"SNT");
	}

	vector<string> labels;
	//vector<string> wlabels;
	for(size_t i = start; i <= end; i++) {
		size_t fid = m_fid[i];
		if(fid < start || fid > end) {
			labels.push_back(m_pos[i]);
			//wlabels.push_back(GetWord(i).GetString(0).as_string());
		}
	}
	string ret = labels[0];

    for(size_t i = 1; i < labels.size(); i++)
        ret += "_" + labels[i];

	vector<string> retVec;
	retVec.push_back(ret);
	return retVec;
}



std::map<size_t,size_t> DependencyInput::GetExternalNodes(size_t start, size_t end, size_t startHole, size_t endHole) const
{
	// connected
	// max 2 external node = max 1 unfull subtree
	map<size_t, size_t> nodes;
	map<size_t, size_t> external_nodes;

	for(size_t i = startHole; i <= endHole; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];

		if (eid > end || eid < start) {
			external_nodes[eid] = 1;
		}
		nodes[sid] = 1;
		nodes[eid] = 1;
	}

	for(size_t i = start; i < startHole; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	for(size_t i = endHole+1; i <= end; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	return external_nodes;
}


std::vector<std::string> DependencyInput::GetLabels(size_t start, size_t end, size_t startHole, size_t endHole) const
{
		vector<string> ret;
		map<size_t,size_t> externalNodes = GetExternalNodes(start,end, startHole, endHole);
		if (externalNodes.size() == 1) {
			vector<string> labels;
			for(size_t i = startHole; i <= endHole; i++) {
				size_t fid = m_fid[i];
				if(fid < startHole || fid > endHole) {
					labels.push_back(m_pos[i]);
				}
			}
			assert(labels.size() > 0);
			//string label = labels[0];
			map<string,int> used;
			for(size_t i = 0; i < labels.size(); i++) {
				if (used.find(labels[i]) == used.end())
				//label += "_" + labels[i];
					ret.push_back(labels[i]);
				used[labels[i]] = 1;
			}
		} else if (externalNodes.size() == 2) {

			int fid = GetSpanFid(startHole, endHole);

			if (fid < 0) {
				cerr << "span fid error " << start << " " << end << endl;
				assert(false);
			}

			map<size_t,size_t>::iterator iter = externalNodes.begin();
			size_t s = iter->first;
			iter++;
			size_t e = iter->first;

			if (fid != s && fid != e) {
				cerr << "external nodes error " << start << " " << end << endl;
				assert(false);
			}

			if (fid == s) {
				s = e;
				e = fid;
			}

			vector<string> labels;
			size_t step = s;
			while (m_fid[step] != e) {
					labels.push_back(m_pos[step]);
					step = m_fid[step];
			}
			labels.push_back(m_pos[step]);

			string label = labels[0];
			if (labels.size() > 1)
				label += "_" + labels[labels.size()-1];

			ret.push_back(label);
		}
		return ret;
}


std::vector<size_t> DependencyInput::GetKids(size_t start, size_t end) const
{
	vector<size_t> ret;
	for (size_t i = 1; i < start; i++) {
		size_t fid = m_fid[i];
		if (fid >= start && fid <= end)
			ret.push_back(i);
	}
	for (size_t i = end+1; i < GetSize()-1; i++) {
		size_t fid = m_fid[i];
		if (fid >= start && fid <= end)
			ret.push_back(i);
	}
	return ret;
}

std::vector<size_t> DependencyInput::GetSibs(size_t start, size_t end) const
{
	size_t father = GetSpanFid(start,end);

	vector<size_t> ret;
	for (size_t i = 1; i < start; i++) {
		size_t fid = m_fid[i];
		if (fid == father)
			ret.push_back(i);
	}
	for (size_t i = end+1; i < GetSize()-1; i++) {
		size_t fid = m_fid[i];
		if (fid == father)
			ret.push_back(i);
	}
	return ret;
}

} // namespace

