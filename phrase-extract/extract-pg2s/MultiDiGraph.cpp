/*
 * DepNgramGraph.cpp
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

//#include "tables-core.h"
//#include "moses/Util.h"
#include <set>
#include <algorithm>
#include <iostream>
#include "MultiDiGraph.h"

using namespace std;
using namespace Moses;

namespace Moses {
namespace PG2S {

void MultiDiGraph::add_node(int id, const struct Node* attr) {
	// no duplicate node
	nodes[id] = attr;
}

void MultiDiGraph::add_edge(int u, int v, const std::string& label) {
	// add to succ
	if (succ.find(u) == succ.end()) {
		succ[u] = map<int, set<string> >();
	}
	if (succ[u].find(v) == succ[u].end()) {
		succ[u][v] = set<string>();
	}
	succ[u][v].insert(label);
	// add to pred
	if (pred.find(v) == succ.end()) {
		pred[v] = map<int, set<string> >();
	}
	if (pred[v].find(u) == pred[v].end()) {
		pred[v][u] = set<string>();
	}
	pred[v][u].insert(label);
}

void MultiDiGraph::DepthFirst(int curr_nid,
		set<int>& old_nids,
		const set<int>& all_nids) const {

	set<int> ajacent;
	map<int, set<string> >::const_iterator iter;
	map<int, map<int, set<string> > >::const_iterator iter2;
	iter2 = succ.find(curr_nid);
	if (iter2 != succ.end()) {
		for(iter=iter2->second.begin(); iter!=iter2->second.end(); iter++) {
			int sid = iter->first;
			if (all_nids.find(sid) != all_nids.end()) {
				ajacent.insert(sid);
			}
		}
	}
	iter2 = pred.find(curr_nid);
	if (iter2 != pred.end()) {
		for(iter=iter2->second.begin(); iter!=iter2->second.end(); iter++) {
			int pid = iter->first;
			if (all_nids.find(pid) != all_nids.end()) {
				ajacent.insert(pid);
			}
		}
	}

	for(set<int>::const_iterator it=ajacent.begin(); it!=ajacent.end(); it++) {
		if (old_nids.find(*it) == old_nids.end()) {
			old_nids.insert(*it);
			DepthFirst(*it, old_nids, all_nids);
		}
	}
}

bool MultiDiGraph::is_connected(const std::set<int>& nids) const {
	set<int> old_nids;
	int curr_nid = *(nids.begin());
	old_nids.insert(curr_nid);
	DepthFirst(curr_nid, old_nids, nids);
	return old_nids.size() == nids.size();
}

const MultiDiGraph* MultiDiGraph::get_subgraph(const std::set<int>& nids) const {
	MultiDiGraph* mdg = new MultiDiGraph();

	map<int, set<string> >::const_iterator iter;
	map<int, map<int, set<string> > >::const_iterator iter2;
	std::set<int>::const_iterator iter3;

	for(iter3=nids.begin(); iter3!=nids.end(); iter3++) {
		int nid = *iter3;
		// add node
		mdg->add_node(nid, nodes.find(nid)->second);
		// add edges
		iter2 = succ.find(nid);
		if (iter2 != succ.end()) {
			for(iter=iter2->second.begin(); iter!=iter2->second.end(); iter++) {
				int sid = iter->first;
				if (nids.find(sid) != nids.end()) {
					// add a succ edge
					if (mdg->succ.find(nid) == mdg->succ.end())
						mdg->succ[nid] = map<int, set<string> >();
					mdg->succ[nid][sid] = iter->second;
				}
			}
		}
		iter2 = pred.find(nid);
		if (iter2 != pred.end()) {
			for(iter=iter2->second.begin(); iter!=iter2->second.end(); iter++) {
				int pid = iter->first;
				if (nids.find(pid) != nids.end()) {
					// add a succ edge
					if (mdg->pred.find(pid) == mdg->pred.end())
						mdg->pred[nid] = map<int, set<string> >();
					mdg->pred[nid][pid] = iter->second;
				}
			}
		}
	}
	return mdg;
}

std::set<int> MultiDiGraph::get_neighbors(const std::set<int>& nids) const {
	set<int> neighbors;
	map<int, set<string> >::const_iterator iter;
	map<int, map<int, set<string> > >::const_iterator iter2;
	for(set<int>::const_iterator it = nids.begin();
			it != nids.end(); it++) {
		int nid = *it;
		iter2 = succ.find(nid);
		if (iter2 != succ.end()) {
			for(iter=iter2->second.begin(); iter!=iter2->second.end(); iter++) {
				int sid = iter->first;
				if (nids.find(sid) == nids.end())
					neighbors.insert(sid);
			}
		}
		iter2 = pred.find(nid);
		if (iter2 != pred.end()) {
			for(iter=iter2->second.begin(); iter!=iter2->second.end(); iter++) {
				int pid = iter->first;
				if (nids.find(pid) == nids.end())
					neighbors.insert(pid);
			}
		}
	}
	return neighbors;
}

void MultiDiGraph::print_graph() const {
	cerr << "----------------------" << endl;
	cerr << "nodes:" << endl;
	cerr << "size: " << nodes.size() << endl;

	for(map<int, const struct Node*>::const_iterator iter = nodes.begin();
			iter != nodes.end(); iter++) {
		const struct Node* n = iter->second;
		cerr << " [" << n->id << " " << n->word << "]" << endl;
	}

	cerr << "edges:" << endl;

	map<int, set<string> >::const_iterator iter;
	map<int, map<int, set<string> > >::const_iterator iter2;
	for (iter2 = succ.begin(); iter2 != succ.end(); iter2++) {
		for(iter = iter2->second.begin(); iter != iter2->second.end(); iter++) {
			for(set<string>::const_iterator it = iter->second.begin();
					it != iter->second.end(); it++)
				cerr << " " << "[" << iter2->first << " " << iter->first
					 << " " << *it << "]" << endl;
		}
	}

	cerr << "----------------------" << endl;
}

} /* namespace PG2S */
} /* namespace Moses */
