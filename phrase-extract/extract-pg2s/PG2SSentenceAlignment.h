/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2010 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once
#ifndef PG2S_SENTENCE_ALIGNMENT_H_INCLUDED_
#define PG2S_SENTENCE_ALIGNMENT_H_INCLUDED_

#include <string>
#include <vector>
#include <set>
#include <map>

#include "MultiDiGraph.h"
#include "PG2SExtractionOptions.h"

namespace Moses
{
namespace PG2S
{

class PG2SSentenceAlignment
{
public:
  std::vector<std::string> target;

  std::vector<int> targetFid;
  std::vector<std::string> targetPos;
  std::vector<std::string> targetLabel;

  const MultiDiGraph* source;
  std::vector<int> alignedCountS;
  std::vector<std::vector<int> > alignedToT;
  int sentenceID;
  std::string weightString;

  virtual ~PG2SSentenceAlignment();

  virtual bool processTargetSentence(const char *, int, bool boundaryRules);

  virtual bool processSourceSentence(const char *, int, const PG2SExtractionOptions&);

  bool create(char targetString[], char sourceString[],
              char alignmentString[], char weightString[],
							int sentenceID, const PG2SExtractionOptions&);

  std::set<int> get_unalignedF(const std::set<int>& nids);
  void get_sorted_words(const std::set<int>& nids, std::vector<std::pair<std::string, int> >&);
  std::vector<std::string> get_structure(const std::vector<int>& nids, bool incLabel) const;
  std::string get_structure_string(const std::vector<int>&, bool incLabel) const;
  std::string get_target_label(int start, int end) const;
  bool isSyntaxTarget(int start, int end) const;

};

}
}


#endif
