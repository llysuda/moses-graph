/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2010 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include "PG2SSentenceAlignment.h"

#include <map>
#include <set>
#include <string>
#include <algorithm>

#include "tables-core.h"
#include "moses/Util.h"

using namespace std;

namespace Moses
{
namespace PG2S
{

vector<string> tokenize( const char* input, const char separator=' ' )
{
  vector< string > token;
  bool betweenWords = true;
  int start=0;
  int i=0;
  for(; input[i] != '\0'; i++) {
    bool isSpace = (input[i] == separator);
    if (!isSpace && betweenWords) {
      start = i;
      betweenWords = false;
    } else if (isSpace && !betweenWords) {
      token.push_back( string( input+start, i-start ) );
      betweenWords = true;
    }
  }
  if (!betweenWords)
    token.push_back( string( input+start, i-start ) );
  return token;
}

const MultiDiGraph* CreateGraphFromLine(const char* line, const PG2SExtractionOptions& options) {
	MultiDiGraph* mdg = new MultiDiGraph();

//	bool linkType = options.isUseLinkType();
	bool noBigram = options.isNoBigram();
//	bool sibling = options.sibling;

	vector<string> tokens = tokenize(line);
	vector< vector<int> > children;
	children.resize(tokens.size());

	for(size_t i = 0; i < tokens.size(); i++) {
	  vector<string> nodeAttrib = tokenize(tokens[i].c_str(),'|');
	  if (nodeAttrib.size() < 3) {
		cerr << "error source format !" << endl;
		exit(1);
	  }

	  int fid = atoi(nodeAttrib[2].c_str());

	  // add node
	  struct Node* n = new Node();
	  n->id = i;
	  n->word = nodeAttrib[0];
	  n->pos = nodeAttrib[1];
	  n->fid = fid;
	  mdg->add_node(i, n);

	  if (fid >= 0)
	    children[fid].push_back(i);

	  // add dep edge
//	  string label = nodeAttrib[3];
//	  if (linkType)
	  string label = "DP";
	  if (fid >= 0)
		  mdg->add_edge(fid, i, label);
	  // add bigram edge
	  if (!noBigram && i > 0) {
	    mdg->add_edge(i, i-1, "BG");
	  }
	}
	return mdg;
}

PG2SSentenceAlignment::~PG2SSentenceAlignment() {}

void addBoundaryWords(vector<string> &phrase)
{
  phrase.insert(phrase.begin(), "<s>");
  phrase.push_back("</s>");
}

bool PG2SSentenceAlignment::processTargetSentence(const char * targetString, int, bool boundaryRules)
{
  vector<string> words = tokenize(targetString);

  for(vector<string>::iterator iter = words.begin(); iter != words.end(); iter++) {
  	string word = *iter;
  	vector<string> factors = tokenize(word.c_str(),'|');

  	target.push_back(factors[0]);

  	if (factors.size() > 1) {
  		// dep format
  		if (factors.size() < 3) {
			cerr << "error target format !" << endl;
			exit(1);
			}
  		targetPos.push_back(factors[1]);
  		targetLabel.push_back(factors[3]);
  		targetFid.push_back(atoi(factors[2].c_str()));
  	}
  }

  if (boundaryRules)
    addBoundaryWords(target);
  return true;
}

bool PG2SSentenceAlignment::processSourceSentence(const char * sourceString, int, const PG2SExtractionOptions& options)
{
  //source = tokenize(sourceString);
  //if (boundaryRules)
  //  addBoundaryWords(source);
  source = CreateGraphFromLine(sourceString, options);
  return true;
}

bool PG2SSentenceAlignment::create( char targetString[], char sourceString[], char alignmentString[], char weightString[],
		int sentenceID, const PG2SExtractionOptions& options)
{
	bool boundaryRules = false;

  using namespace std;
  this->sentenceID = sentenceID;
  this->weightString = std::string(weightString);

  // process sentence strings and store in target and source members.
  if (!processTargetSentence(targetString, sentenceID, false)) {
    return false;
  }
  if (!processSourceSentence(sourceString, sentenceID, options)) {
    return false;
  }

  // check if sentences are empty
  if (target.size() == 0 || source->size() == 0) {
    cerr << "no target (" << target.size() << ") or source (" << source->size() << ") words << end insentence " << sentenceID << endl;
    cerr << "T: " << targetString << endl << "S: " << sourceString << endl;
    return false;
  }

  // prepare data structures for alignments
  for(size_t i=0; i<source->size(); i++) {
    alignedCountS.push_back( 0 );
  }
  for(size_t i=0; i<target.size(); i++) {
    vector< int > dummy;
    alignedToT.push_back( dummy );
  }

  // reading in alignments
  vector<string> alignmentSequence = tokenize( alignmentString );
  for(size_t i=0; i<alignmentSequence.size(); i++) {
    int s,t;
    // cout << "scaning " << alignmentSequence[i].c_str() << endl;
    if (! sscanf(alignmentSequence[i].c_str(), "%d-%d", &s, &t)) {
      cerr << "WARNING: " << alignmentSequence[i] << " is a bad alignment point in sentence " << sentenceID << endl;
      cerr << "T: " << targetString << endl << "S: " << sourceString << endl;
      return false;
    }

    if (boundaryRules) {
      ++s;
      ++t;
    }

    // cout << "alignmentSequence[i] " << alignmentSequence[i] << " is " << s << ", " << t << endl;
    if ((size_t)t >= target.size() || (size_t)s >= source->size()) {
      cerr << "WARNING: sentence " << sentenceID << " has alignment point (" << s << ", " << t << ") out of bounds (" << source->size() << ", " << target.size() << ")\n";
      cerr << "T: " << targetString << endl << "S: " << sourceString << endl;
      return false;
    }
    alignedToT[t].push_back( s );
    alignedCountS[s]++;
  }

  if (boundaryRules) {
    alignedToT[0].push_back(0);
    alignedCountS[0]++;

    alignedToT.back().push_back(alignedCountS.size() - 1);
    alignedCountS.back()++;

  }

  return true;
}

std::set<int> PG2SSentenceAlignment::get_unalignedF(const std::set<int>& nids) {
	set<int> unalignedF;
		int prev = -1;
		for(set<int>::const_iterator iter = nids.begin();
				iter != nids.end(); ++iter) {
			int id = *iter;
			if (prev >= 0) {
				if (id != prev+1) {
					if (alignedCountS[prev+1] == 0)
						unalignedF.insert(prev+1);
					if (alignedCountS[id-1] == 0)
						unalignedF.insert(id-1);
				}
			}
			prev = id;
		}
		// boundary
		int start = *(nids.begin())-1;
		if (start >= 0 && alignedCountS[start] == 0)
			unalignedF.insert(start);
		int end = prev+1;
		if (end < source->size() && alignedCountS[end] == 0)
					unalignedF.insert(end);
	return unalignedF;
}

struct less_than_key
{
    inline bool operator() (const pair<string,int>& struct1,
    												const pair<string,int>& struct2) const
    {
        return (struct1.first < struct2.first);
    }
} node_key;

void PG2SSentenceAlignment::get_sorted_words(const set<int>& nids,
				vector<pair<string, int> >& words){

	for(set<int>::const_iterator iter = nids.begin(); iter != nids.end(); iter++) {
		int nid = *iter;
		string word = source->get_word(nid);
		words.push_back(make_pair<string,int>(word,nid));
	}
}

vector<string> PG2SSentenceAlignment::get_structure(const vector<int>& nids, bool incLabel) const {
	vector<string> ret;
	int size = (int)nids.size();
	for(int i = 0; i < size; i++) {
		int nid = nids[i];
		bool success = false;
		string retStr = "";
		if (source->has_succ(nid)) {
			const map<int, set<string> >& succ_edges = source->get_succ_edges(nid);
			for(int j = 0; j < size; j++) {
				if (i == j)
					continue;
				int sid = nids[j];
				map<int, set<string> >::const_iterator it = succ_edges.find(sid);
				if ( it != succ_edges.end() ) {
					success = true;
					const set<string>& labels = it->second;
					for(set<string>::const_iterator it2 = labels.begin();
							it2 != labels.end(); it2++) {
						string label = *it2;
						retStr += SPrint<int>(j) + ":";
						if (incLabel)
							retStr += label + ":";
						else
							break;
					}
				}
			}
		}
		if (!success){
			ret.push_back("-1");
		} else {
			retStr = retStr.erase(retStr.size()-1);
			ret.push_back(retStr);
		}
	}
	return ret;
}

string PG2SSentenceAlignment::get_structure_string(const vector<int>& nids, bool incLabel) const {
	string retStr = "X";
	vector<string> strs = get_structure(nids, incLabel);
	for(size_t i = 0; i < strs.size(); i++)
		retStr += "_" + strs[i];
	return retStr;
}

std::string PG2SSentenceAlignment::get_target_label(int start, int end) const
{
	string ret = "X";

	for(int i = start; i <= end; i++) {
		int fid = targetFid[i];
		if (fid < start || fid > end) {
			fid = -1;
		} else {
			fid -= start;
		}
		ret += "_" + SPrint<int>(fid);
	}

	return ret;
}

bool PG2SSentenceAlignment::isSyntaxTarget(int start, int end) const
{
	int fidCount = 0;
	for(int i = start; i <= end; i++) {
		int fid = targetFid[i];
		if (fid < start || fid > end) {
			fidCount++;
			if (fidCount > 1)
				return false;
		}
	}
	return true;
}

}
}

