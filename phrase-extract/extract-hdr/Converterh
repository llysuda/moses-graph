/*
 * Converter.h
 *
 *  Created on: Dec 12, 2013
 *      Author: lly
 */

#ifndef CONVERTER_H
#define CONVERTER_H

#include <string>
#include <vector>
#include "DependencyTree.h"

namespace Moses {
namespace HDR {

class ConsNode {
public:
  std::vector<std::string> labels;
  ConsNode* father;
  std::vector<ConsNode*> kids;
public:
  ConsNode(): father(NULL){}

  ConsNode(const ConsNode* node) {
    labels = node->labels;
    father = node->father;
    kids = node->kids;
  }

  ConsNode(std::string label, ConsNode* father = NULL) {
      this->labels.push_back(label);
      this->father = father;
  }

  bool IsLeaf() const {
      return kids.size() == 0;
  }
};

class Converter {
private:
  int m_sentID;
  DependencyTree source;
  ConsNode* tree;

public:
  Converter(int sentID, const std::string & line);
  void Process ();
  ConsNode* ProcessSubtree(int hid, int& depth);
  ConsNode* ProcessInternal(int id, int position, int& depth);
  ConsNode* ProcessLeaf(int id,int position);
  ConsNode* ProcessHead(int hid);
  std::string SPrintTree(const ConsNode* head) const;
  std::string SPrintTree() const;
  virtual ~Converter();
  //static bool useRelation;
};

} /* namespace HDR */
} /* namespace Moses */
#endif /* CONVERTER_H_ */
