/*
 * Converter.cpp
 *
 *  Created on: Dec 12, 2013
 *      Author: lly
 */

#include "Converter.h"
#include "moses/Util.h"
#include "../InputFileStream.h"
#include "../OutputFileStream.h"
#include <iostream>
#include "SafeGetline.h"
#include "DependencyTree.h"

using namespace std;
using namespace Moses::HDR;

namespace Moses {
namespace HDR {

//bool Converter::useRelation = false;
bool Converter::useLabel = false;

Converter::Converter(int sentID, const string & line) : m_sentID (sentID){
  m_source.Init(line);

  for (int i = 0; i < m_source.Size(); i++) {
    vector<vector<string> > dummy;
    for (int j = 0; j < m_source.Size(); j++) {
      vector<string> dummy2;
      dummy.push_back(dummy2);
    }
    m_sourceLabels.push_back(dummy);
  }
}

Converter::~Converter() {
  // TODO Auto-generated destructor stub
}

void Converter::Process () {
    vector<int> rootid = m_source.GetRootId();
    //ConstTree ctree =
    int depth = 0;
    for (size_t i = 0; i < rootid.size(); i++)
      ProcessSubtree(rootid[i],depth);
}

void Converter::ProcessSubtree(int hid, int& depth) {
  //Span span = m_source.GetSourceSpan(hid);

  if (m_source.GetNode(hid).IsLeaf()) {
    if (m_source.GetNode(hid).GetFatherId() < 0) {
            ProcessHead(hid);
     } else {
      ProcessLeaf(hid,0);
     }
      return;
  }

  vector<int> ids = m_source.GetSubtreeIndexes(hid);

  int hindex = -1;
  for (size_t i = 0; i < ids.size(); i++) {
    if (m_source.GetNode(ids[i]).GetId() == hid) {
        hindex = i;
        break;
    }
  }

  //string gluelabel = "G"+SPrint<int>(depth);
  for (size_t i = 0; i < ids.size(); i++) {
    Node n = m_source.GetNode(ids[i]);
    if (n.GetId() == hid) {
        ProcessHead(hid);
       // m_sourceLabels[hid][hid].push_back(gluelabel);
    } else if (n.IsLeaf()) {
        ProcessLeaf(n.GetId(),i-hindex);
       // m_sourceLabels[n.GetId()][n.GetId()].push_back(gluelabel);
    } else {
        depth++;
        ProcessInternal(n.GetId(),i-hindex, depth);

        //Span span2 = m_source.GetSourceSpan(n.GetId());
        //m_sourceLabels[span2.first][span2.second].push_back(gluelabel);
    }
  }


  //for (int i = span.first+1; i <=span.second; i++) {
  //  m_sourceLabels[span.first][i].push_back(gluelabel);
  //}

  //if (hid == m_source.GetRootId()) {
  //  string label = "TOP";
  //  m_sourceLabels[span.first][span.second].push_back(label);
  //}
}

void Converter::ProcessInternal(int id, int position, int& depth) {

  Span span = m_source.GetSourceSpan(id);

  string llabel = m_source.GetNode(id).GetWord()+":"+SPrint<int>(position);
  string hlabel = m_source.GetNode(id).GetPos()+":"+SPrint<int>(position);
 // string llabel = m_source.GetNode(id).GetWord()+":"+m_source.GetNode(id).GetRelation();
  //  string hlabel = m_source.GetNode(id).GetPos()+":"+m_source.GetNode(id).GetRelation();

  if (useLabel) {
    llabel = m_source.GetNode(id).GetWord()+":I"+SPrint<int>(position);
    hlabel = m_source.GetNode(id).GetPos()+":I"+SPrint<int>(position);
  }
    //if (useRelation) {
    //  hlabel = "I"+SPrint<int>(flag*i)+m_source.GetNode(id).GetRelation();
    //}

    m_sourceLabels[span.first][span.second].push_back(llabel);
    m_sourceLabels[span.first][span.second].push_back(hlabel);
  ProcessSubtree(id, depth);
}

void Converter::ProcessLeaf(int id,int position) {
  Node n = m_source.GetNode(id);
  if (n.IsOpenClass()) {
      string label = n.GetPos()+":"+SPrint<int>(position);
      if (useLabel) {
        label = n.GetPos()+":L"+SPrint<int>(position);
      }
      //label = n.GetPos();//":"+n.GetRelation();
      m_sourceLabels[id][id].push_back(label);
  }
}

void Converter::ProcessHead(int hid) {
  string label = m_source.GetNode(hid).GetPos()+":0";
  if (useLabel) {
    label = m_source.GetNode(hid).GetPos()+":H0";
  }
  m_sourceLabels[hid][hid].push_back(label);

  vector<int> idx = m_source.GetLeftChildrenIndexes(hid);
  Span span = m_source.GetSourceSpan(hid);
  int position = idx.size();
  label = "H" + SPrint<int>(position);
  m_sourceLabels[span.first][span.second].push_back(label);
}

string Converter::SPrintForest() const {
  string out = "<tree label=\"TOP\">";
  for (int i = 0; i < m_source.Size(); i++) {
    out += " " + m_source.GetWord(i);
  }
  for (size_t i = 0; i < m_sourceLabels.size(); i++) {
    for (size_t j = 0; j < m_sourceLabels[i].size(); j++) {
      if (m_sourceLabels[i][j].size() > 0) {
        string spanStr = SPrint<int>(i)+"-"+SPrint<int>(j);
        for (size_t k = 0 ; k < m_sourceLabels[i][j].size(); k++) {
          out += " <tree label=\""+m_sourceLabels[i][j][k]+"\" span=\""+spanStr+"\"> </tree>";
        }
      }
    }
  }
  out += " </tree>";
  return out;
}

} /* namespace HDR */
} /* namespace Moses */


int main(int argc, char* argv[])
{
  cerr  << "converter-hdr v1.0, written by Liangyou Li\n"
            << "convert from dep line to cons tree with XML\n";

  if (argc < 3) {
    cerr << "syntax: converter-hdr input output";
    exit(1);
  }

  for(int i=3; i<argc; i++)
  {
    if (strcmp(argv[i],"--Label") == 0) {
      cerr << "use label" << endl;
      Converter::useLabel = true;
    } else {
      cerr << "convert-forest-hdr: syntax error, unknown option '" << string(argv[i]) << "'\n";
      exit(1);
    }
  }

  const long int LINE_MAX_LENGTH = 500000 ;

 const char* const &fileNameIN = argv[1];
 const char* const &fileNameOUT = argv[2];

 Moses::OutputFileStream outFile(fileNameOUT);
 Moses::InputFileStream inFile(fileNameIN);

 istream *inFileP = &inFile;

 int i = 0;
 while(true)
 {
   i++;
   if (i%100 == 0) cerr << "." << flush;
   char inString[LINE_MAX_LENGTH];
   SAFE_GETLINE((*inFileP), inString, LINE_MAX_LENGTH, '\n', __FILE__);
   if (inFileP->eof()) break;

   Converter converter(i,string(inString));
   converter.Process();
   string out = converter.SPrintForest();

   outFile << out << endl;
 }
cerr << "end" << endl;
 inFile.Close();
 outFile.Close();

}
