/*
 * Converter.h
 *
 *  Created on: Dec 12, 2013
 *      Author: lly
 */

#ifndef CONVERTER_TREE_H
#define CONVERTER_TREE_H

#include <string>
#include <vector>
#include "DependencyTree.h"

namespace Moses {
namespace HDR {

class ConsNode {
public:
  std::vector<std::string> labels;
  ConsNode* father;
  std::vector<ConsNode*> kids;
public:
  ConsNode(): father(NULL){}

  ConsNode(const ConsNode* node) {
    labels = node->labels;
    father = node->father;
    kids = node->kids;
  }

  ConsNode(std::string label, ConsNode* father = NULL) {
      this->labels.push_back(label);
      this->father = father;
  }

  bool IsLeaf() const {
      return kids.size() == 0;
  }
};

class Converter {
private:
  int m_sentID;
  DependencyTree m_source;
  std::vector<std::vector<std::vector<std::string> > > m_sourceLabels;
public:
  Converter(int sentID, const std::string & line);
  void Process ();
  void ProcessSubtree(int hid, int& depth);
  void ProcessInternal(int id, int position, int& depth);
  void ProcessLeaf(int id,int position);
  void ProcessHead(int hid);
  std::string SPrintForest() const;
  virtual ~Converter();
  static bool useLabel;
};

} /* namespace HDR */
} /* namespace Moses */
#endif /* CONVERTER_H_ */
