/*
 * Node.cpp
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

#include "Node.h"
//#include <string>
//#include <vector>
#include "Node.h"

using namespace std;

namespace Moses {
namespace HDR {

std::vector<std::string> Node::m_openclass = Node::InitOpenClass();
std::vector<std::string> Node::m_delClass = Node::InitDelClass();
std::vector<std::string> Node::m_modClass = Node::InitModClass();

Node::Node(int id, string word, string pos, int fid)
: m_id (id)
, m_word (word)
, m_pos (pos)
, m_fatherId (fid)
, m_consistent (true)
, m_isLeaf(true)
, m_headSpan(-1,-1)
, m_depSpan(-1,-1)
, m_wholeSpan(-1,-1) {
  // set open class
  m_isOpenClass = Node::IsOpenClass(m_pos);
  m_isDelClass = Node::IsDelClass(m_pos);
  m_isModClass = Node::IsModClass(m_pos);
}

Node::Node(const Node & node) {
  m_id = node.GetId();
  m_word = node.GetWord();
  m_pos = node.GetPos();
  m_fatherId = node.GetFatherId();
  m_consistent = node.IsConsistent();
  m_isLeaf = node.IsLeaf();
  m_headSpan = node.GetHeadSpan();
  m_depSpan = node.GetDepSpan();
  m_wholeSpan = node.getWholeSpan();
  m_isOpenClass = node.IsOpenClass();
  m_relation = node.GetRelation();
  m_isDelClass = node.IsDelClass();
  m_isModClass = node.IsModClass();
}

Node::~Node() {
  // TODO Auto-generated destructor stub
}


bool Node::GeneralAsLeaf() const {
  if (m_isLeaf && m_headSpan.first>=0 && m_isOpenClass && m_consistent) {
    return true;
  }
  return false;
}


void Node::SetHeadSpan(const Span & span) {
  //if (span.first < 0) return;
  m_headSpan = span;
}

void Node::SetDepSpan(const Span & span) {
  //if (span.first < 0) return;
  m_depSpan = span;
}

void Node::SetWholeSpan(const Span & span) {
  //if (span.first < 0) return;
  m_wholeSpan = span;
}

void Node::UpdateDepSpan(const Span & span) {
  if (span.first < 0) return;

  if (m_depSpan.first < 0) {
    SetDepSpan(span);
  } else {
    m_depSpan.first = std::min(m_depSpan.first,span.first);
    m_depSpan.second = std::max(m_depSpan.second,span.second);
    //m_depSpan.first = min;
    //m_depSpan.second = max;
  }
}

void Node::UpdateHeadSpan(const Span & span) {
  if (span.first < 0) return;

  if (m_headSpan.first < 0) {
    SetHeadSpan(span);
  } else {
    m_headSpan.first = std::min(m_headSpan.first,span.first);
    m_headSpan.second = std::max(m_headSpan.second,span.second);
    //m_depSpan.first = min;
    //m_depSpan.second = max;
  }
}

void Node::UpdateWholeSpan(const Span & span) {
  if (span.first < 0) return;

  if (m_wholeSpan.first < 0) {
    SetWholeSpan(span);
  } else {
    m_wholeSpan.first = std::min(m_wholeSpan.first,span.first);
    m_wholeSpan.second = std::max(m_wholeSpan.second,span.second);
    //m_depSpan.first = min;
    //m_depSpan.second = max;
  }
}

} /* namespace HDR */
} /* namespace Moses */
