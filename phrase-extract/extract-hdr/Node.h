#pragma once

//#ifndef NODE_H_
//#define NODE_H_

#include <string>
#include <vector>
#include <map>

namespace Moses {

namespace HDR {

typedef std::pair<int,int> Span;

//#ifndef MODPOS
//#define MODPOS
//std::string DEL_POS[] = {"DEG","DEC","DER","DEV","AS"};
//std::string MOD_POS[] = {"M"};
//#endif

class Node {
public:
  static std::vector<std::string> m_openclass;
  static std::vector<std::string> m_delClass;
  static std::vector<std::string> m_modClass;
  static std::vector<std::string> InitOpenClass() {
    std::vector<std::string> ret;
    ret.push_back("CD");
    ret.push_back("DT");
    ret.push_back("OD");
    ret.push_back("JJ");
    ret.push_back("NN");
    ret.push_back("NR");
    ret.push_back("NT");
    ret.push_back("AD");
    ret.push_back("FW");
    ret.push_back("PN");
    //ret.push_back("VV");
    //ret.push_back("VA");
    //ret.push_back("ON");
    return ret;
  }

static std::vector<std::string> InitDelClass() {
  std::vector<std::string> ret;
  ret.push_back("DEG");
  ret.push_back("DEC");
  ret.push_back("DER");
  ret.push_back("DEV");
  ret.push_back("AS");
   return ret;
  }
static std::vector<std::string> InitModClass() {
  std::vector<std::string> ret;
  ret.push_back("M");
   return ret;
  }
  static bool IsOpenClass(std::string pos) {
    return true;
 /*     for (unsigned int i = 0; i < m_openclass.size(); i++) {
        if (pos == m_openclass[i]) {
          return true;
        }
      }
      return false;*/
    }
    static bool IsDelClass(std::string pos) {
      for (unsigned int i = 0; i < m_delClass.size(); i++) {
        if (pos == m_delClass[i]) {
          return true;
        }
      }
      return false;
   }
    static bool IsModClass(std::string pos) {
          for (unsigned int i = 0; i < m_modClass.size(); i++) {
            if (pos == m_modClass[i]) {
              return true;
            }
          }
          return false;
       }
public:
  Node(int id, std::string word, std::string pos, int fid);
  Node(const Node & node);

  virtual ~Node();
  int GetId() const { return m_id;}
  int GetFatherId() const { return m_fatherId;}
  std::string GetWord() const { return m_word;}
  std::string GetPos() const { return m_pos;}
  std::string GetRelation() const { return m_relation;}
  Span GetHeadSpan() const { return m_headSpan;}
  Span GetDepSpan() const { return m_depSpan;}
  Span getWholeSpan() const { return m_wholeSpan;}
  bool IsLeaf() const { return m_isLeaf;}
  bool IsConsistent() const { return m_consistent;}
  bool IsOpenClass() const { return m_isOpenClass;}
  bool IsDelClass() const { return m_isDelClass;}
  bool IsModClass() const { return m_isModClass;}
  bool GeneralAsHead() const;
  bool GeneralAsLeaf() const;
  //bool GeneralAsInternal() const;
  void ClearDepSpan() {m_depSpan.first = m_depSpan.second = -1;}
  void SetHeadSpan(const Span & span);
  void SetDepSpan(const Span & span);
  void SetWholeSpan(const Span & span);
  void UpdateDepSpan(const Span & span);
  void UpdateHeadSpan(const Span & span);
  void UpdateWholeSpan(const Span & span);
  void SetLeaf(bool flag) { m_isLeaf = flag;}
  void SetWord(std::string word) { m_word = word; }
  void SetPos(std::string pos) { m_pos = pos; }
  void SetConsistent(bool flag) { m_consistent = flag;}
  void SetRelation(std::string relation) {m_relation = relation;}
  void SetFatherId(int fid) {m_fatherId = fid;}
private:
  std::string m_word;
  std::string m_pos;
  std::string m_relation;
  int m_id;
  int m_fatherId;
  Span m_headSpan;
  Span m_depSpan;
  Span m_wholeSpan;
  bool m_consistent;
  bool m_isLeaf;
  bool m_isOpenClass;
  bool m_isDelClass;
  bool m_isModClass;
};

} /* namespace HDR */
} /* namespace Moses */
//#endif /* NODE_H_ */
