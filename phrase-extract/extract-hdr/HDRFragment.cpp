/*
 * HDRFragment.cpp
 *
 *  Created on: Dec 5, 2013
 *      Author: lly
 */

#include "HDRFragment.h"
#include <assert.h>
#include <stdlib.h>
#include "moses/Util.h"
#include <map>
#include <algorithm>

using namespace std;


namespace Moses {
namespace HDR {

HDRFragment::HDRFragment(vector<Node> nodes, int hid)
  : m_nodes(nodes)
  , rIndex(-1), isCoreFragment(false), isShellFragment(false),isFloatFragment(false) {
  for (size_t i = 0; i < m_nodes.size(); i++) {
    if (m_nodes[i].GetId() == hid) {
      rIndex = i;
      break;
    }
  }

  if (rIndex == -1) {
    if (m_nodes[m_nodes.size()-1].GetId() < hid) {
      rIndex = -2;
    }
  }

}

vector<HDRFragment> HDRFragment::GetWellFormedFragments() const {
  vector<HDRFragment> wf_frags;
  for (size_t width = 2; width < m_nodes.size(); width++) {
    for (size_t start = 0; start <= m_nodes.size()-width; start++) {
      size_t end = start+width-1;
      vector<Node> subNodes;
    }
  }
  return wf_frags;
}

vector<HDRFragment> HDRFragment::GetFloatFragments() const {
  vector<int> indexes = SortedIndexesByDepSpan();
  vector<HDRFragment> subFragments;
  for (int start = 0; start < m_nodes.size(); start++) {
    for (int end = start+1; end < m_nodes.size(); end++) {
	
		if (rIndex >= start && rIndex <= end) break;
	
		int flags[indexes.size()];
		for (size_t i = 0; i < indexes.size(); i++) {
		  if (indexes[i] >= start && indexes[i] <= end) {
			flags[i] = 1;
		  } else {
			flags[i] = 0;
		  }
		}

		int changeCount = 0;
		for (unsigned int i = 1; i < indexes.size();i++) {
		  if (flags[i] != flags[i-1]) {
			changeCount++;
		  }
		}

		if (changeCount > 2) {
		  continue;
		}
		
		// for rest
		Node node(m_nodes[start]);
		node.SetLeaf(false);
		node.SetWord("XF");
		node.SetPos("XF");
		//node.ClearDepSpan();
		// end for
		
		vector<Node> subNodes;
        for (int i = start; i<= end; i++) {
          subNodes.push_back(m_nodes[i]);
		  
		  node.UpdateDepSpan(m_nodes[i].GetDepSpan());
        }
		
		node.SetHeadSpan(node.GetDepSpan());
		
		HDRFragment subfragment(subNodes, m_nodes[rIndex].GetId());
		subfragment.SetFloatFragment(true);
        subFragments.push_back(subfragment);
		
		// rest structure
		vector<Node> subtractNodes;
        for (int i = 0; i< start; i++) {
          subtractNodes.push_back(m_nodes[i]);
        }
		
        subtractNodes.push_back(node);
        for (size_t i = end+1; i< m_nodes.size(); i++) {
          subtractNodes.push_back(m_nodes[i]);
        }
		
		HDRFragment subtractfragment(subtractNodes, m_nodes[rIndex].GetId());
        //subtractfragment.RefreshHead();
        subtractfragment.SetFloatFragment(true);
        subFragments.push_back(subtractfragment);
	}
  }
  //cerr << subFragments.size() << endl;
  return subFragments;
}

vector<HDRFragment> HDRFragment::GetSubFragments() const {
  vector<HDRFragment> subFragments;

  if (!m_nodes[rIndex].IsConsistent()) {
    return subFragments;
  }

  vector<int> indexes = SortedIndexesByDepSpan();

  int start = rIndex, end = rIndex;
  size_t size = m_nodes.size();
  bool added = false;
  for (int si = start; si >= 0; si--) {
    for (int ei = end; ei < size; ei++) {
      if (si < ei && ei-si < size -1) {

        int flags[indexes.size()];
        for (size_t i = 0; i < indexes.size(); i++) {
          if (indexes[i] >= si && indexes[i] <= ei) {
            flags[i] = 1;
          } else {
            flags[i] = 0;
          }
        }

        int changeCount = 0;
        for (unsigned int i = 1; i < indexes.size();i++) {
          if (flags[i] != flags[i-1]) {
            changeCount++;
          }
        }

        if (changeCount > 2) {
          continue;
        }

        Node head2(m_nodes[rIndex]);
        // nodes for sub fragments
        vector<Node> subNodes;
        for (int i = si; i<= ei; i++) {
          subNodes.push_back(m_nodes[i]);
          if (i != rIndex)
            head2.UpdateHeadSpan(m_nodes[i].GetDepSpan());
        }
        //nodes for subtract fragments
        vector<Node> subtractNodes;
        for (int i = 0; i< si; i++) {
          subtractNodes.push_back(m_nodes[i]);
        }
        //subtractNodes.push_back(m_nodes[rIndex]);
        subtractNodes.push_back(head2);
        for (size_t i = ei+1; i< m_nodes.size(); i++) {
          subtractNodes.push_back(m_nodes[i]);
        }

        if ((si == rIndex && ei == m_nodes.size()-1) || (ei == rIndex && si == 0)) {
          if (added) {
            continue;
          }
        }

        HDRFragment subfragment(subNodes, m_nodes[rIndex].GetId());
        subfragment.RefreshHead();
        subfragment.SetCoreFragment(true);
        subFragments.push_back(subfragment);


        HDRFragment subtractfragment(subtractNodes, m_nodes[rIndex].GetId());
        //fragment.SetPrecond(0,true); // head must be generalized, no lexical rule.
        subtractfragment.RefreshHead();
        subtractfragment.SetShellFragment(true);
        subFragments.push_back(subtractfragment);

        if ((si == rIndex && ei == m_nodes.size()-1) || (ei == rIndex && si == 0)) {
          added = true;
        }

      }
    }
  }

  return subFragments;
}

void HDRFragment::RefreshHead() {
  Span span(-1,-1);
  m_nodes[rIndex].SetDepSpan(span);
  
  for (size_t i = 0; i < m_nodes.size(); i++) {
    if (i == rIndex) {
      if (m_nodes[i].IsConsistent()) {
        m_nodes[rIndex].UpdateDepSpan(m_nodes[i].GetHeadSpan());
      }
    } else {
      m_nodes[rIndex].UpdateDepSpan(m_nodes[i].GetDepSpan());
    }
  }
}

std::vector<Span> HDRFragment::SortedSpan() const {
  vector<Span> ret;
  vector<int> indexes = SortedIndexesByDepSpan();
  for (size_t i = 0; i < indexes.size(); i++) {
     int index = indexes[i];
     Span span = m_nodes[index].GetDepSpan();
     if (index == rIndex) {
       span = m_nodes[index].GetHeadSpan();
     }
     ret.push_back(span);
  }
  return ret;
}

bool comparator ( const pair<string,int>& l, const pair<string,int>& r)
   { return l.first < r.first; }

vector<int> HDRFragment::SortVector(vector<string> vct) const {
  vector<int> idx(vct.size());
  idx[idx.size()-1] = vct.size()-1;

  typedef pair<string,int> mypair;
  vector<mypair> temp;
  for(int i = 0; i < vct.size()-1; i++) {
    mypair a(vct[i],i);
    temp.push_back(a);
  }

  std::sort(temp.begin(), temp.end(), comparator);

  for(int i = 0; i < temp.size(); i++) {
    idx[temp[i].second] = i;
  }

  return idx;
}

vector<ExtractedHDRRule> HDRFragment::GetRules(const vector<bool> & flags, const std::vector<std::string> & target, const std::vector<int> & alignedCountT, const ExtractHDROptions & options) const {
  vector<ExtractedHDRRule> ret;
//cerr << "in get rule" << endl;
  int headID = rIndex>=0 ? m_nodes[rIndex].GetId():-1;
  // source
  //cerr << "source label" << endl;
  vector<string> sourceLabels = GetGeneralSourceLabels(flags,options);
  //vector<int> sindx = SortVector(sourceLabels);
  //std::sort(sourceLabels.begin(),sourceLabels.end()-1);

  string sourceString = VectorToString(sourceLabels);
  int startS = m_nodes[0].GetId();
  int endS = m_nodes[m_nodes.size()-1].GetId();
  //
  //cerr << "sort" << endl;
  vector<int> indexes = SortedIndexesByDepSpan();
  
  if (indexes.size() == 0) {
    return ret;
  }
  
  //cerr << "extend" << endl;
  vector<vector<Span> > allSpans = ExtendedSpans(flags, indexes, alignedCountT);
//cerr << "main" << endl;
  //map<string,int> existingTarget;
  assert ( allSpans.size() == 1 );
  vector<vector<Span> >::iterator iter;
  for (iter = allSpans.begin(); iter != allSpans.end(); iter++) {
    vector<Span> spans = *iter;
    int startE = spans[0].first;
    int endE = spans[spans.size()-1].second;

    assert(indexes.size() == spans.size());

    for (startE = spans[0].first; startE >= 0 && (startE == spans[0].first || alignedCountT[startE] <= 0); startE--) {
      for (endE = spans.back().second; endE < target.size() && (endE == spans.back().second || alignedCountT[endE] <= 0); endE++) {
			//cerr << startE << " " << endE << endl;
			vector<string> targetLabels;
			vector<string> alignItems;
			vector<string> alignInvItems;
			int tcount = 0;
			
			//cerr << "target label" << endl;
			for (int e = startE; e < spans[0].first; e++) {
			  targetLabels.push_back(target[e]);
			  tcount++;
			}
			  
			int preEndE = -1;
			for (unsigned int j = 0; j < spans.size(); j++) {
			  Span span = spans[j];
			  int index = indexes[j];
			  string slabel = sourceLabels[index];
			  
			  if (j > 0) {
			    if (!flags[indexes[j-1]] && (indexes[j-1] == rIndex || m_nodes[indexes[j-1]].IsLeaf())) {
			      for (int e = preEndE+1; e < span.first && alignedCountT[e] <= 0; e++) {
					 targetLabels.push_back(target[e]);
					 tcount++;
					 preEndE++;
				  }
			    }
				if (!flags[index] && (index == rIndex || m_nodes[index].IsLeaf())) {
				  int e = span.first;
				  while ( e > 0 && e > preEndE+1) {
				    if (alignedCountT[e-1] <= 0) {
					  e--;
					} else {
					  break;
					}
				  }
				  for (; e < span.first; e++) {
				    targetLabels.push_back(target[e]);
				    tcount++;
				  }
				}
			  }			   

			  if (!flags[index] && (index == rIndex || m_nodes[index].IsLeaf())) {
				for (int tpos = span.first; tpos <= span.second; tpos++) {
				  targetLabels.push_back(target[tpos]);
				  assert (alignedCountT[tpos] <= 1);

				  if (alignedCountT[tpos] == 1) {

					string idxString = SPrint<int>(index);
					string tcString = SPrint<int>(tcount);

					alignItems.push_back(idxString+"-"+tcString);
					alignInvItems.push_back(tcString+"-"+idxString);
				  }
				  tcount++;
				}
			  } else {
				string idxString = SPrint<int>(index);
				string tcString = SPrint<int>(tcount);
				targetLabels.push_back(slabel);
				alignItems.push_back(idxString+"-"+tcString);
				alignInvItems.push_back(tcString+"-"+idxString);
				tcount++;
			  }
			  preEndE = span.second;
			}
			
			for (int e = preEndE+1; e <= endE; e++) {
			  targetLabels.push_back(target[e]);
			  tcount++;
			}

			targetLabels.push_back("[X]");

			ExtractedHDRRule rule(startE,endE,startS,endS,headID);
			rule.source = sourceString;
			rule.target = VectorToString(targetLabels);
			rule.alignment = VectorToString(alignItems);
			rule.alignmentInv = VectorToString(alignInvItems);
			rule.count = 1;

		   // if (existingTarget.find(rule.target) == existingTarget.end()) {
			ret.push_back(rule);
			
			// 
			if (flags[indexes.back()] || (indexes.back() != rIndex && !m_nodes[indexes.back()].IsLeaf())) {
			  break;
			}
		}
		if (flags[indexes[0]] || (indexes[0] != rIndex && !m_nodes[indexes[0]].IsLeaf())) {
		  break;
		}
      }
  }

  return ret;
}

string HDRFragment::VectorToString(const vector<string> & vec) const {
  string ret = "";
  for (size_t i = 0; i < vec.size(); i++) {
    ret += vec[i] + " ";
  }
  return ret.erase(ret.size()-1);
}

vector<vector<Span> > HDRFragment::ExtendedSpans(const vector<bool> & flags, const vector<int> & indexes, const std::vector<int> & alignedCountT) const {
  vector<vector<Span> > ret;
  vector<Span> oneInstance;
  for (size_t i = 0; i < indexes.size(); i++) {
    int index = indexes[i];
    Span span = m_nodes[index].GetDepSpan();
    if (index == rIndex) {
      span = m_nodes[index].GetHeadSpan();
    }
	oneInstance.push_back(span);
  }
  ret.push_back(oneInstance);
  return ret;
}

vector<vector<Span> > HDRFragment::AddOneSpanToCollection(const Span & span, const vector<vector<Span> > & coll) const {
  vector<vector<Span> > ret;
  if (coll.size() == 0) {
    vector<Span> element;
    element.push_back(span);
    ret.push_back(element);
    return ret;
  }

  vector<vector<Span> >::const_iterator iter;
  for (iter = coll.begin(); iter != coll.end(); iter++) {
    vector<Span> element = *iter;
    if (Overlap(span, element[element.size()-1])) continue;
    element.push_back(span);
    ret.push_back(element);
  }
  return ret;
}

vector<int> HDRFragment::SortedIndexesByDepSpan() const {
  vector<int> ret;
  for (size_t i = 0; i < m_nodes.size(); i++) {
    Span depSpan = m_nodes[i].GetDepSpan();
    if (i == rIndex) {
      depSpan = m_nodes[i].GetHeadSpan();
    }
    if (depSpan.first >= 0) {
      size_t pos = 0;
      for (pos = 0; pos < ret.size(); pos++) {
        Span preSpan = m_nodes[ret[pos]].GetDepSpan();
        if (ret[pos] == rIndex) {
          preSpan = m_nodes[ret[pos]].GetHeadSpan();
        }
        if (depSpan.first < preSpan.first) {
          break;
        }
      }
      if (pos >= ret.size()) {
        ret.push_back(i);
      } else {
        vector<int>::iterator iter = ret.begin();
        ret.insert(iter+pos,i);
      }
    }
  }
  return ret;
}

vector<string> HDRFragment::GetGeneralSourceLabels(const vector<bool> & generalFlags, const ExtractHDROptions & options) const {
  vector<string> ret;
  assert (generalFlags.size() == m_nodes.size());

  for (int i = 0; i < m_nodes.size(); i++) {
    string label;
    int pos = i-rIndex;
    if (isFloatFragment && rIndex < 0) {
      if (rIndex == -1)
        pos = i+1;
      else
        pos = -i-1;
    }

    if (i == rIndex) {
        label = generalFlags[i] ? ("["+m_nodes[i].GetPos()+":0][X]") : m_nodes[i].GetWord();
      
    } else if (m_nodes[i].IsLeaf()) {

        label = generalFlags[i] ? ("[" +m_nodes[i].GetPos()+":"+SPrint<int>(pos)+"][X]") : m_nodes[i].GetWord();

    } else {
      label = "[" ;
	  label = generalFlags[i] ? (label+m_nodes[i].GetPos()) : (label+m_nodes[i].GetWord());
      label += ":"+SPrint<int>(pos)+"][X]";
    }
    ret.push_back(label);
  }

  string father = "[H"+ SPrint<int>(rIndex) +"]";
  //string father = "[H]";

  //if (IsSubFragment()) {
    //father = "[XF"+ SPrint<int>(rIndex) +"]";
  //}
  ret.push_back(father);
  return ret;
}

vector<vector<bool> > HDRFragment::EnumerateGeneralFlag() const {
  // flag for : head leaf internal
  vector<bool> flags = CreateGeneralFlag();

  // enumerate up to 8 combinations
  vector<vector<bool> > ret; // flag for each node

  if (isShellFragment && !flags[0])
    return ret;

  // fisrt all lexical
  vector<bool> forLex;
  for (size_t i = 0; i < m_nodes.size(); i++) {
    if (i == rIndex && isShellFragment)
      forLex.push_back(true);
    else forLex.push_back(false);
  }
  ret.push_back(forLex);

  // second for head
  if (flags[0] && rIndex >= 0 & !isShellFragment) {
    vector<int> indexes;
    indexes.push_back(rIndex);
    ExtendWithIndexes(ret, indexes);
  }
  // third for leaf
  if (flags[1]) {
    vector<int> indexes = GetGeneralLeafIndex();
    ExtendWithIndexes(ret, indexes);
  }
  // last for internal
  if (flags[2]) {
    vector<int> indexes = GetGeneralInternalIndex();
    ExtendWithIndexes(ret, indexes);
  }

  return ret;
}

void HDRFragment::ExtendWithIndexes(vector<vector<bool> > & coll, const vector<int> & indexes) const {
  int size = coll.size();
  for (int i = 0; i < size; i++) {
    vector<bool> instance = coll[i];
    for (size_t j = 0; j < indexes.size(); j++) {
      instance[indexes[j]] = true;
    }
    coll.push_back(instance);
  }
}

vector<int> HDRFragment::GetGeneralLeafIndex() const {
  vector<int> indexes;
  for (size_t i = 0; i < m_nodes.size(); i++) {
    if (m_nodes[i].IsLeaf() && m_nodes[i].GeneralAsLeaf()) {
      indexes.push_back(i);
    }
  }
  return indexes;
}

vector<int> HDRFragment::GetGeneralInternalIndex() const {
  vector<int> indexes;
  for (size_t i = 0; i < m_nodes.size(); i++) {
    if (i != rIndex && !m_nodes[i].IsLeaf()) {
      indexes.push_back(i);
    }
  }
  return indexes;
}

vector<bool> HDRFragment::CreateGeneralFlag() const {
  // flag for : head leaf internal
  vector<bool> ret;
  ret.push_back(false);
  ret.push_back(false);
  ret.push_back(false);
  for (size_t i = 0; i < m_nodes.size(); i++) {
    if (i == rIndex) {
      ret[0] = m_nodes[i].GetHeadSpan().first >= 0 && m_nodes[i].IsConsistent();
    } else if (m_nodes[i].IsLeaf()) {
      if (m_nodes[i].GeneralAsLeaf()) {
        ret[1] = true;
      }
    } else {
      ret[2] = true;
    }
  }
  return ret;
}

/**
 * all the children nodes are leaf
 */
bool HDRFragment::IsLowest() const {
  for (size_t i = 0; i < m_nodes.size(); i++) {
    if (i != rIndex && !m_nodes[i].IsLeaf()) {
      return false;
    }
  }
  return true;
}

bool HDRFragment::IsValid(const ExtractHDROptions & options) const {
//cerr << rIndex << endl << flush;
bool empty = true;
  if (rIndex >= 0) {
    if (m_nodes[rIndex].GetDepSpan().first < 0) return false;
    if (!m_nodes[rIndex].IsConsistent()) return false;
	empty = false;
  }
//cerr << m_nodes.size() << endl << flush;
  for (size_t i = 0; i < m_nodes.size(); i++) {

    if (i != rIndex) {
      if (m_nodes[i].IsLeaf()) {
        if (!m_nodes[i].IsConsistent()) return false;
      }  else
      if (m_nodes[i].GetDepSpan().first < 0) {
        return false;
      }
	  
	  if (m_nodes[i].GetDepSpan().first >= 0) {
	    empty = false;
	  }
    }

    Span spani = m_nodes[i].GetDepSpan();
    if (i == rIndex) spani = m_nodes[i].GetHeadSpan();

    for (size_t j = 0; j < i; j++) {
      Span spanj = m_nodes[j].GetDepSpan();
      if (j == rIndex) spanj = m_nodes[j].GetHeadSpan();

      if (Overlap(spani, spanj)) {
        return false;
      }

    }
  }

  return !empty;
}

bool HDRFragment::Overlap(const Span & spani, const Span & spanj) const {
  if (spani.first < 0 || spanj.first < 0) return false;
  if (spani.second < spanj.first || spani.first > spanj.second) {
    return false;
  }
  return true;
}

HDRFragment::~HDRFragment() {
  // TODO Auto-generated destructor stub
}

} /* namespace HDR */
} /* namespace Moses */
