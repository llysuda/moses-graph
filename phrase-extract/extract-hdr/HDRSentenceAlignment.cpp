/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2010 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include "SentenceAlignment.h"
#include "Node.h"
#include <map>
#include <set>
#include <algorithm>
#include "HDRSentenceAlignment.h"

#include "tables-core.h"

using namespace std;

namespace Moses
{
namespace HDR
{

HDRSentenceAlignment::~HDRSentenceAlignment() {}

void addBoundaryWords(vector<string> &phrase)
{
  phrase.insert(phrase.begin(), "<s>");
  phrase.push_back("</s>");
}

bool HDRSentenceAlignment::processTargetSentence(const char * targetString, int, bool boundaryRules)
{
  target = tokenize(targetString);
  if (boundaryRules)
    addBoundaryWords(target);
  return true;
}

bool HDRSentenceAlignment::processSourceSentence(const char * sourceString, int, bool boundaryRules)
{
  source.Init(sourceString);
  return true;
}

bool HDRSentenceAlignment::create( char targetString[], char sourceString[], char alignmentString[], int sentenceID, bool boundaryRules)
{
  
  this->sentenceID = sentenceID;

  // process sentence strings and store in target and source members.
  if (!processTargetSentence(targetString, sentenceID, boundaryRules)) {
    return false;
  }
  if (!processSourceSentence(sourceString, sentenceID, boundaryRules)) {
    return false;
  }

  // check if sentences are empty
  if (target.size() == 0 || source.Size() == 0) {
    cerr << "no target (" << target.size() << ") or source (" << source.Size() << ") words << end insentence " << sentenceID << endl;
    cerr << "T: " << targetString << endl << "S: " << sourceString << endl;
    return false;
  }

  // prepare data structures for alignments
  for(size_t i=0; i<target.size(); i++) {
    alignedCountT.push_back( 0 );
    vector< int > dummy;
    alignedToS.push_back( dummy );
  }
  for(size_t i=0; i<source.Size(); i++) {
    vector< int > dummy;
    alignedSoT.push_back( dummy );
  }

  // reading in alignments
  vector<string> alignmentSequence = tokenize( alignmentString );
  for(size_t i=0; i<alignmentSequence.size(); i++) {
    int s,t;
    // cout << "scaning " << alignmentSequence[i].c_str() << endl;
    if (! sscanf(alignmentSequence[i].c_str(), "%d-%d", &s, &t)) {
      cerr << "WARNING: " << alignmentSequence[i] << " is a bad alignment point in sentence " << sentenceID << endl;
      cerr << "T: " << targetString << endl << "S: " << sourceString << endl;
      return false;
    }

    // cout << "alignmentSequence[i] " << alignmentSequence[i] << " is " << s << ", " << t << endl;
    if ((size_t)t >= target.size() || (size_t)s >= source.Size()) {
      cerr << "WARNING: sentence " << sentenceID << " has alignment point (" << s << ", " << t << ") out of bounds (" << source.Size() << ", " << target.size() << ")\n";
      cerr << "T: " << targetString << endl << "S: " << sourceString << endl;
      return false;
    }
    //if (options.DelAlign() && source.GetNode(s).IsDelClass()) {
    //  continue;
    //}
    alignedSoT[s].push_back( t );
    alignedToS[t].push_back( s );
    alignedCountT[t]++;
  }

  vector<int> roots = source.GetRootId();
  for (size_t i = 0; i < roots.size(); i++)
    source.Annotate(roots[i], alignedCountT, alignedSoT);
  //source.PrintAnnotation();
  return true;
}

//void HDRSentenceAlignment::Annotate() {

//}

void HDRSentenceAlignment::modifyAlignment()
{
  std::vector<int>::iterator iter;
  for (size_t i = 0; i < source.Size(); i++) {
    Node n = source.GetNode(i);
    if (n.IsDelClass() || (n.IsModClass() && !n.IsConsistent())) {
      for (size_t j = 0; j < alignedSoT[i].size(); j++) {
        int t = alignedSoT[i][j];
        alignedCountT[t]--;
        iter = std::find(alignedToS[t].begin(),alignedToS[t].end(), i);
        if (iter != alignedToS[t].end()) {
          alignedToS[t].erase(iter);
        }
      }
      alignedSoT[i].clear();
    }
  }
}

std::string HDRSentenceAlignment::GetAlignString() const {
  string ret = "";
  for (size_t t = 0; t < alignedToS.size(); t++) {
    for (size_t i = 0; i < alignedToS[t].size(); i++) {
      int s = alignedToS[t][i];
      ret += SPrint<int>(s)+"-"+SPrint<int>(t)+" ";
    }
  }
  return ret.erase(ret.size()-1);
}

}
}

