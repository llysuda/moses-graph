/*
 * HDRFragment.h
 *
 *  Created on: Dec 5, 2013
 *      Author: lly
 */

#ifndef HDRFRAGMENT_H_
#define HDRFRAGMENT_H_

#include <vector>
#include <string>
#include "ExtractedHDRRule.h"
#include "Node.h"
#include "ExtractHDROptions.h"

namespace Moses {
namespace HDR {

class HDRFragment {

public:
  bool IsCoreFragment() const {return isCoreFragment;}
  bool IsShellFragment() const {return isShellFragment;}
  bool IsFloatFragment() const {return isFloatFragment;}
  HDRFragment(std::vector<Node> nodes, int hid);
  virtual ~HDRFragment();
  bool IsValid(const ExtractHDROptions &) const;
  bool IsLowest() const;
  bool Overlap(const Span & spani, const Span & spanj) const;
  std::vector<std::vector<bool> > EnumerateGeneralFlag() const;
  std::vector<std::string> GetGeneralSourceLabels(const std::vector<bool> & genralFlags, const ExtractHDROptions &) const;
  std::vector<ExtractedHDRRule> GetRules(const std::vector<bool> &, const std::vector<std::string> &, const std::vector<int> &, const ExtractHDROptions &) const;
  std::vector<HDRFragment> GetSubFragments() const;
  std::vector<HDRFragment> GetFloatFragments() const;
  std::vector<HDRFragment> GetWellFormedFragments() const;
  //std::vector<HDRFragment> GetSubtractFragments() const;
  //void SetPrecond(int index, bool flag);
  //Node GetSubFragmentNode(int start, int end) const;
  void RefreshHead();
  int GetSize() const { return m_nodes.size(); }
  int GetID(int index) const { return m_nodes[index].GetId(); }
  void SetCoreFragment(bool flag) { isCoreFragment = flag;}
  void SetShellFragment(bool flag) { isShellFragment = flag;}
  void SetFloatFragment(bool flag) { isFloatFragment = flag;}
  std::vector<int> SortedIndexesByDepSpan() const;
  std::vector<Span> SortedSpan() const;
  std::vector<int> SortVector(std::vector<std::string>) const;
private:
  bool isCoreFragment;
  bool isShellFragment;
  bool isFloatFragment;
  std::vector<bool> CreateGeneralFlag() const;
  std::vector<int> GetGeneralLeafIndex() const;
  std::vector<int> GetGeneralInternalIndex() const;
  void ExtendWithIndexes(std::vector<std::vector<bool> > & coll, const std::vector<int> & indexes) const;
  std::vector<std::vector<Span> > ExtendedSpans(const std::vector<bool> & flags, const std::vector<int> & indexes, const std::vector<int> & alignedCountT) const;
  std::vector<std::vector<Span> > AddOneSpanToCollection(const Span & span, const std::vector<std::vector<Span> > & coll) const;
  std::string VectorToString(const std::vector<std::string> & vec) const;
  //void addRuleToCollection(ExtractedHDRRule & newRule, std::vector<ExtractedHDRRule> & coll) const;
private:
  std::vector<Node> m_nodes;
  int rIndex;
  //std::vector<bool> m_preCond;
};

} /* namespace HDR */
} /* namespace Moses */
#endif /* HDRFRAGMENT_H_ */
