/*
 * Converter.h
 *
 *  Created on: Dec 12, 2013
 *      Author: lly
 */

#ifndef CONVERTER_FOREST_H
#define CONVERTER_FOREST_H

#include <string>
#include <vector>
#include "DependencyTree.h"

namespace Moses {
namespace HDR {

class ConverterForest {
private:
  int m_sentID;
  DependencyTree m_source;
  std::vector<std::vector<std::vector<std::string> > > m_sourceLabels;
public:
  ConverterForest(int sentID, const std::string & line);
  void Process ();
  void ProcessSubtree(int hid, int& depth);
  void ProcessInternal(int id, int position, int& depth);
  void ProcessLeaf(int id,int position);
  void ProcessHead(int hid);
  std::string SPrintForest() const;
  virtual ~ConverterForest();
  static bool useLabel;
};

} /* namespace HDR */
} /* namespace Moses */
#endif /* CONVERTER_H_ */
