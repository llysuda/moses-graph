/*
 * DependencyTree.h
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

#ifndef EXTRACT_HDR_DEPENDENCYTREE_H_
#define EXTRACT_HDR_DEPENDENCYTREE_H_

#include <string>
#include <vector>
#include "Node.h"
#include "HDRFragment.h"

namespace Moses
{
namespace HDR
{

class DependencyTree
{
private:
  std::vector<Node> m_nodes;
  std::vector<int> m_rootId;

public:
  DependencyTree();
  DependencyTree(std::string line);
  virtual ~DependencyTree();

  void Init(const std::string & line);
  int Size() const {return m_nodes.size();}
  std::vector<int> GetPostOrder(int headId) const;
  void Annotate (int headId, const std::vector<int> & alignedCountS, const std::vector<std::vector<int> > & alignedToT);
  std::vector<int> GetRootId() const { return m_rootId;}
  Node GetNode(int index) const { return m_nodes[index]; }
  bool IsLeaf(int index) { return m_nodes[index].IsLeaf();}
  std::string GetWord(int index) const { return m_nodes[index].GetWord();}
  HDRFragment CreateHDRFragment(int index) const;
  std::vector<int> GetSubtreeIndexes(int index) const;
  std::vector<Node> GetSubtreeNodes(int index) const;
  Span GetSourceSpan(int index) const;
  void CheckSequence() const;
  std::string GetFactorString() const;
  std::vector<int> GetLeftChildrenIndexes(int index) const;
  std::vector<int> GetRightChildrenIndexes(int index) const;
  void SetFatherId(int nid, int fid);
  void PrintAnnotation() const;

};

} /* namespace HDR */
} /* namespace Moses */

#endif /* DEPENDENCYTREE_H_ */
