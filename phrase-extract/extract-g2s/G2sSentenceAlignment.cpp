/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2010 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include <map>
#include <set>
#include <string>
#include <algorithm>

#include "G2sSentenceAlignment.h"
#include "tables-core.h"
#include "moses/Util.h"

using namespace std;
using namespace Moses;

namespace MosesG2S
{

vector<string> tokenize( const char* input, const char separator=' ' )
{
  vector< string > token;
  bool betweenWords = true;
  int start=0;
  int i=0;
  for(; input[i] != '\0'; i++) {
    bool isSpace = (input[i] == separator);
    if (!isSpace && betweenWords) {
      start = i;
      betweenWords = false;
    } else if (isSpace && !betweenWords) {
      token.push_back( string( input+start, i-start ) );
      betweenWords = true;
    }
  }
  if (!betweenWords)
    token.push_back( string( input+start, i-start ) );
  return token;
}

MultiDiGraph* CreateGraphFromLine(const char* line, const G2sExtractionOptions& options) {
//  bool sibling = options.sibling;
	vector<string> tokens = tokenize(line);
	vector< vector<int> > children;
	children.resize(tokens.size());

	MultiDiGraph* mdg = new MultiDiGraph(tokens.size());
	for(size_t i = 0; i < tokens.size(); i++) {
	  vector<string> nodeAttrib = tokenize(tokens[i].c_str(),'|');
	  if (nodeAttrib.size() < 3) {
		cerr << "error source format !" << endl;
		exit(1);
	  }

	  int fid = std::atoi(nodeAttrib[2].c_str());

	  // add node
	  struct Node* n = new Node();
	  n->id = i;
	  n->word = nodeAttrib[0];
	  n->pos = nodeAttrib[1];
	  n->fid = fid;
	  mdg->add_node(i, n);

	  if (fid >= 0)
	    children[fid].push_back(i);

	  // add dep edge
	  string label = nodeAttrib[3];
	  if (fid >= 0)
		  mdg->add_edge(fid, i, "DP");
	  // add bigram edge
	  if (i > 0)
		  mdg->add_edge(i, i-1, "BG");
	}

	return mdg;
}

G2sSentenceAlignment::~G2sSentenceAlignment() {}

void addBoundaryWords(vector<string> &phrase)
{
  phrase.insert(phrase.begin(), "<s>");
  phrase.push_back("</s>");
}

//bool G2sSentenceAlignment::any_between_connected(const std::vector<std::vector<int> >& subgraphs) const
//{
//	int size = (int)subgraphs.size();
//	if (size <= 1) return false;
//
//	for(int i = 0; i < size-1; i++) {
//		for (int j = i+1; j< size; j++) {
//			if (source->between_connected(subgraphs[i], subgraphs[j]))
//				return true;
//		}
//	}
//	return false;
//}
//
//bool G2sSentenceAlignment::both_between_connected(const std::vector<int>& nids, const std::vector<std::vector<int> >& subgraphs) const
//{
//	int size = (int)subgraphs.size();
//	if (size <= 1) return true;
//
//	BUIset remains(nids.begin(), nids.end());
//	set<int>::const_iterator iter1, iter2;
//	for(int i = 0; i < size; i++) {
//		iter1 = subgraphs[i].begin();
//		for(; iter1 != subgraphs[i].end(); iter1++)
//			iter2 = remains.find(*iter1);
//			if (iter2 != remains.end())
//				remains.erase(*iter2);
//	}
//
//	if (remains.size() == 0)
//		return false;
//
//	for(int i = 0; i < size; i++) {
//		if (!source->between_connected(remains, subgraphs[i]))
//			return false;
//	}
//	return true;
//}

bool G2sSentenceAlignment::processTargetSentence(const char * targetString, int, bool boundaryRules)
{
  vector<string> words = tokenize(targetString);

  for(vector<string>::iterator iter = words.begin(); iter != words.end(); iter++) {
  	string word = *iter;
  	vector<string> factors = tokenize(word.c_str(),'|');

  	target.push_back(factors[0]);

  	if (factors.size() > 1) {
  		// dep format
  		if (factors.size() < 3) {
			cerr << "error target format !" << endl;
			exit(1);
			}
  		targetPos.push_back(factors[1]);
  		targetLabel.push_back(factors[3]);
  		targetFid.push_back(atoi(factors[2].c_str()));
  	}
  }

  if (boundaryRules)
    addBoundaryWords(target);
  return true;
}

bool G2sSentenceAlignment::processSourceSentence(const char * sourceString, int, const G2sExtractionOptions& options)
{
  //source = tokenize(sourceString);
  //if (boundaryRules)
  //  addBoundaryWords(source);
  source = CreateGraphFromLine(sourceString, options);
  return true;
}

bool G2sSentenceAlignment::create( char targetString[], char sourceString[], char alignmentString[], char weightString[], int sentenceID, const G2sExtractionOptions& options)
{
  using namespace std;
  this->sentenceID = sentenceID;
  this->weightString = std::string(weightString);

  bool boundaryRules = options.boundaryRules;

  // process sentence strings and store in target and source members.
  if (!processTargetSentence(targetString, sentenceID, boundaryRules)) {
    return false;
  }
  if (!processSourceSentence(sourceString, sentenceID, options)) {
    return false;
  }

  // check if sentences are empty
  if (target.size() == 0 || source->size() == 0) {
    cerr << "no target (" << target.size() << ") or source (" << source->size() << ") words << end insentence " << sentenceID << endl;
    cerr << "T: " << targetString << endl << "S: " << sourceString << endl;
    return false;
  }

  // prepare data structures for alignments
  for(size_t i=0; i<source->size(); i++) {
    alignedCountS.push_back( 0 );
  }
  for(size_t i=0; i<target.size(); i++) {
    vector< int > dummy;
    alignedToT.push_back( dummy );
  }

  // reading in alignments
  vector<string> alignmentSequence = tokenize( alignmentString );
  for(size_t i=0; i<alignmentSequence.size(); i++) {
    int s,t;
    // cout << "scaning " << alignmentSequence[i].c_str() << endl;
    if (! sscanf(alignmentSequence[i].c_str(), "%d-%d", &s, &t)) {
      cerr << "WARNING: " << alignmentSequence[i] << " is a bad alignment point in sentence " << sentenceID << endl;
      cerr << "T: " << targetString << endl << "S: " << sourceString << endl;
      return false;
    }

    if (boundaryRules) {
      ++s;
      ++t;
    }

    // cout << "alignmentSequence[i] " << alignmentSequence[i] << " is " << s << ", " << t << endl;
    if ((size_t)t >= target.size() || (size_t)s >= source->size()) {
      cerr << "WARNING: sentence " << sentenceID << " has alignment point (" << s << ", " << t << ") out of bounds (" << source->size() << ", " << target.size() << ")\n";
      cerr << "T: " << targetString << endl << "S: " << sourceString << endl;
      return false;
    }
    alignedToT[t].push_back( s );
    alignedCountS[s]++;
  }

  if (boundaryRules) {
    alignedToT[0].push_back(0);
    alignedCountS[0]++;

    alignedToT.back().push_back(alignedCountS.size() - 1);
    alignedCountS.back()++;

  }

  return true;
}

GraphRange G2sSentenceAlignment::get_unalignedF(const GraphRange& nids) {
	GraphRange unalignedF;
		int prev = -1;
		for(GraphRange::const_iterator iter = nids.begin();
				iter != nids.end(); ++iter) {
			int id = *iter;
			if (prev >= 0) {
				if (id != prev+1) {
					if (alignedCountS[prev+1] == 0)
						unalignedF.push_back(prev+1);
					if (alignedCountS[id-1] == 0)
						unalignedF.push_back(id-1);
				}
			}
			prev = id;
		}
		// boundary
		int start = *(nids.begin())-1;
		if (start >= 0 && alignedCountS[start] == 0)
			unalignedF.push_back(start);
		int end = prev+1;
		if (end < source->size() && alignedCountS[end] == 0)
					unalignedF.push_back(end);
	return unalignedF;
}

struct less_than_key
{
    inline bool operator() (const pair<string,int>& struct1,
    												const pair<string,int>& struct2) const
    {
        return (struct1.first < struct2.first);
    }
} node_key;

void G2sSentenceAlignment::get_sorted_words(const GraphRange& nids,
				vector<pair<string, int> >& words){

	for(GraphRange::const_iterator iter = nids.begin(); iter != nids.end(); iter++) {
		int nid = *iter;
		string word = source->get_word(nid);
		words.push_back(make_pair<string,int>(word,nid));
	}
//	if (sort)
//		std::stable_sort(words.begin(), words.end(), node_key);
}

vector<string> G2sSentenceAlignment::get_structure(const GraphRange& nids, bool incLabel) const {
	vector<string> ret;
	int size = (int)nids.size();
	for(int i = 0; i < size; i++) {
		int nid = nids[i];
		bool success = false;
		string retStr = "";
		if (source->has_succ(nid)) {
			const EdgeMap& succ_edges = source->get_succ_edges(nid);
			for(int j = 0; j < size; j++) {
				if (i == j)
					continue;
				int sid = nids[j];
				EdgeMap::const_iterator it = succ_edges.find(sid);
				if ( it != succ_edges.end() ) {
					success = true;
					const LabelSet& labels = it->second;
					for(LabelSet::const_iterator it2 = labels.begin();
							it2 != labels.end(); it2++) {
						string label = *it2;
						retStr += SPrint<int>(j) + ":";
						if (incLabel)
							retStr += label + ":";
						else
							break;
					}
				}
			}
		}
		if (!success){
			ret.push_back("-1");
		} else {
			retStr = retStr.erase(retStr.size()-1);
			ret.push_back(retStr);
		}
	}
	return ret;
}

string G2sSentenceAlignment::get_structure_string(const GraphRange& nids, bool incLabel)
{
	TermCacheType::const_iterator iter = termCache.find(nids);
	if (iter != termCache.end()) {
		return iter->second;
	}

	string retStr = "";
	vector<string> strs = get_structure(nids, incLabel);
	for(size_t i = 0; i < strs.size(); i++)
		retStr += strs[i] + "_";
	retStr = retStr.erase(retStr.size()-1);

	termCache[nids] = retStr;
	return retStr;
}

std::string G2sSentenceAlignment::get_target_struct(int start, int end) const
{
	string ret = "X";

	for(int i = start; i <= end; i++) {
		int fid = targetFid[i];
		if (fid < start || fid > end) {
			fid = -1;
		} else {
			fid -= start;
		}
		ret += "_" + SPrint<int>(fid);
	}

	return ret;
}

std::string G2sSentenceAlignment::GetTargetLabel(int startT, int endT) const
{
	string ret = "";
	for(int i = startT; i <= endT; i++) {
		int fid = targetFid[i];
		if (fid < startT || fid > endT) {
			ret += targetPos[i] + "_";
		}
	}
	return ret.erase(ret.size()-1);
}

std::string G2sSentenceAlignment::GetSourceLabel(const GraphRange& nids) const
{
  string ret = "";
  for(GraphRange::const_iterator iter = nids.begin();
      iter != nids.end(); ++iter) {
    int id = *iter;
    int fid = source->get_fid(id);
    bool found = false;
    for(size_t i = 0; i < nids.size(); i++) {
      if (nids[i] > fid) break;
      if (nids[i] == fid) found = true;
      if (nids[i] >= fid) break;
    }
    if (!found) {
      ret += source->get_pos(id) + ":";
    }
  }
  return ret.erase(ret.size()-1);
}

/*
 *  return structure strings for each sorted word and holes
 *  holes are correspond to node replacement grammar.
 */
std::vector<std::string> G2sSentenceAlignment::SortWordsWithHoles(const GraphRange& nids,
  	std::map<int, int>& indexWord,
		bool incLabel, bool sort)
{
	vector<pair<string, int> > words;
	for(GraphRange::const_iterator iter = nids.begin(); iter != nids.end(); iter++) {
		int nid = *iter;
		string word = source->get_word(nid);
		words.push_back(make_pair<string,int>(word,nid));
	}
//	if (sort) {
//		std::stable_sort(words.begin(), words.end(), node_key);
//	}
	vector<int> sortedIndex;
	for(size_t i = 0; i < words.size(); i++) {
		sortedIndex.push_back(words[i].second);
		indexWord[words[i].second] = i;
	}
	string terminalStr = get_structure_string(sortedIndex, incLabel);

	// combine terminal and non-termimal string
	vector<string> structStrs;
	structStrs.push_back(terminalStr);

	return structStrs;
}

vector<string> G2sSentenceAlignment::get_nt_structure_string(
		const GraphRange& nids,
		const std::vector<GraphRange >& holes,
		bool incLabel, bool relaxNT)
{
	return get_nt_structure(nids, holes, incLabel, relaxNT);
}

vector<string> G2sSentenceAlignment::get_nt_structure(
		const GraphRange& nids,
		const std::vector<GraphRange >& holes,
		bool incLabel, bool relaxNT)
{

	vector<string> ret;
	int size = (int)nids.size();

	BUIImap I2J;
	for(int i = 0; i < size; i++) {
		I2J[nids[i]] = i;
	}

	NTermCache& cache = ntermCahce[nids];
	NTermCache::iterator iterCache;
	for(size_t i = 0; i < holes.size(); i++) {
		const GraphRange& nidss = holes[i];

		if ((iterCache=cache.find(nidss)) != cache.end()) {
			ret.push_back(iterCache->second);
			continue;
		}

		EdgeMap predLabels;
		EdgeMap succLabels;
		for(GraphRange::const_iterator iter = nidss.begin(); iter != nidss.end(); ++iter) {
			int nid = *iter;
			if (source->has_succ(nid)) {
				const EdgeMap& succ_edges = source->get_succ_edges(nid);
				CollectLabels(I2J, nidss, succ_edges, succLabels);
			}
			if (source->has_pred(nid)) {
				const EdgeMap& pred_edges = source->get_pred_edges(nid);
				CollectLabels(I2J, nidss, pred_edges, predLabels);
			}
		}
		string str = ConcatLabels(predLabels, incLabel) + "_" + ConcatLabels(succLabels, incLabel);
		cache[nidss] = str;
		ret.push_back(str);
	}
	return ret;
}

string G2sSentenceAlignment::ConcatLabels(const EdgeMap& labels,
		bool incLabel) const
{
	string ret = "-1";
	if (labels.size() > 0){
		string retStr = "";
		for(EdgeMap::const_iterator iter = labels.begin();
				iter != labels.end(); iter++) {
			int j = iter->first;
			const set<string>& labels = iter->second;
			for(set<string>::const_iterator it2 = labels.begin();
					it2 != labels.end(); it2++) {
				string label = *it2;
				retStr += SPrint<int>(j) + ":";
				if (incLabel)
					retStr += label + ":";
				else
					break;
			}
		}
		ret = retStr.erase(retStr.size()-1);
	}
	return ret;
}

void G2sSentenceAlignment::CollectLabels(const BUIImap& I2J,
		const GraphRange& except,
		const EdgeMap& edges,
		EdgeMap& ret) const
{
	EdgeMap::const_iterator it = edges.begin();
	for (; it != edges.end(); ++it) {
		int j = it->first;

		if (I2J.find(j) == I2J.end())
			continue;
		j = I2J.find(j)->second;
		LabelSet& labels = ret[j];
		labels.insert(it->second.begin(), it->second.end());
	}
}

vector<string> G2sSentenceAlignment::get_structure(const vector<GraphRange >& nids, bool incLabel) const {
	vector<string> ret;
	int size = (int)nids.size();

	BUIImap I2J;
	for(int i = 0; i < size; i++) {
		const GraphRange& nidss = nids[i];
		for(GraphRange::const_iterator iter = nidss.begin(); iter != nidss.end(); ++iter) {
			int nid = *iter;
			I2J[nid] = i;
		}
	}

	for(int i = 0; i < size; i++) {
		const GraphRange& nidss = nids[i];
		BUIset nidSet(nidss.begin(), nidss.end());
		EdgeMap allLabels;
		for(GraphRange::const_iterator iter = nidss.begin(); iter != nidss.end(); ++iter) {
			int nid = *iter;
			if (source->has_succ(nid)) {
				const EdgeMap& succ_edges = source->get_succ_edges(nid);
				EdgeMap::const_iterator it = succ_edges.begin();
				for (; it != succ_edges.end(); ++it) {
					int j = it->first;

					if (I2J.find(j) == I2J.end() || nidSet.find(j) != nidSet.end())
						continue;

					j = I2J.find(j)->second;
					if (allLabels.find(j) == allLabels.end())
						allLabels[j] = LabelSet();
					allLabels[j].insert(it->second.begin(), it->second.end());
				}
			}
		}
		if (allLabels.size() == 0){
			ret.push_back("-1");
		} else {
			string retStr = "";
			for(EdgeMap::const_iterator iter = allLabels.begin();
					iter != allLabels.end(); iter++) {
				int j = iter->first;
				const LabelSet& labels = iter->second;
				for(LabelSet::const_iterator it2 = labels.begin();
						it2 != labels.end(); it2++) {
					string label = *it2;
					retStr += SPrint<int>(j) + ":";
					if (incLabel)
						retStr += label + ":";
					else
						break;
				}
			}
			retStr = retStr.erase(retStr.size()-1);
			ret.push_back(retStr);
		}
	}
	return ret;
}

std::string G2sSentenceAlignment::GetBetweenLink(const GraphRange& range, const GraphRange& nextRange, bool incLabel) const
{
  string ret = "";
  set<int> nextSet(nextRange.begin(), nextRange.end());
  for (size_t i = 0; i < range.size(); i++) {
    int nid = range[i];
    const EdgeMap& succ_edges = source->get_succ_edges(nid);
    EdgeMap::const_iterator it = succ_edges.begin();
    for (; it != succ_edges.end(); ++it) {
      int j = it->first;
      if (nextSet.find(j) != nextSet.end()) {
        ret += "s"+SPrint<int>(i);
        break;
      }
    }
    const EdgeMap& pred_edges = source->get_pred_edges(nid);
    it = pred_edges.begin();
    for (; it != pred_edges.end(); ++it) {
      int j = it->first;
      if (nextSet.find(j) != nextSet.end()) {
        ret += "p"+SPrint<int>(i);
        break;
      }
    }
  }
  if (ret.empty()) return "-1";
  return ret;
}

}

