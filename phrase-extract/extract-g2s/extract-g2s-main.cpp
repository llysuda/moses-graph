/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2009 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include <algorithm>
#include <assert.h>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#ifdef WIN32
// Include Visual Leak Detector
//#include <vld.h>
#endif

#include "typedef.h"
#include "ExtractedG2sRule.h"
#include "SafeGetline.h"
#include "G2sSentenceAlignment.h"
#include "MultiDiGraph.h"
#include "tables-core.h"
#include "XmlTree.h"
#include "InputFileStream.h"
#include "OutputFileStream.h"
#include "G2sExtractionOptions.h"
#include "GraphBitmap.h"

#define LINE_MAX_LENGTH 500000

using namespace std;
using namespace MosesG2S;

namespace MosesG2S {
  typedef vector< int > LabelIndex;
  typedef map< int, int > WordIndex;


  // HPhraseVertex represents a point in the alignment matrix
  typedef pair <int, int> HPhraseVertex;

  // Phrase represents a bi-phrase; each bi-phrase is defined by two points in the alignment matrix:
  // bottom-left and top-right
  typedef pair<HPhraseVertex, HPhraseVertex> HPhrase;

  typedef vector< pair<int,GraphRange> > LabelVec;
  typedef map<int, LabelVec > HPhraseLabel;

  // HPhraseVector is a vector of HPhrases
  typedef vector < HPhrase > HPhraseVector;

  typedef vector < vector<int> > HSourceVector;

  // SentenceVertices represents, from all extracted phrases, all vertices that have the same positioning
  // The key of the map is the English index and the value is a set of the source ones
  typedef map <int, set<int> > HSentenceVertices;

  REO_POS getOrientWordModel(G2sSentenceAlignment &, REO_MODEL_TYPE, bool, bool,
                             int, int, int, int, int, int, int,
                             bool (*)(int, int), bool (*)(int, int));
  REO_POS getOrientPhraseModel(G2sSentenceAlignment &, REO_MODEL_TYPE, bool, bool,
                               int, int, int, int, int, int, int,
                               bool (*)(int, int), bool (*)(int, int),
                               const HSentenceVertices &, const HSentenceVertices &);
  REO_POS getOrientHierModel(G2sSentenceAlignment &, REO_MODEL_TYPE, bool, bool,
                             int, int, int, int, int, int, int,
                             bool (*)(int, int), bool (*)(int, int),
                             const HSentenceVertices &, const HSentenceVertices &,
                             const HSentenceVertices &, const HSentenceVertices &,
                             REO_POS);

  void insertVertex(HSentenceVertices &, int, int);
  void insertPhraseVertices(HSentenceVertices &, HSentenceVertices &, HSentenceVertices &, HSentenceVertices &,
                            int, int, int, int);
  string getOrientString(REO_POS, REO_MODEL_TYPE);

  bool ge(int, int);
  bool le(int, int);
  bool lt(int, int);

  bool isAligned (G2sSentenceAlignment &, int, int);



class ExtractTask
{
private:
  G2sSentenceAlignment &m_sentence;
  const G2sExtractionOptions &m_options;
  Moses::OutputFileStream& m_extractFile;
  Moses::OutputFileStream& m_extractFileInv;
  Moses::OutputFileStream& m_extractFileOrientation;
  Moses::OutputFileStream& m_extractFileOrder;

  vector< ExtractedG2sRule > m_extractedRules;
  HPhraseLabel m_phraseLabels;
  bool isBinary(int, int) const;

  // main functions
  void extractRules();
  void addRuleToCollection(ExtractedG2sRule &rule);
  void consolidateRules();
  void writeRulesToFile();

  // subs
  void addRule( int, int, const GraphRange&, int, const string&, const string&, const string&);

  inline string IntToString( int i ) {
    stringstream out;
    out << i;
    return out.str();
  }

  string GetOrientationString(int startF, int endF, int startE, int endE, int countS) const;

public:
  ExtractTask(G2sSentenceAlignment &sentence, const G2sExtractionOptions &options, Moses::OutputFileStream &extractFile,
      Moses::OutputFileStream &extractFileInv, Moses::OutputFileStream &extractFileOrientation, Moses::OutputFileStream &extractFileOrder):
    m_sentence(sentence),
    m_options(options),
    m_extractFile(extractFile),
    m_extractFileInv(extractFileInv),
    m_extractFileOrientation(extractFileOrientation),
    m_extractFileOrder(extractFileOrder){}
  void Run();

  HSentenceVertices inTopLeft;
  HSentenceVertices inTopRight;
  HSentenceVertices inBottomLeft;
  HSentenceVertices inBottomRight;

  HSentenceVertices outTopLeft;
  HSentenceVertices outTopRight;
  HSentenceVertices outBottomLeft;
  HSentenceVertices outBottomRight;

};

};

int main(int argc, char* argv[])
{
  cerr << "extract-g2s, written by Liangyou Li, midified from Moses\n"
       << "rule extraction from an aligned parallel corpus\n";

  G2sExtractionOptions options;
  int sentenceOffset = 0;
#ifdef WITH_THREADS
  int thread_count = 1;
#endif
  if (argc < 5) {
    cerr << "syntax: extract-g2s corpus.target corpus.source corpus.align extract ["

         << " --GlueGrammar FILE"
         << " | --UnknownWordLabel FILE"
         << " | --OnlyDirect"
         << " | --OutputNTLengths"
         << " | --MaxSpan[" << options.maxSpan << "]"
         << " | --MaxSymbolsTarget[" << options.maxSymbolsTarget << "]"
         << " | --MaxSymbolsSource[" << options.maxSymbolsSource << "]"
         << " | --BoundaryRules[" << options.boundaryRules << "]";

    exit(1);
  }
  char* &fileNameT = argv[1];
  char* &fileNameS = argv[2];
  char* &fileNameA = argv[3];
  string fileNameGlueGrammar;
  string fileNameUnknownWordLabel;
  string fileNameExtract = string(argv[4]);

  int optionInd = 5;

  for(int i=optionInd; i<argc; i++) {
    // maximum span length
    if (strcmp(argv[i],"--MaxSpan") == 0) {
      options.maxSpan = atoi(argv[++i]);
      if (options.maxSpan < 1) {
        cerr << "extract error: --maxSpan should be at least 1" << endl;
        exit(1);
      }
//    // maximum number of words in hierarchical phrase
    } else if (strcmp(argv[i],"--MaxSymbolsTarget") == 0) {
      options.maxSymbolsTarget = atoi(argv[++i]);
      if (options.maxSymbolsTarget < 1) {
        cerr << "extract error: --MaxSymbolsTarget should be at least 1" << endl;
        exit(1);
      }
    }
    // maximum number of words in hierarchical phrase
    else if (strcmp(argv[i],"--MaxSymbolsSource") == 0) {
      options.maxSymbolsSource = atoi(argv[++i]);
      if (options.maxSymbolsSource < 1) {
        cerr << "extract error: --MaxSymbolsSource should be at least 1" << endl;
        exit(1);
      }
    } else if (strcmp(argv[i], "--GZOutput") == 0) {
      options.gzOutput = true;
    } else if (strcmp(argv[i],"--OnlyOutputSpanInfo") == 0) {
      options.onlyOutputSpanInfo = true;
    } else if (strcmp(argv[i],"--OnlyDirect") == 0) {
      options.onlyDirectFlag = true;
    // if an source phrase is paired with two target phrases, then count(t|s) = 0.5
    } else if (strcmp(argv[i],"--FractionalCounting") == 0) {
      options.fractionalCounting = true;
    } else if (strcmp(argv[i],"--UnpairedExtractFormat") == 0) {
      options.unpairedExtractFormat = true;
    } else if (strcmp(argv[i],"--ConditionOnTargetLHS") == 0) {
      options.conditionOnTargetLhs = true;
    } else if (strcmp(argv[i],"-threads") == 0 ||
               strcmp(argv[i],"--threads") == 0 ||
               strcmp(argv[i],"--Threads") == 0) {
#ifdef WITH_THREADS
      thread_count = atoi(argv[++i]);
#else
      cerr << "thread support not compiled in." << '\n';
      exit(1);
#endif
    } else if (strcmp(argv[i], "--SentenceOffset") == 0) {
      if (i+1 >= argc || argv[i+1][0] < '0' || argv[i+1][0] > '9') {
        cerr << "extract: syntax error, used switch --SentenceOffset without a number" << endl;
        exit(1);
      }
      sentenceOffset = atoi(argv[++i]);
    } else if (strcmp(argv[i],"--BoundaryRules") == 0) {
      options.boundaryRules = true;
    } else if (strcmp(argv[i],"--NoTranslation") == 0) {
      options.translationFlag = false;
    } else if (strcmp(argv[i],"--IncLabel") == 0) {
      options.incLabel = true;
    } else if (strcmp(argv[i],"orientation") == 0 || strcmp(argv[i],"--Orientation") == 0) {
      options.orientationFlag = true;
    } else if(strcmp(argv[i],"--model") == 0) {
      if (i+1 >= argc) {
        cerr << "extract: syntax error, no model's information provided to the option --model " << endl;
        exit(1);
      }
      char*  modelParams = argv[++i];
      char*  modelName = strtok(modelParams, "-");
      char*  modelType = strtok(NULL, "-");

      // REO_MODEL_TYPE intModelType;

      if(strcmp(modelName, "wbe") == 0) {
        options.wordModel = true;
        if(strcmp(modelType, "msd") == 0)
          options.wordType = REO_MSD;
        else if(strcmp(modelType, "mslr") == 0)
          options.wordType = REO_MSLR;
        else if(strcmp(modelType, "mslro") == 0)
          options.wordType = REO_MSLRO;
        else if(strcmp(modelType, "mono") == 0 || strcmp(modelType, "monotonicity") == 0)
          options.wordType = REO_MONO;
        else {
          cerr << "extract: syntax error, unknown reordering model type: " << modelType << endl;
          exit(1);
        }
      } else if(strcmp(modelName, "phrase") == 0) {
        options.phraseModel = true;
        if(strcmp(modelType, "msd") == 0)
          options.phraseType = REO_MSD;
        else if(strcmp(modelType, "mslr") == 0)
          options.phraseType = REO_MSLR;
        else if(strcmp(modelType, "mslro") == 0)
          options.phraseType = REO_MSLRO;
        else if(strcmp(modelType, "mono") == 0 || strcmp(modelType, "monotonicity") == 0)
          options.phraseType = REO_MONO;
        else {
          cerr << "extract: syntax error, unknown reordering model type: " << modelType << endl;
          exit(1);
        }
      } else if(strcmp(modelName, "hier") == 0) {
        options.hierModel = true;
        if(strcmp(modelType, "msd") == 0)
          options.hierType = REO_MSD;
        else if(strcmp(modelType, "mslr") == 0)
          options.hierType = REO_MSLR;
        else if(strcmp(modelType, "mslro") == 0)
          options.hierType = REO_MSLRO;
        else if(strcmp(modelType, "mono") == 0 || strcmp(modelType, "monotonicity") == 0)
          options.hierType = REO_MONO;
        else {
          cerr << "extract: syntax error, unknown reordering model type: " << modelType << endl;
          exit(1);
        }
      } else {
        cerr << "extract: syntax error, unknown reordering model: " << modelName << endl;
        exit(1);
      }

      options.allModelsOutputFlag = true;
    } else {
      cerr << "extract: syntax error, unknown option '" << string(argv[i]) << "'\n";
      exit(1);
    }
  }

  cerr << "extracting context-aware rules" << endl;

  // open input files
  Moses::InputFileStream tFile(fileNameT);
  Moses::InputFileStream sFile(fileNameS);
  Moses::InputFileStream aFile(fileNameA);

  istream *tFileP = &tFile;
  istream *sFileP = &sFile;
  istream *aFileP = &aFile;

  // open output files
  string fileNameExtractInv = fileNameExtract + ".inv" + (options.gzOutput?".gz":"");
  Moses::OutputFileStream extractFile;
  Moses::OutputFileStream extractFileInv;
  extractFile.Open((fileNameExtract  + (options.gzOutput?".gz":"")).c_str());
  if (!options.onlyDirectFlag)
    extractFileInv.Open(fileNameExtractInv.c_str());

  Moses::OutputFileStream extractFileOrientation;
  if (options.orientationFlag) {
    string fileNameExtractOrientation = fileNameExtract + ".o" + (options.gzOutput?".gz":"");
    extractFileOrientation.Open(fileNameExtractOrientation.c_str());
  }

  Moses::OutputFileStream extractFileOrder;

  // stats on labels for glue grammar and unknown word label probabilities
  set< string > targetLabelCollection, sourceLabelCollection;
  map< string, int > targetTopLabelCollection, sourceTopLabelCollection;

  // loop through all sentence pairs
  size_t i=sentenceOffset;
  while(true) {
    i++;
    if (i%1000 == 0) cerr << i << " " << flush;

    char targetString[LINE_MAX_LENGTH];
    char sourceString[LINE_MAX_LENGTH];
    char alignmentString[LINE_MAX_LENGTH];
    SAFE_GETLINE((*tFileP), targetString, LINE_MAX_LENGTH, '\n', __FILE__);
    if (tFileP->eof()) break;
    SAFE_GETLINE((*sFileP), sourceString, LINE_MAX_LENGTH, '\n', __FILE__);
    SAFE_GETLINE((*aFileP), alignmentString, LINE_MAX_LENGTH, '\n', __FILE__);

    G2sSentenceAlignment sentence;
    //az: output src, tgt, and alingment line
    if (options.onlyOutputSpanInfo) {
      cout << "LOG: SRC: " << sourceString << endl;
      cout << "LOG: TGT: " << targetString << endl;
      cout << "LOG: ALT: " << alignmentString << endl;
      cout << "LOG: PHRASES_BEGIN:" << endl;
    }

    if (sentence.create(targetString, sourceString, alignmentString,"", i, options)) {
      ExtractTask *task = new ExtractTask(sentence, options, extractFile, extractFileInv, extractFileOrientation, extractFileOrder);
      task->Run();
      delete task;
    }
    if (options.onlyOutputSpanInfo) cout << "LOG: PHRASES_END:" << endl; //az: mark end of phrases
  }

  tFile.Close();
  sFile.Close();
  aFile.Close();
  // only close if we actually opened it
  if (!options.onlyOutputSpanInfo) {
    extractFile.Close();
    if (!options.onlyDirectFlag) extractFileInv.Close();
  }

  if (options.orientationFlag) {
    extractFileOrientation.Close();
  }

}


namespace MosesG2S {

void ExtractTask::Run()
{
  extractRules();
  consolidateRules();
  writeRulesToFile();
  m_extractedRules.clear();
}

void ExtractTask::extractRules()
{
  int countT = m_sentence.target.size();
  int countS = m_sentence.source->size();


  HPhraseVector inboundPhrases;
  HSourceVector inboundSources;


  HSentenceVertices::const_iterator it;

  bool relaxLimit = m_options.hierModel;
  bool buildExtraStructure = true;

  GraphBitmap nidSet(countS);
  GraphBitmap subtractId(countS);

  // check alignments for target phrase startT...endT
  for(int lengthT=1;
      (relaxLimit || lengthT <= m_options.maxSpan) && lengthT <= countT;
      lengthT++) {
    for(int startT=0; startT < countT-(lengthT-1); startT++) {

      // that's nice to have
      int endT = startT + lengthT - 1;

      BUIset usedF;
      vector< int > usedFAlign = m_sentence.alignedCountS;
      for(int ei=startT; ei<=endT; ei++) {
        for(size_t i=0; i<m_sentence.alignedToT[ei].size(); i++) {
          int fi = m_sentence.alignedToT[ei][i];
          usedF.insert(fi);
          usedFAlign[ fi ]--;
        }
      }

      int usedF_size = (int)usedF.size();
      if (usedF_size == 0 || (!relaxLimit && usedF_size > m_options.maxSpan))
          continue;

      // make sure usedF does not align to outside
      bool out_of_bounds = false;
      for(BUIset::const_iterator iter = usedF.begin(); iter != usedF.end(); ++iter) {
      	if (usedFAlign[*iter] > 0) {
      		out_of_bounds = true;
      		break;
      	}
      }
      if (out_of_bounds)
      	continue;

			// make sure usedF does not aligned to outside

      vector< GraphRange > queue;
      queue.reserve(10000);
      BUVIset seen;
      GraphRange initGR(GraphRange(usedF.begin(), usedF.end()));
      std::sort(initGR.begin(), initGR.end());
      queue.push_back(initGR);

      while (queue.size()>0) {
        GraphRange base = queue.back();
        queue.pop_back();

        if (seen.find(base) != seen.end())
            continue;
        seen.insert(base);

        for(size_t i = 1; i < base.size(); i++)
          if (base[i] <= base[i-1]) {
            cerr << "error!" << endl;
            exit(0);
          }


        int base_size = (int)base.size();
        int startS = *base.begin();
        int endS = *base.rbegin();

        bool isConnected = m_sentence.source->is_connected(base);

        if (endT-startT < m_options.maxSpan
            && base_size <= m_options.maxSpan
            && (true || isConnected || isBinary(startT, endT))) {
          int point = startT;
          if (m_phraseLabels.find(point) == m_phraseLabels.end())
            m_phraseLabels[point] = vector< pair<int,GraphRange> >();
          m_phraseLabels[point].push_back(make_pair(endT,base));
        }

        if (isConnected) {

          if(endT-startT < m_options.maxSpan && base.size() <= m_options.maxSpan) { // within limit
            inboundPhrases.push_back(HPhrase(HPhraseVertex(startS,startT),
                                             HPhraseVertex(endS,endT)));
            inboundSources.push_back(base);
            insertPhraseVertices(inTopLeft, inTopRight, inBottomLeft, inBottomRight,
                                 startS, startT, endS, endT);
          } else
            insertPhraseVertices(outTopLeft, outTopRight, outBottomLeft, outBottomRight,
                                 startS, startT, endS, endT);
        }
        // grow source with unaligned words
        if (!relaxLimit && base_size >= m_options.maxSpan)
            continue;
        GraphRange unalignedF_set = m_sentence.get_unalignedF(base);

        if (relaxLimit && base_size >= m_options.maxSpan && unalignedF_set.size()>2) {
          GraphRange newUnalign;
          if (unalignedF_set.front() < base.front())
            newUnalign.push_back(unalignedF_set.front());
          if (unalignedF_set.back() > base.back())
            newUnalign.push_back(unalignedF_set.back());
          unalignedF_set = newUnalign;
        }

        for(GraphRange::const_iterator iter = unalignedF_set.begin(); iter != unalignedF_set.end(); ++iter) {
          GraphRange nids(base);
          size_t ni = 0;
          for (; ni < base.size(); ni++) {
            if (*iter < base[ni]) {
              nids.insert(nids.begin()+ni,*iter);
              break;
            }
          }
          if (ni == base.size())
            nids.push_back(*iter);

          queue.push_back(nids);
        }
      }
    } // end startT
  } // end length

  if(buildExtraStructure) { // phrase || hier
    string orientationInfo = "";
//    REO_POS wordPrevOrient, wordNextOrient, phrasePrevOrient, phraseNextOrient, hierPrevOrient, hierNextOrient;

    for(size_t i = 0; i < inboundPhrases.size(); i++) {
      int startF = inboundPhrases[i].first.first;
      int startE = inboundPhrases[i].first.second;
      int endF = inboundPhrases[i].second.first;
      int endE = inboundPhrases[i].second.second;

      const vector<int>& base = inboundSources[i];

      orientationInfo = GetOrientationString(startF, endF, startE, endE, countS);

      if (endE-startE < m_options.maxSymbolsTarget && base.size() <= m_options.maxSymbolsSource) {
          int startPoint = endE+1;
          HPhraseLabel::const_iterator iterLabel = m_phraseLabels.find(startPoint);
          if (iterLabel != m_phraseLabels.end()) {
            const vector< pair<int, GraphRange> >& labels = iterLabel->second;
            for (vector< pair<int,GraphRange> >::const_iterator it=labels.begin(); it != labels.end(); ++it) {
              int endPoint = it->first;
              const GraphRange nextRange = it->second;
              if (nextRange.size() + base.size() > m_options.maxSpan || endPoint-startE >= m_options.maxSpan)
                continue;
              string flabel = "X#";
              string order = "";
              string betlink = m_sentence.GetBetweenLink(base, nextRange, m_options.incLabel);
              flabel += betlink;
              addRule(startE,endE, base, countS, order, "X", flabel);
            }
          }
          addRule(startE,endE, base, countS,orientationInfo, "X", "X#X");
      }

    }
  }
}

bool ExtractTask::isBinary(int startT, int endT) const
{
  HPhraseLabel::const_iterator iter = m_phraseLabels.find(startT);
  if (iter == m_phraseLabels.end())
    return false;
  const LabelVec& labels = iter->second;
  for(LabelVec::const_reverse_iterator iter2 = labels.rbegin();
      iter2 != labels.rend(); ++iter2) {
    int end = iter2->first;
    if (end > endT) continue;
    if (end == endT) return true;
    if (isBinary(end+1, endT))
      return true;
  }
  return false;
}

string ExtractTask::GetOrientationString(int startF, int endF, int startE, int endE, int countS) const {

  string orientationInfo = "";
  REO_POS wordPrevOrient, wordNextOrient, phrasePrevOrient, phraseNextOrient, hierPrevOrient, hierNextOrient;
  bool connectedLeftTopP  = isAligned( m_sentence, startF-1, startE-1 );
  bool connectedRightTopP = isAligned( m_sentence, endF+1,   startE-1 );
  bool connectedLeftTopN  = isAligned( m_sentence, endF+1, endE+1 );
  bool connectedRightTopN = isAligned( m_sentence, startF-1,   endE+1 );

  if(m_options.wordModel) {
    wordPrevOrient = getOrientWordModel(m_sentence, m_options.wordType,
                                        connectedLeftTopP, connectedRightTopP,
                                        startF, endF, startE, endE, countS, 0, 1,
                                        &ge, &lt);
    wordNextOrient = getOrientWordModel(m_sentence, m_options.wordType,
                                        connectedLeftTopN, connectedRightTopN,
                                        endF, startF, endE, startE, 0, countS, -1,
                                        &lt, &ge);
  }
  if (m_options.phraseModel) {
    phrasePrevOrient = getOrientPhraseModel(m_sentence, m_options.phraseType,
                                            connectedLeftTopP, connectedRightTopP,
                                            startF, endF, startE, endE, countS-1, 0, 1, &ge, &lt, inBottomRight, inBottomLeft);
    phraseNextOrient = getOrientPhraseModel(m_sentence, m_options.phraseType,
                                            connectedLeftTopN, connectedRightTopN,
                                            endF, startF, endE, startE, 0, countS-1, -1, &lt, &ge, inBottomLeft, inBottomRight);
  } else {
    phrasePrevOrient = phraseNextOrient = UNKNOWN;
  }
  if(m_options.hierModel) {
    hierPrevOrient = getOrientHierModel(m_sentence, m_options.hierType,
                                        connectedLeftTopP, connectedRightTopP,
                                        startF, endF, startE, endE, countS-1, 0, 1, &ge, &lt, inBottomRight, inBottomLeft, outBottomRight, outBottomLeft, phrasePrevOrient);
    hierNextOrient = getOrientHierModel(m_sentence, m_options.hierType,
                                        connectedLeftTopN, connectedRightTopN,
                                        endF, startF, endE, startE, 0, countS-1, -1, &lt, &ge, inBottomLeft, inBottomRight, outBottomLeft, outBottomRight, phraseNextOrient);
  }

  orientationInfo = ((m_options.wordModel)? getOrientString(wordPrevOrient, m_options.wordType) + " " + getOrientString(wordNextOrient, m_options.wordType) : "") + " | " +
                    ((m_options.phraseModel)? getOrientString(phrasePrevOrient, m_options.phraseType) + " " + getOrientString(phraseNextOrient, m_options.phraseType) : "") + " | " +
                    ((m_options.hierModel)? getOrientString(hierPrevOrient, m_options.hierType) + " " + getOrientString(hierNextOrient, m_options.hierType) : "");
  return orientationInfo;
}



void ExtractTask::addRule( int startT, int endT, const GraphRange& nids, int countS, const string& orientationStr, const string& plabel, const string& flabel)
{

  if (m_options.onlyOutputSpanInfo) {
    cout << nids.size() << " " << startT << " " << endT << endl;
    return;
  }

  ExtractedG2sRule rule(startT, endT, nids, nids);
  rule.orientation = orientationStr;

  // phrase labels
  string targetLabel,sourceLabel;
  sourceLabel = flabel;
  targetLabel = "X";

  //
  vector<pair<string,int> > wordsF;
	m_sentence.get_sorted_words(nids,wordsF);
	GraphRange sorted_nids;
	for(size_t i = 0; i < wordsF.size(); i++) {
		sorted_nids.push_back(wordsF[i].second);
	}
	string structStr = m_sentence.get_structure_string(sorted_nids, m_options.incLabel);

  // source
  rule.source = "";
  for(int fi=0; fi<(int)wordsF.size(); fi++)
    rule.source += wordsF[fi].first + " ";

  rule.reoSource = rule.source;

  rule.source += "[" + sourceLabel + "@" + structStr + "]";
  rule.reoSource += "[" + sourceLabel + "@" + structStr + "]";;//"[X#X@" + structStr + "]";

  // target
  rule.target = m_sentence.target[startT];
  for(int ti=startT+1; ti<=endT; ti++)
    rule.target += " " + m_sentence.target[ti];

  rule.reoTarget = rule.target;
  rule.reoTarget = rule.reoTarget.erase(rule.reoTarget.size()-1);


  map<int,int> alignMap;
	for(int i = 0; i < (int)wordsF.size(); i++) {
		alignMap[wordsF[i].second] = i;
	}
  // alignment
  for(int ti=startT; ti<=endT; ti++) {
    for(unsigned int i=0; i<m_sentence.alignedToT[ti].size(); i++) {
      int si = m_sentence.alignedToT[ti][i];
      std::string sourceSymbolIndex = IntToString(alignMap[si]);
      std::string targetSymbolIndex = IntToString(ti-startT);
      rule.alignment += sourceSymbolIndex + "-" + targetSymbolIndex + " ";
      if (!m_options.onlyDirectFlag)
        rule.alignmentInv += targetSymbolIndex + "-" + sourceSymbolIndex + " ";
    }
  }

  rule.alignment.erase(rule.alignment.size()-1);
  if (!m_options.onlyDirectFlag)
    rule.alignmentInv.erase(rule.alignmentInv.size()-1);

  addRuleToCollection( rule );
}

void ExtractTask::addRuleToCollection( ExtractedG2sRule &newRule )
{

  // no double-counting of identical rules from overlapping spans
  if (!m_options.duplicateRules) {
    vector<ExtractedG2sRule>::const_iterator rule;
    for(rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
      if (rule->source.compare( newRule.source ) == 0 &&
          rule->target.compare( newRule.target ) == 0 &&
          !(rule->endT < newRule.startT || rule->startT > newRule.endT)) { // overlapping
        return;
      }
    }
  }
  m_extractedRules.push_back( newRule );
}

void ExtractTask::consolidateRules()
{
  typedef vector<ExtractedG2sRule>::iterator R;
  map<int, map<int, map<vector<int>,int > > > spanCount;

  // compute number of rules per span
  if (m_options.fractionalCounting) {
    for(R rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
      spanCount[ rule->startT ][ rule->endT ][ rule->fnids ]++;
    }
  }

  // compute fractional counts
  for(R rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
    rule->count =    1.0/(float) (m_options.fractionalCounting ? spanCount[ rule->startT ][ rule->endT ][ rule->fnids ] : 1.0 );
  }

  // consolidate counts
  map<std::string, map< std::string, map< std::string, float> > > consolidatedCount;
  for(R rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
    consolidatedCount[ rule->source ][ rule->target][ rule->alignment ] += rule->count;
  }

  for(R rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
    float count = consolidatedCount[ rule->source ][ rule->target][ rule->alignment ];
    rule->count = count;
    consolidatedCount[ rule->source ][ rule->target][ rule->alignment ] = 0;
  }
}

void ExtractTask::writeRulesToFile()
{
  vector<ExtractedG2sRule>::const_iterator rule;
  ostringstream out;
  ostringstream outInv;
  ostringstream outOri;
  ostringstream outOrd;
  for(rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
    if (rule->count == 0)
      continue;

    out << rule->source << " ||| "
        << rule->target << " ||| "
        << rule->alignment << " ||| "
        << rule->count << " ||| ";
//    if (m_options.outputNTLengths) {
//      rule->OutputNTLengths(out);
//    }
//    if (m_options.pcfgScore) {
//      out << " ||| " << rule->pcfgScore;
//    }
    out << "\n";

    if (!m_options.onlyDirectFlag) {
      outInv << rule->target << " ||| "
             << rule->source << " ||| "
             << rule->alignmentInv << " ||| "
             << rule->count << "\n";
    }

    if (m_options.orientationFlag && !rule->orientation.empty()) {
        outOri << rule->reoSource << " ||| "
              << rule->reoTarget << " ||| "
              << rule->orientation << endl;
    }
  }
  if (m_options.translationFlag) {
    m_extractFile << out.str();
    m_extractFileInv << outInv.str();
  }
  if (m_options.orientationFlag) {
    m_extractFileOrientation << outOri.str();
  }
}



inline std::vector<std::string> Tokenize(const std::string& str,
    const std::string& delimiters = " \t")
{
  std::vector<std::string> tokens;
  // Skip delimiters at beginning.
  std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  // Find first "non-delimiter".
  std::string::size_type pos     = str.find_first_of(delimiters, lastPos);

  while (std::string::npos != pos || std::string::npos != lastPos) {
    // Found a token, add it to the vector.
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    // Skip delimiters.  Note the "not_of"
    lastPos = str.find_first_not_of(delimiters, pos);
    // Find next "non-delimiter"
    pos = str.find_first_of(delimiters, lastPos);
  }

  return tokens;
}

REO_POS getOrientWordModel(G2sSentenceAlignment & sentence, REO_MODEL_TYPE modelType,
                           bool connectedLeftTop, bool connectedRightTop,
                           int startF, int endF, int startE, int endE, int countF, int zero, int unit,
                           bool (*ge)(int, int), bool (*lt)(int, int) )
{

  if( connectedLeftTop && !connectedRightTop)
    return LEFT;
  if(modelType == REO_MONO)
    return UNKNOWN;
  if (!connectedLeftTop &&  connectedRightTop)
    return RIGHT;
  if(modelType == REO_MSD)
    return UNKNOWN;
  for(int indexF=startF-2*unit; (*ge)(indexF, zero) && !connectedLeftTop; indexF=indexF-unit)
    connectedLeftTop = isAligned(sentence, indexF, startE-unit);
  for(int indexF=endF+2*unit; (*lt)(indexF,countF) && !connectedRightTop; indexF=indexF+unit)
    connectedRightTop = isAligned(sentence, indexF, startE-unit);
  if(connectedLeftTop && !connectedRightTop)
    return DRIGHT;
  else if(!connectedLeftTop && connectedRightTop)
    return DLEFT;
  // for REO_MSLRO
  if (modelType == REO_MSLR)
    return UNKNOWN;
  int realSF = startF <= endF ? startF : endF;
  int realEF = startF <= endF ? endF : startF;
  for(int indexF=realSF; indexF <= realEF; indexF=indexF+1) {
    if (isAligned(sentence, indexF, startE-unit)) {
      return OVERLAP;
    }
  }
  // end REO_MSLRO
  return UNKNOWN;
}

// to be called with countF-1 instead of countF
REO_POS getOrientPhraseModel (G2sSentenceAlignment & sentence, REO_MODEL_TYPE modelType,
                              bool connectedLeftTop, bool connectedRightTop,
                              int startF, int endF, int startE, int endE, int countF, int zero, int unit,
                              bool (*ge)(int, int), bool (*lt)(int, int),
                              const HSentenceVertices & inBottomRight, const HSentenceVertices & inBottomLeft)
{

  HSentenceVertices::const_iterator it;

  if((connectedLeftTop && !connectedRightTop) ||
      //(startE == 0 && startF == 0) ||
      //(startE == sentence.target.size()-1 && startF == sentence.source.size()-1) ||
      ((it = inBottomRight.find(startE - unit)) != inBottomRight.end() &&
       it->second.find(startF-unit) != it->second.end()))
    return LEFT;
  if(modelType == REO_MONO)
    return UNKNOWN;
  if((!connectedLeftTop &&  connectedRightTop) ||
      ((it = inBottomLeft.find(startE - unit)) != inBottomLeft.end() && it->second.find(endF + unit) != it->second.end()))
    return RIGHT;
  if(modelType == REO_MSD)
    return UNKNOWN;
  connectedLeftTop = false;
  for(int indexF=startF-2*unit; (*ge)(indexF, zero) && !connectedLeftTop; indexF=indexF-unit)
    if(connectedLeftTop = (it = inBottomRight.find(startE - unit)) != inBottomRight.end() &&
                          it->second.find(indexF) != it->second.end())
      return DRIGHT;
  connectedRightTop = false;
  for(int indexF=endF+2*unit; (*lt)(indexF, countF) && !connectedRightTop; indexF=indexF+unit)
    if(connectedRightTop = (it = inBottomLeft.find(startE - unit)) != inBottomLeft.end() &&
                           it->second.find(indexF) != it->second.end())
      return DLEFT;
  // for REO_MSLRO
  if (modelType == REO_MSLR)
    return UNKNOWN;
  int realSF = startF <= endF ? startF : endF;
  int realEF = startF <= endF ? endF : startF;
  for(int indexF=realSF; indexF <= realEF; indexF=indexF+1) {
    if (((it = inBottomLeft.find(startE - unit)) != inBottomLeft.end() &&
        it->second.find(indexF) != it->second.end()) ||
        ((it = inBottomRight.find(startE - unit)) != inBottomRight.end() &&
        it->second.find(indexF) != it->second.end())) {
      return OVERLAP;
    }
  }
  // end REO_MSLRO
  return UNKNOWN;
}

// to be called with countF-1 instead of countF
REO_POS getOrientHierModel (G2sSentenceAlignment & sentence, REO_MODEL_TYPE modelType,
                            bool connectedLeftTop, bool connectedRightTop,
                            int startF, int endF, int startE, int endE, int countF, int zero, int unit,
                            bool (*ge)(int, int), bool (*lt)(int, int),
                            const HSentenceVertices & inBottomRight, const HSentenceVertices & inBottomLeft,
                            const HSentenceVertices & outBottomRight, const HSentenceVertices & outBottomLeft,
                            REO_POS phraseOrient)
{

  HSentenceVertices::const_iterator it;

  if(phraseOrient == LEFT ||
      (connectedLeftTop && !connectedRightTop) ||
      //    (startE == 0 && startF == 0) ||
      //(startE == sentence.target.size()-1 && startF == sentence.source.size()-1) ||
      ((it = inBottomRight.find(startE - unit)) != inBottomRight.end() &&
       it->second.find(startF-unit) != it->second.end()) ||
      ((it = outBottomRight.find(startE - unit)) != outBottomRight.end() &&
       it->second.find(startF-unit) != it->second.end()))
    return LEFT;
  if(modelType == REO_MONO)
    return UNKNOWN;
  if(phraseOrient == RIGHT ||
      (!connectedLeftTop &&  connectedRightTop) ||
      ((it = inBottomLeft.find(startE - unit)) != inBottomLeft.end() &&
       it->second.find(endF + unit) != it->second.end()) ||
      ((it = outBottomLeft.find(startE - unit)) != outBottomLeft.end() &&
       it->second.find(endF + unit) != it->second.end()))
    return RIGHT;
  if(modelType == REO_MSD)
    return UNKNOWN;
  if(phraseOrient != UNKNOWN)
    return phraseOrient;
  connectedLeftTop = false;
  for(int indexF=startF-2*unit; (*ge)(indexF, zero) && !connectedLeftTop; indexF=indexF-unit) {
    if((connectedLeftTop = (it = inBottomRight.find(startE - unit)) != inBottomRight.end() &&
                           it->second.find(indexF) != it->second.end()) ||
        (connectedLeftTop = (it = outBottomRight.find(startE - unit)) != outBottomRight.end() &&
                            it->second.find(indexF) != it->second.end()))
      return DRIGHT;
  }
  connectedRightTop = false;
  for(int indexF=endF+2*unit; (*lt)(indexF, countF) && !connectedRightTop; indexF=indexF+unit) {
    if((connectedRightTop = (it = inBottomLeft.find(startE - unit)) != inBottomLeft.end() &&
                            it->second.find(indexF) != it->second.end()) ||
        (connectedRightTop = (it = outBottomLeft.find(startE - unit)) != outBottomLeft.end() &&
                             it->second.find(indexF) != it->second.end()))
      return DLEFT;
  }
  // for REO_MSLRO
  if (modelType == REO_MSLR)
    return UNKNOWN;
  int realSF = startF <= endF ? startF : endF;
  int realEF = startF <= endF ? endF : startF;
  for(int indexF=realSF; indexF <= realEF; indexF=indexF+1) {
    if (((it = inBottomLeft.find(startE - unit)) != inBottomLeft.end() &&
        it->second.find(indexF) != it->second.end()) ||
        ((it = inBottomRight.find(startE - unit)) != inBottomRight.end() &&
        it->second.find(indexF) != it->second.end()) ||
        ((it = outBottomLeft.find(startE - unit)) != outBottomLeft.end() &&
        it->second.find(indexF) != it->second.end()) ||
        ((it = outBottomRight.find(startE - unit)) != outBottomRight.end() &&
        it->second.find(indexF) != it->second.end())) {
      return OVERLAP;
    }
  }
  // end REO_MSLRO
  return UNKNOWN;
}

bool isAligned ( G2sSentenceAlignment &sentence, int fi, int ei )
{
  if (ei == -1 && fi == -1)
    return true;
  if (ei <= -1 || fi <= -1)
    return false;
  if ((size_t)ei == sentence.target.size() && fi == sentence.source->size())
    return true;
  if ((size_t)ei >= sentence.target.size() || fi >= sentence.source->size())
    return false;
  for(size_t i=0; i<sentence.alignedToT[ei].size(); i++)
    if (sentence.alignedToT[ei][i] == fi)
      return true;
  return false;
}

bool ge(int first, int second)
{
  return first >= second;
}

bool le(int first, int second)
{
  return first <= second;
}

bool lt(int first, int second)
{
  return first < second;
}

void insertVertex( HSentenceVertices & corners, int x, int y )
{
  set<int> tmp;
  tmp.insert(x);
  pair< HSentenceVertices::iterator, bool > ret = corners.insert( pair<int, set<int> > (y, tmp) );
  if(ret.second == false) {
    ret.first->second.insert(x);
  }
}

void insertPhraseVertices(
  HSentenceVertices & topLeft,
  HSentenceVertices & topRight,
  HSentenceVertices & bottomLeft,
  HSentenceVertices & bottomRight,
  int startF, int startE, int endF, int endE)
{

  insertVertex(topLeft, startF, startE);
  insertVertex(topRight, endF, startE);
  insertVertex(bottomLeft, startF, endE);
  insertVertex(bottomRight, endF, endE);
}

string getOrientString(REO_POS orient, REO_MODEL_TYPE modelType)
{
  switch(orient) {
  case LEFT:
    return "mono";
    break;
  case RIGHT:
    return "swap";
    break;
  case DRIGHT:
    return "dright";
    break;
  case DLEFT:
    return "dleft";
    break;
  case OVERLAP:
    return "overlap";
    break;
  case UNKNOWN:
    switch(modelType) {
    case REO_MONO:
      return "nomono";
      break;
    case REO_MSD:
      return "other";
      break;
    case REO_MSLR:
      return "dright";
      break;
    case REO_MSLRO:
      return "overlap";
      break;
    }
    break;
  }
  return "";
}

};
