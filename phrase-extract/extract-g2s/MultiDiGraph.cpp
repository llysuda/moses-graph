/*
 * DepNgramGraph.cpp
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

#include <algorithm>
#include <iostream>
#include "MultiDiGraph.h"

using namespace std;

namespace MosesG2S {

void MultiDiGraph::add_node(int id, const struct Node* attr) {
	// no duplicate node
	nodes[id] = attr;
}

void MultiDiGraph::add_edge(int u, int v, const std::string& label) {
	// add to succ
	if (succ[u].find(v) == succ[u].end()) {
		succ[u][v] = LabelSet();
	}
	succ[u][v].insert(label);
	// add to pred
	if (pred[v].find(u) == pred[v].end()) {
		pred[v][u] = LabelSet();
	}
	pred[v][u].insert(label);
}

void MultiDiGraph::DepthFirst(int curr_nid,
		BUIset& old_nids,
		const BUIset& all_nids) const {

  EdgeMap::const_iterator iter;
  const EdgeMap& sem = succ[curr_nid];
  if (sem.size() > 0) {
    for(iter=sem.begin(); iter!=sem.end(); iter++) {
      int sid = iter->first;
      if (all_nids.find(sid) != all_nids.end()
          && old_nids.find(sid) == old_nids.end()) {
        old_nids.insert(sid);
        DepthFirst(sid, old_nids, all_nids);
      }
    }
  }
  const EdgeMap&  pem = pred[curr_nid];
  if (pem.size() > 0) {
    for(iter=pem.begin(); iter!=pem.end(); iter++) {
      int pid = iter->first;
      if (all_nids.find(pid) != all_nids.end()
          && old_nids.find(pid) == old_nids.end()) {
        old_nids.insert(pid);
        DepthFirst(pid, old_nids, all_nids);
      }
    }
  }
}

bool MultiDiGraph::is_connected(const BUIset& nids) {
    BUIset old_nids;
    int curr_nid = *nids.begin();
    old_nids.insert(curr_nid);
    DepthFirst(curr_nid, old_nids, nids);
    return old_nids.size() == nids.size();
}

bool MultiDiGraph::is_connected(const GraphRange& nids) {
  size_t size = nids.size()-1;
  ConnectCacheType::iterator iter = cct[size].find(nids);
  if (iter != cct[size].end())
    return iter->second;

  BUIset all_nids(nids.begin(), nids.end());
  bool isconnect = is_connected(all_nids);

  cct[size][nids] = isconnect;
  return isconnect;
}

bool MultiDiGraph::between_connected(const GraphRange& g1, const GraphRange& g2) const
{
  BUIset g2set(g2.begin(), g2.end());
  return between_connected(g2set, g1);
}

bool MultiDiGraph::between_connected(const BUIset& g2set, const GraphRange& g1) const
{
  GraphRange::const_iterator iter;
  EdgeMap::const_iterator iterEM;
  for (iter = g1.begin(); iter != g1.end(); ++iter) {
    int nid = *iter;
    const EdgeMap& sem = succ[nid];
    if (sem.size() > 0) {
      for(iterEM = sem.begin(); iterEM != sem.end(); iterEM++) {
        int sid = iterEM->first;
        if (g2set.find(sid) != g2set.end())
          return true;
      }
    }

    const EdgeMap& pem = pred[nid];
    if (pem.size() > 0) {
      for(iterEM = pem.begin(); iterEM != pem.end(); iterEM++) {
        int pid = iterEM->first;
        if (g2set.find(pid) != g2set.end())
          return true;
      }
    }
  }
  return false;
}

//const MultiDiGraph* MultiDiGraph::get_subgraph(const std::vector<int>& nids) const {
//	MultiDiGraph* mdg = new MultiDiGraph();
//
//	map<int, set<string> >::const_iterator iter;
//	map<int, map<int, set<string> > >::const_iterator iter2;
//	std::set<int>::const_iterator iter3;
//
//	for(iter3=nids.begin(); iter3!=nids.end(); iter3++) {
//		int nid = *iter3;
//		// add node
//		mdg->add_node(nid, nodes.find(nid)->second);
//		// add edges
//		iter2 = succ.find(nid);
//		if (iter2 != succ.end()) {
//			for(iter=iter2->second.begin(); iter!=iter2->second.end(); iter++) {
//				int sid = iter->first;
//				if (nids.find(sid) != nids.end()) {
//					// add a succ edge
//					if (mdg->succ.find(nid) == mdg->succ.end())
//						mdg->succ[nid] = map<int, set<string> >();
//					mdg->succ[nid][sid] = iter->second;
//				}
//			}
//		}
//		iter2 = pred.find(nid);
//		if (iter2 != pred.end()) {
//			for(iter=iter2->second.begin(); iter!=iter2->second.end(); iter++) {
//				int pid = iter->first;
//				if (nids.find(pid) != nids.end()) {
//					// add a succ edge
//					if (mdg->pred.find(pid) == mdg->pred.end())
//						mdg->pred[nid] = map<int, set<string> >();
//					mdg->pred[nid][pid] = iter->second;
//				}
//			}
//		}
//	}
//	return mdg;
//}

BUIset MultiDiGraph::get_neighbors(const GraphRange& nids) const {
	BUIset neighbors;
	BUIset coll(nids.begin(), nids.end());
	EdgeMap::const_iterator iter;
	for(size_t i = 0; i < nids.size(); i++) {
		int nid = nids[i];
		const EdgeMap& sem = succ[nid];
        for(iter=sem.begin(); iter!=sem.end(); iter++) {
          int sid = iter->first;
          if (coll.find(sid) == coll.end())
            neighbors.insert(sid);
        }
        const EdgeMap& pem = pred[nid];
        for(iter=pem.begin(); iter!=pem.end(); iter++) {
          int pid = iter->first;
          if (coll.find(pid) == coll.end())
            neighbors.insert(pid);
        }
	}
	return neighbors;
}

void MultiDiGraph::print_graph() const {
//	cerr << "----------------------" << endl;
//	cerr << "nodes:" << endl;
//	cerr << "size: " << nodes.size() << endl;
//
//	for(map<int, const struct Node*>::const_iterator iter = nodes.begin();
//			iter != nodes.end(); iter++) {
//		const struct Node* n = iter->second;
//		cerr << " [" << n->id << " " << n->word << "]" << endl;
//	}
//
//	cerr << "edges:" << endl;
//
//	map<int, set<string> >::const_iterator iter;
//	map<int, map<int, set<string> > >::const_iterator iter2;
//	for (iter2 = succ.begin(); iter2 != succ.end(); iter2++) {
//		for(iter = iter2->second.begin(); iter != iter2->second.end(); iter++) {
//			for(set<string>::const_iterator it = iter->second.begin();
//					it != iter->second.end(); it++)
//				cerr << " " << "[" << iter2->first << " " << iter->first
//					 << " " << *it << "]" << endl;
//		}
//	}
//
//	cerr << "----------------------" << endl;
}

} /* namespace Moses */
