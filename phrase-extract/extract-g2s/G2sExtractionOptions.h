/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2010 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once
#ifndef G2SEXTRACTIONOPTIONS_H_INCLUDED_
#define G2SEXTRACTIONOPTIONS_H_INCLUDED_


namespace MosesG2S
{

enum REO_MODEL_TYPE {REO_MSD, REO_MSLR, REO_MONO, REO_MSLRO};
enum REO_POS {LEFT, RIGHT, DLEFT, DRIGHT, UNKNOWN, OVERLAP};

struct G2sExtractionOptions {
public:
  int maxSpan;
//  int minHoleSource;
//  int minHoleTarget;
//  int minWords;
  int maxSymbolsTarget;
  int maxSymbolsSource;
//  int maxNonTerm;
//  int maxScope;
  bool onlyDirectFlag;
//  bool glueGrammarFlag;
//  bool unknownWordLabelFlag;
  bool onlyOutputSpanInfo;
//  bool noFileLimit;
//  bool properConditioning;
//  bool nonTermFirstWord;
//  bool nonTermConsecTarget;
//  bool nonTermConsecSource;
//  bool requireAlignedWord;
//  bool sourceSyntax;
//  bool targetSyntax;
  bool duplicateRules;
  bool fractionalCounting;
//  bool pcfgScore;
//  bool outputNTLengths;
  bool gzOutput;
  bool unpairedExtractFormat;
  bool conditionOnTargetLhs;
  bool boundaryRules;
//  bool sibling;
//  bool onlyLastNT;
//  bool augPhrase;
//  bool augPhraseLink;

//  int unalignedOrder;
	bool incLabel;
//	bool targetDep;
//	bool sort;
//	bool noRelaxNT;
//	bool nonTermLastWord;
//	bool normUnalign;

	bool wordModel;
    REO_MODEL_TYPE wordType;
    bool phraseModel;
    REO_MODEL_TYPE phraseType;
    bool hierModel;
    REO_MODEL_TYPE hierType;
    bool allModelsOutputFlag;
    bool orientationFlag;
    bool translationFlag;

  G2sExtractionOptions()
    : maxSpan(10)
//    , minHoleSource(2)
//    , minHoleTarget(1)
//    , minWords(1)
    , maxSymbolsTarget(999)
    , maxSymbolsSource(5)
//    , maxNonTerm(2)
//    , maxScope(999)
    // int minHoleSize(1)
    // int minSubPhraseSize(1) // minimum size of a remaining lexical phrase
    , onlyDirectFlag(false)
//    , glueGrammarFlag(false)
//    , unknownWordLabelFlag(false)
    , onlyOutputSpanInfo(false)
//    , noFileLimit(false)
    //bool zipFiles(false)
//    , properConditioning(false)
//    , nonTermFirstWord(false)
//		, nonTermLastWord(false)
//    , nonTermConsecTarget(true)
//    , nonTermConsecSource(true)
//    , requireAlignedWord(true)
//    , sourceSyntax(false)
//    , targetSyntax(false)
    , duplicateRules(true)
    , fractionalCounting(false)
//    , pcfgScore(false)
//    , outputNTLengths(false)
    , gzOutput(false)
    , unpairedExtractFormat(false)
    , conditionOnTargetLhs(false)
    , boundaryRules(false)
//		, unalignedOrder(999)
		,	incLabel(false)
//		,	targetDep(false)
//		, sort(false)
//		, noRelaxNT(false)
//		, normUnalign(true)
    , wordModel(false),
      wordType(REO_MSD),
      phraseModel(false),
      phraseType(REO_MSD),
      hierModel(false),
      hierType(REO_MSD),
      allModelsOutputFlag(false),
      orientationFlag(false),
      translationFlag(true)
//      sibling(false),
//      onlyLastNT(false),
//      augPhrase(true),
//      augPhraseLink(false)
      {
  }
};

}

#endif
