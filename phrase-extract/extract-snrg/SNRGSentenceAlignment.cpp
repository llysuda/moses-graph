/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2010 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

//#include "SentenceAlignment.h"
#include "SNRGSentenceAlignment.h"

#include <map>
#include <set>
#include <algorithm>
#include "GraphNode.h"
#include "tables-core.h"

using namespace std;

namespace Moses
{
namespace SNRG
{

SNRGSentenceAlignment::~SNRGSentenceAlignment() {}

void addBoundaryWords(vector<string> &phrase)
{
  phrase.insert(phrase.begin(), "<s>");
  phrase.push_back("</s>");
}

bool SNRGSentenceAlignment::processTargetSentence(const char * targetString, int, const SNRGExtractionOptions& options)
{
  target = tokenize(targetString);
  if (options.boundaryRules)
    addBoundaryWords(target);
  return true;
}

bool SNRGSentenceAlignment::processSourceSentence(const char * sourceString, int, const SNRGExtractionOptions& options)
{
  source.Init(sourceString,options);
  return true;
}

bool SNRGSentenceAlignment::create( char targetString[], char sourceString[], char alignmentString[], size_t sentenceID, const SNRGExtractionOptions& options)
{
  
  this->sentenceID = sentenceID;

  // process sentence strings and store in target and source members.
  if (!processTargetSentence(targetString, sentenceID, options)) {
    return false;
  }
  if (!processSourceSentence(sourceString, sentenceID, options)) {
    return false;
  }

  // check if sentences are empty
  if (target.size() == 0 || source.size() == 0) {
    cerr << "no target (" << target.size() << ") or source (" << source.size() << ") words << end insentence " << sentenceID << endl;
    cerr << "T: " << targetString << endl << "S: " << sourceString << endl;
    return false;
  }

  // prepare data structures for alignments
  for(size_t i=0; i<target.size(); i++) {
    alignedCountT.push_back( 0 );
    vector< int > dummy;
    alignedToS.push_back( dummy );
  }
  for(size_t i=0; i<source.size(); i++) {
    vector< int > dummy;
    alignedSoT.push_back( dummy );
  }

  // reading in alignments
  vector<string> alignmentSequence = tokenize( alignmentString );
  for(size_t i=0; i<alignmentSequence.size(); i++) {
    int s,t;
    if (! sscanf(alignmentSequence[i].c_str(), "%d-%d", &s, &t)) {
      cerr << "WARNING: " << alignmentSequence[i] << " is a bad alignment point in sentence " << sentenceID << endl;
      cerr << "T: " << targetString << endl << "S: " << sourceString << endl;
      return false;
    }

    if ((size_t)t >= target.size() || (size_t)s >= source.size()) {
      cerr << "WARNING: sentence " << sentenceID << " has alignment point (" << s << ", " << t << ") out of bounds (" << source.size() << ", " << target.size() << ")\n";
      cerr << "T: " << targetString << endl << "S: " << sourceString << endl;
      return false;
    }
    alignedSoT[s].push_back( t );
    alignedToS[t].push_back( s );
    alignedCountT[t]++;
  }


  source.Annotate(alignedCountT, alignedSoT);

  return true;
}

/*std::string SNRGSentenceAlignment::GetAlignString() const {
  string ret = "";
  for (size_t t = 0; t < alignedToS.size(); t++) {
    for (size_t i = 0; i < alignedToS[t].size(); i++) {
      int s = alignedToS[t][i];
      ret += SPrint<int>(s)+"-"+SPrint<int>(t)+" ";
    }
  }
  return ret.erase(ret.size()-1);
}


Span SNRGSentenceAlignment::GetDepSpan(int start, int end) const {
	Span span(1,0);
	for(int i = start; i <= end; i++) {
		const Edge& edge = source.GetEdge(i);
		Span hspan = edge.GetHeadSpan();
		if(edge.IsConsistent()) {
			if(span.first > span.second) {
				span.first = hspan.first;
				span.second = hspan.second;
			} else {
				span.first = std::min(span.first, hspan.first);
				span.second = std::max(span.second, hspan.second);
			}
		}
	}
	return span;
}

Span SNRGSentenceAlignment::GetWholeSpan(int start, int end) const {
	Span span(1,0);
	for(int i = start; i <= end; i++) {
		const Edge& edge = source.GetEdge(i);
		Span hspan = edge.GetHeadSpan();
		if(span.first > span.second) {
			span.first = hspan.first;
			span.second = hspan.second;
		} else {
			span.first = std::min(span.first, hspan.first);
			span.second = std::max(span.second, hspan.second);
		}
	}
	return span;
}
*/

}
}

