/*
 * DepNgramGraph.cpp
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

#include "tables-core.h"
#include "moses/Util.h"
#include <set>
#include "DepNgramGraph.h"
#include <algorithm>


using namespace std;
using namespace Moses;
//using namespace MosesTraining;

//vector<string> tokenize( const char* input );
//vector<string> tokenize( const char* input, const char separator );

vector<string> tokenize( const char* input, const char separator )
{
  vector< string > token;
  bool betweenWords = true;
  int start=0;
  int i=0;
  for(; input[i] != '\0'; i++) {
    bool isSpace = (input[i] == separator);
    if (!isSpace && betweenWords) {
      start = i;
      betweenWords = false;
    } else if (isSpace && !betweenWords) {
      token.push_back( string( input+start, i-start ) );
      betweenWords = true;
    }
  }
  if (!betweenWords)
    token.push_back( string( input+start, i-start ) );
  return token;
}

namespace Moses {
namespace SNRG {



void DepNgramGraph::Init(const string & line, const SNRGExtractionOptions& options) {
  vector<string> tokens = tokenize(line.c_str());
  vector< vector<int> > children;
  children.resize(tokens.size());

  for(size_t i = 0; i < tokens.size(); i++) {
    vector<string> nodeAttrib = tokenize(tokens[i].c_str(),'|');
    if (nodeAttrib.size() < 3) {
      cerr << "error source format !" << endl;
      exit(1);
    }
    int fid = atoi(nodeAttrib[2].c_str());
    if (fid < 0) {
      //fid = 9999;
      m_rootId.push_back(i);
    }

    if (fid >= 0)
      children[fid].push_back(i);

    string  rel = nodeAttrib[3];
//    if (options.useType)
      rel = "DP";
    GraphNode node(i, nodeAttrib[0], nodeAttrib[1], fid, rel);
    m_nodes.push_back(node);
  }

  // set leaf flag, startNodeID and endNodeID
  for (int i = 0; i < m_nodes.size(); i++) {
      int j = m_nodes[i].GetFid();
      if (j >= 0 && j < m_nodes.size()) {
        m_nodes[j].SetLeaf(false);
        m_nodes[j].AddOutgoing(i);
      }
  }


    if (!options.onlyTree) {
      for(size_t i = 0; i < children.size(); i++) {
        for (size_t j = 1; j < children[i].size(); j++) {
          int uid = children[i][j];
          int nid = children[i][j-1];
          m_nodes[uid].AddOutgoing(nid);
          m_nodes[nid].AddIncoming(uid, "SG");
        }
      }
    }

  CheckSequence();

  InitLabel(options);
}

bool DepNgramGraph::BigramConnected(int im1,int i) const {
  return m_nodes[im1].Connected(i) || m_nodes[i].Connected(im1);
}

bool DepNgramGraph::NextIsOlder(int im1, int i) const {
  int level_im1 = GetLevel(im1);
  int level_i = GetLevel(i);
  if (level_im1<=level_i)
    return false;
  return true;
}

int DepNgramGraph::GetLevel(int index) const {
  int ret = 0;
  while (m_nodes[index].GetFid() >= 0) {
    index = m_nodes[index].GetFid();
    ret++;
  }
  return ret;
}

void DepNgramGraph::Annotate (const vector<int> & alignedCountS, const vector<vector<int> > & alignedToT) {

}

DepNgramGraph::DepNgramGraph()
  : m_rootId (0)
  , m_nodes() {
}

DepNgramGraph::DepNgramGraph(string line, const SNRGExtractionOptions& options) {

  Init(line, options);
}
void DepNgramGraph::CheckSequence() const {
	int id = 0;
  for (size_t i = 0; i < m_nodes.size(); i++){
    assert(id == m_nodes[i].GetId());
    id++;
  }
}

void DepNgramGraph::PrintAnnotation() const
{

}

vector<int> DepNgramGraph::GetPostOrder(int headId) const {
  vector<int> ret;
  for (size_t i = 0; i < m_nodes.size(); i++) {
    if (m_nodes[i].GetFid() == headId) {
      vector<int> kidsOrder = GetPostOrder(m_nodes[i].GetId());
      for (size_t j = 0; j < kidsOrder.size(); i++) {
        ret.push_back(kidsOrder[j]);
      }
    }
  }
  ret.push_back(headId);
  return ret;
}

DepNgramGraph::~DepNgramGraph() {
  // TODO Auto-generated destructor stub
}

string DepNgramGraph::GetFactorString() const
{
  string ret = "";
  for (size_t i = 0; i < m_nodes.size(); i++) {
    ret += m_nodes[i].GetWord() + "|" + m_nodes[i].GetPos() + "|" + SPrint<int>(m_nodes[i].GetFid()) + "|" + m_nodes[i].GetIncoming(0).relation;
    ret += " ";
  }
  return ret.erase(ret.size()-1);
}

void DepNgramGraph::SetFatherId(int nid, int fid)
{
  m_nodes[nid].SetFid(fid);
}


void DepNgramGraph::InitLabel(const SNRGExtractionOptions& options) {

	m_labels.clear();
	for(size_t start = 0; start < m_nodes.size(); start++) {
		m_labels.push_back(vector<vector<string> >());
		for(size_t end=start; end < m_nodes.size(); end++) {
			m_labels[start].push_back(vector<string>());
			vector<string> labels;

			  // use nodes which are heads in dep tree
              for(size_t i = start; i <= end; i++) {
                  const GraphNode& node = m_nodes[i];
                  int fid = node.GetFid();
                  if(fid < (int)start || fid > (int)end) {
                      labels.push_back(node.GetPos());
                  }
              }

            string ret = labels[0];

            for(size_t i = 1; i < labels.size(); i++) {
                ret += "_" + labels[i];
            }

            if (options.singleNT) {
              ret = "SNT";
            }

            m_labels[start][end-start].push_back(ret);
		}
	}

}


int DepNgramGraph::GetSpanFid(int start, int end) const {
	set<int> fids;
	for(int i = start; i <= end; i++) {
		int fid = (int)m_nodes[i].GetFid();
		if(fid < start || fid > end) {
			fids.insert(fid);
		}
	}
	if (fids.size() > 1)
	  return -1;
	assert(fids.size() == 1);
	return *(fids.begin());
}

vector<Edge> DepNgramGraph::GetSpanIncomings(int start, int end) const {
    set<Edge, EdgeSetSortor> incomings;
    for(int i = start; i <= end; i++) {
        const vector<Edge>& ic = m_nodes[i].GetIncomings();
        for(int j = 0; j < ic.size(); j++) {
          if (ic[j].incoming < start || ic[j].incoming > end)
            incomings.insert(ic[j]);
        }
    }
    vector<Edge> ret(incomings.begin(), incomings.end());
    return ret;
}


std::pair<int,int> DepNgramGraph::GetTreeSpan(int headid) const
{
	pair<int, int> span(headid, headid);
	for(size_t i = 0; i < m_nodes.size(); i++) {
		int fid = m_nodes[i].GetFid();
	  if (fid == headid) {
	  	pair<int, int> subspan = GetTreeSpan((int)i);
			span.first = std::min(subspan.first, span.first);
			span.second = std::max(subspan.second, span.second);
	  }
	}
	return span;
}


std::vector<int> DepNgramGraph::GetSpanKidsIndex(int start, int end) const
{
  vector<int> ret;
  int size = m_nodes.size();
  for (int i = 0; i < start; i++) {
    int fid = m_nodes[i].GetFid();
    if (fid >= start && fid <= end)
      ret.push_back(i);
  }
  for (int i = end+1; i < size; i++) {
    int fid = m_nodes[i].GetFid();
    if (fid >= start && fid <= end)
      ret.push_back(i);
  }
  return ret;
}

std::vector<int> DepNgramGraph::GetSpanSiblingIndex(int start, int end) const
{
  vector<int> ret;
  int size = m_nodes.size();
  int rfid = GetSpanFid(start,end);
  for (int i = 0; i < start; i++) {
    int fid = m_nodes[i].GetFid();
    if (fid == rfid)
      ret.push_back(i);
  }
  for (int i = end+1; i < size; i++) {
    int fid = m_nodes[i].GetFid();
    if (fid == rfid)
      ret.push_back(i);
  }
  return ret;
}

void DepNgramGraph::DepthFirst(int start, int end, int current, set<int>& nodes) const {
  vector<int> outgoings = m_nodes[current].GetOutgoings();
  vector<Edge> incomings = m_nodes[current].GetIncomings();

  set<int> adjacent;
  for(size_t i = 0; i < outgoings.size(); i++) {
    int id = outgoings[i];
    if (id >= start && id <= end && nodes.find(id) == nodes.end()) {
      adjacent.insert(id);
    }
  }
  for (size_t i = 0; i < incomings.size(); i++) {
    int id = incomings[i].incoming;
    if (id >= start && id <= end && nodes.find(id) == nodes.end()) {
      adjacent.insert(id);
    }
  }

  for(set<int>::const_iterator iter = adjacent.begin(); iter != adjacent.end(); iter++) {
    nodes.insert(*iter);
    DepthFirst(start, end, *iter, nodes);
  }
}

bool DepNgramGraph::IsConnected(int start, int end) const {
  set<int> nodes;
  nodes.insert(start);
  DepthFirst(start, end, start, nodes);
  return nodes.size() == end-start+1;
}


} /* namespace SNRG */
} /* namespace Moses */
