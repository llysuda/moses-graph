/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2010 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once
#ifndef SNRG_HOLECOLLECTION_H_INCLUDED_
#define SNRG_HOLECOLLECTION_H_INCLUDED_

#include <set>
#include <vector>

#include "SNRGHole.h"

namespace Moses
{
namespace SNRG
{

class SNRGHoleCollection
{
protected:
  HoleList m_holes;
  std::vector<SNRGHole*> m_sortedTargetHoles;
  std::vector<int> m_sourceHoleStartPoints;
  std::vector<int> m_sourceHoleEndPoints;
  std::vector<int> m_scope;
  std::vector<int> m_sourcePhraseStart;
  std::vector<int> m_sourcePhraseEnd;

public:
  SNRGHoleCollection(int sourcePhraseStart, int sourcePhraseEnd)
    : m_scope(1, 0)
    , m_sourcePhraseStart(1, sourcePhraseStart)
    , m_sourcePhraseEnd(1, sourcePhraseEnd) {
  }

  const HoleList &GetHoles() const {
    return m_holes;
  }

  HoleList &GetHoles() {
    return m_holes;
  }

  std::vector<SNRGHole*> &GetSortedTargetHoles() {
    return m_sortedTargetHoles;
  }

  void Add(int startT, int endT, int startS, int endS, bool isdep);

  void RemoveLast();

  bool OverlapTarget(const SNRGHole &targetHole) const {
    HoleList::const_iterator iter;
    for (iter = m_holes.begin(); iter != m_holes.end(); ++iter) {
      const SNRGHole &currHole = *iter;
      if (currHole.Overlap(targetHole, 1))
        return true;
    }
    return false;
  }

  bool ConsecSource(const SNRGHole &sourceHole) const {
    HoleList::const_iterator iter;
    for (iter = m_holes.begin(); iter != m_holes.end(); ++iter) {
      const SNRGHole &currHole = *iter;
      if (currHole.Neighbor(sourceHole, 0))
        return true;
    }
    return false;
  }

  bool ConsecTarget(const SNRGHole &sourceHole) const {
		HoleList::const_iterator iter;
		for (iter = m_holes.begin(); iter != m_holes.end(); ++iter) {
			const SNRGHole &currHole = *iter;
			if (currHole.Neighbor(sourceHole, 1))
				return true;
		}
		return false;
	}

  // Determine the scope that would result from adding the given hole.
  int Scope(const SNRGHole &proposedHole) const;

  void SortTargetHoles();

};

}
}

#endif
